/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#include <stm_display_types.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include "capture_defs.h"
#include "capture_regs.h"

#include "stmgammacapture.h"
#include "capture_filters.h"

#define CAP_HFILTER_TABLE_SIZE (NB_HSRC_FILTERS * (NB_HSRC_COEFFS + HFILTERS_ALIGN))
#define CAP_VFILTER_TABLE_SIZE (NB_VSRC_FILTERS * (NB_VSRC_COEFFS + VFILTERS_ALIGN))
#define CAP_MATRIX_TABLE_SIZE  (NB_MATRIX_CONVERSION * MATRIX_ENTRY_SIZE)

//#define CAPTURE_DEBUG_REGISTERS


#define CAPTURE_INPUT_CS_RGB              0x1

static const stm_pixel_capture_input_s stm_pixel_capture_inputs[] = {
  {"stm_input_video1",      CAP_CTL_SOURCE_VID1      , 0},
  {"stm_input_video2",      CAP_CTL_SOURCE_VID2      , 1},
  {"stm_input_mix2",        CAP_CTL_SOURCE_MIX2_YCbCr, 1},
  {"stm_input_mix1",        CAP_CTL_SOURCE_MIX1_YCbCr, 0},
};

static const stm_pixel_capture_input_s stm_pixel_capture_inputs_hw_v3_0[] = {
  {"stm_input_mix2",        CAP_CTL_SOURCE_MIX1_YCbCr_HW_V3_0, CAP_CTL_MIX1_VTG_SELECT_HW_V3_0},
  {"stm_input_mix1",        CAP_CTL_SOURCE_MIX0_YCbCr_HW_V3_0, CAP_CTL_MIX0_VTG_SELECT_HW_V3_0},
};

static const stm_pixel_capture_input_s stm_pixel_capture_inputs_hw_v3_0_6[] = {
  {"stm_input_video1",      CAP_CTL_SOURCE_VID1            , CAP_CTL_MIX0_VTG_SELECT_HW_V3_0},
  {"stm_input_video2",      CAP_CTL_SOURCE_VID2            , CAP_CTL_MIX1_VTG_SELECT_HW_V3_0},
  {"stm_input_mix2",        CAP_CTL_SOURCE_MIX2_RGB_HW_V3_0, CAP_CTL_MIX2_VTG_SELECT_HW_V3_0},
  {"stm_input_mix1",        CAP_CTL_SOURCE_MIX1_RGB_HW_V3_0, CAP_CTL_MIX1_VTG_SELECT_HW_V3_0},
};

static const stm_pixel_capture_format_t c_surfaceFormats[] = {
  STM_PIXEL_FORMAT_YCbCr422R,
  STM_PIXEL_FORMAT_RGB565,
  STM_PIXEL_FORMAT_RGB_8B8B8B_SP,
  STM_PIXEL_FORMAT_ARGB1555,
  STM_PIXEL_FORMAT_ARGB8565,
  STM_PIXEL_FORMAT_ARGB8888,
  STM_PIXEL_FORMAT_BGRA8888,
  STM_PIXEL_FORMAT_ARGB4444,
  STM_PIXEL_FORMAT_YUV_8B8B8B_SP,
  STM_PIXEL_FORMAT_RAW_10B10B10B_SP,
};

static stm_pixel_capture_format_t c_Formats[STM_PIXEL_FORMAT_COUNT];

#if defined(CAPTURE_DEBUG_REGISTERS)
static uint32_t ctx=0;
#endif

CGammaCompositorCAP::CGammaCompositorCAP(const char     *name,
                                         uint32_t        id,
                                         uint32_t        base_address,
                                         uint32_t        size,
                                         const CPixelCaptureDevice *pDev,
                                         const stm_pixel_capture_capabilities_flags_t caps,
                                         stm_pixel_capture_hw_features_t hw_features):CCapture(name, id, pDev, caps)
{
  TRCIN( TRC_ID_COMPO_CAPTURE, "%s id = %u", name, id );

  m_pSurfaceFormats = c_surfaceFormats;
  m_nFormats = N_ELEMENTS (c_surfaceFormats);

  m_ulCAPBaseAddress       = base_address;
  m_CompoHardwareFeatures  = hw_features;

  if(m_CompoHardwareFeatures.compo_cap_v3_0_6)
  {
    m_pCaptureInputs         = stm_pixel_capture_inputs_hw_v3_0_6;
    m_numInputs              = N_ELEMENTS(stm_pixel_capture_inputs_hw_v3_0_6);
  }
  else if (m_CompoHardwareFeatures.compo_cap_v3_0)
  {
    m_pCaptureInputs         = stm_pixel_capture_inputs_hw_v3_0;
    m_numInputs              = N_ELEMENTS(stm_pixel_capture_inputs_hw_v3_0);
  }
  else
  {
    m_pCaptureInputs         = stm_pixel_capture_inputs;
    m_numInputs              = N_ELEMENTS(stm_pixel_capture_inputs);
  }

  m_pCurrentInput          = m_pCaptureInputs[0];

  /*
   * Override scaling capabilities this for Gamma Compositor Capture hw.
   */
  m_ulMaxHSrcInc = m_fixedpointONE*8; // downscale to 1/8
  m_ulMaxVSrcInc = m_fixedpointONE*8; // downscale to 1/8
  m_ulMinHSrcInc = m_fixedpointONE;   // upscale not supported by compo capture
  m_ulMinVSrcInc = m_fixedpointONE;   // upscale not supported by compo capture

  /* No Skip Line support for Compositor Capture */
  m_ulMaxLineStep = 1;

  m_HFilter      = (DMA_Area) { 0 };
  m_VFilter      = (DMA_Area) { 0 };
  m_CMatrix      = (DMA_Area) { 0 };

  m_pReg = (uint32_t*)vibe_os_map_memory(m_ulCAPBaseAddress, size);
  ASSERTF(m_pReg,("Unable to map CAPTURE device registers"));

  TRC( TRC_ID_COMPO_CAPTURE, "Register remapping 0x%08x -> 0x%08x", m_ulCAPBaseAddress,(uint32_t)m_pReg );

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );
}


CGammaCompositorCAP::~CGammaCompositorCAP(void)
{
  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  this->Stop();

  vibe_os_free_dma_area(&m_HFilter);
  vibe_os_free_dma_area(&m_VFilter);
  vibe_os_free_dma_area(&m_CMatrix);

  vibe_os_unmap_memory(m_pReg);
  m_pReg = 0;

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );
}


bool CGammaCompositorCAP::Create(void)
{
  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  /*
   * Each filter table contains _two_ complete sets of filters, one for
   * Luma and one for Chroma.
   */
  vibe_os_allocate_dma_area(&m_HFilter, CAP_HFILTER_TABLE_SIZE, 16, SDAAF_NONE);
  if(!m_HFilter.pMemory)
  {
    TRC( TRC_ID_ERROR, "Out of memory" );
    return false;
  }
  vibe_os_memcpy_to_dma_area(&m_HFilter,0,&capture_HSRC_Coeffs,sizeof(capture_HSRC_Coeffs));

  TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "&m_HFilter = %p pMemory = %p pData = %p phys = %08x", &m_HFilter,m_HFilter.pMemory,m_HFilter.pData,m_HFilter.ulPhysical );

  vibe_os_allocate_dma_area(&m_VFilter, CAP_VFILTER_TABLE_SIZE, 16, SDAAF_NONE);
  if(!m_VFilter.pMemory)
  {
    TRC( TRC_ID_ERROR, "Out of memory" );
    goto exit_error;
  }
  vibe_os_memcpy_to_dma_area(&m_VFilter,0,&capture_VSRC_Coeffs,sizeof(capture_VSRC_Coeffs));

  TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "&m_VFilter = %p pMemory = %p pData = %p phys = %08x", &m_VFilter,m_VFilter.pMemory,m_VFilter.pData,m_VFilter.ulPhysical );

  vibe_os_allocate_dma_area(&m_CMatrix, CAP_MATRIX_TABLE_SIZE, 32, SDAAF_NONE);
  if(!m_CMatrix.pMemory)
  {
    TRC( TRC_ID_ERROR, "CGammaCompositorCAP::Create 'out of memory'" );
    goto exit_error;
  }
  vibe_os_memcpy_to_dma_area(&m_CMatrix,0,&capture_MATRIX_Coeffs,sizeof(capture_MATRIX_Coeffs));

  TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "&m_CMatrix = %p pMemory = %p pData = %p phys = %08x", &m_CMatrix,m_CMatrix.pMemory,m_CMatrix.pData,m_CMatrix.ulPhysical );

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );
  return true;

exit_error:
  if(m_HFilter.pMemory)
      vibe_os_free_dma_area(&m_HFilter);

  if(m_VFilter.pMemory)
      vibe_os_free_dma_area(&m_VFilter);

  if(m_CMatrix.pMemory)
      vibe_os_free_dma_area(&m_CMatrix);

  return false;
}


bool CGammaCompositorCAP::ConfigureCaptureResizeAndFilters(const stm_pixel_capture_buffer_descr_t * const   pBuffer,
                                                      CaptureQueueBufferInfo               &qbinfo,
                                                      GammaCaptureSetup_t                  *pCaptureSetup)
{
  uint32_t ulCtrl = 0;
  uint32_t hw_srcinc;
  unsigned filter_index;

  if(qbinfo.hsrcinc != (uint32_t)m_fixedpointONE)
  {
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "H Resize Enabled (HSRC 4.13/4.8: %x/%x)", qbinfo.hsrcinc, qbinfo.hsrcinc >> 5 );

    hw_srcinc = (qbinfo.hsrcinc >> 5);

    pCaptureSetup->HSRC = hw_srcinc;

    ulCtrl |= CAP_CTL_EN_H_RESIZE;

    for(filter_index=0;filter_index<N_ELEMENTS(HSRC_index);filter_index++)
    {
      if(hw_srcinc <= HSRC_index[filter_index])
      {
        TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "  -> 5-Tap Polyphase Filtering (idx: %d)",  filter_index );

        pCaptureSetup->HFCn = (uint32_t )m_HFilter.pData + (filter_index*HFILTERS_ENTRY_SIZE);

        pCaptureSetup->HSRC |= CAP_HSRC_FILTER_EN;
        break;
      }
    }
  }

  if(qbinfo.vsrcinc != (uint32_t)m_fixedpointONE)
  {
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "V Resize Enabled (VSRC 4.13/4.8: %x/%x)", qbinfo.vsrcinc, qbinfo.vsrcinc >> 5 );

    hw_srcinc = (qbinfo.vsrcinc >> 5);

    pCaptureSetup->VSRC = hw_srcinc;

    ulCtrl |= CAP_CTL_EN_V_RESIZE;

    if(qbinfo.isInputInterlaced)
    {
      /*
       * When putting progressive content on an interlaced display
       * we adjust the filter phase of the bottom field to start
       * an appropriate distance lower in the source bitmap, based on the
       * scaling factor. If the scale means that the bottom field
       * starts >1 source bitmap line lower then this will get dealt
       * with in the memory setup by adjusting the source bitmap address.
       */
      uint32_t fractionalinc = ((qbinfo.verticalFilterInputSamples - 1) * m_fixedpointONE
                                / (qbinfo.verticalFilterOutputSamples ? (qbinfo.verticalFilterOutputSamples - 1):1));
      uint32_t fieldphase    = (((fractionalinc - m_fixedpointONE + (m_fixedpointONE>>8)))
                                / (m_fixedpointONE>>2)) % 8;

      if(qbinfo.InputFrameRect.y%2)
        pCaptureSetup->VSRC |= (fieldphase<<CAP_VSRC_TOP_INITIAL_PHASE_SHIFT);
      else
        pCaptureSetup->VSRC |= (fieldphase<<CAP_VSRC_BOT_INITIAL_PHASE_SHIFT);
    }

    for(filter_index=0;filter_index<N_ELEMENTS(VSRC_index);filter_index++)
    {
      if(hw_srcinc <= VSRC_index[filter_index])
      {
        TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "  -> 3-Tap Polyphase Filtering (idx: %d)",  filter_index );

        pCaptureSetup->VFCn = (uint32_t )m_VFilter.pData + (filter_index*VFILTERS_ENTRY_SIZE);

        pCaptureSetup->VSRC |= CAP_VSRC_FILTER_EN;

        break;
      }
    }
  }

  pCaptureSetup->CTL |= ulCtrl;

  return true;
}


void CGammaCompositorCAP::ConfigureCaptureWindowSize(const stm_pixel_capture_buffer_descr_t * const   pBuffer,
                                                      CaptureQueueBufferInfo               &qbinfo,
                                                      GammaCaptureSetup_t                  *pCaptureSetup)
{
  uint32_t start_x, stop_x, start_y, stop_y;
  uint32_t uAVO, uAVS, uBCO, uBCS;

  /*
   * Set Capture viewport start, start on the first blanking line (negative offset
   * from the start of the active area).
   */
  start_x = qbinfo.InputFrameRect.x;

  int blankline = m_InputParams.vsync_width - (qbinfo.InputVisibleArea.y - 1);
  if(qbinfo.isInputInterlaced)
    blankline *= 2;

  start_y = CaptureCalculateWindowLine(m_InputParams, blankline) + qbinfo.InputFrameRect.y;
  if(qbinfo.isInputInterlaced)
    start_y += qbinfo.InputFrameRect.y;

  uAVO = start_x | (start_y << 16);

  //Set Capture to start at the first active video line
  start_y = CaptureCalculateWindowLine(m_InputParams, qbinfo.InputFrameRect.y);

  if((m_CompoHardwareFeatures.interlaced_hw_shift)
  && ((m_InputParams.flags & STM_PIXEL_CAPTURE_BUFFER_INTERLACED) == STM_PIXEL_CAPTURE_BUFFER_INTERLACED))
  {
    /*
     * Issue in HW in case of Interlaced format :
     *
     * The Capture receives the information of first active line from the
     * source (MIXER_BCO.ydo). This register is "frame based".
     * In the Capture, the operation FRAME=>FIELD is done twice (instead
     * of once) on this value.
     * The consequences are :
     * - The Resulting value is divided by 4 instead of 2 in case of
     *   Interlaced mode.
     * - The Capture misses the first lines of the active windows
     * - The number of lines missed depends on the value of this register
     *
     * HW bug confirmed by design.
     *
     *------------------------------------------------------
     * SW WA AVAILABLE
     *------------------------------------------------------
     * The hypothesis is that MIXER_AVO.ydo is even.
     * To correct the bug we can adjust the Capture windows :
     *  => CAP_CWO.ydo = MIXER_BCO.ydo - (MIXER_AVO.ydo / 2)
     *  => CAP_CWO.yds = MIXER_BCO.yds - (MIXER_AVO.ydo / 2)
     * ------------------------------------------------------
     */
     start_y -= (CaptureCalculateWindowLine(m_InputParams, blankline) + qbinfo.InputFrameRect.y) >> 1;
  }
  uBCO = start_x | (start_y << 16);

  /*
   * Set Active video stop, this is also where the capture stop as well
   */
  stop_x = start_x + qbinfo.InputFrameRect.width - 1;
  stop_y = start_y + qbinfo.InputHeight - 1;

  uAVS = stop_x | (stop_y << 16);
  uBCS = stop_x | (stop_y << 16);

  /* Use Video Active Area in case capturing video pilene */
  if(!m_CompoHardwareFeatures.compo_cap_v3_0
  && ((m_pCurrentInput.input_id == CAP_CTL_SOURCE_VID1) || (m_pCurrentInput.input_id == CAP_CTL_SOURCE_VID2)))
  {
    /* FIXME : Check if this configuration is OK for all display modes */
    pCaptureSetup->CWO = uAVO;
    pCaptureSetup->CWS = uAVS;
  }
  else
  {
    pCaptureSetup->CWO = uBCO;
    pCaptureSetup->CWS = uBCS;
  }
}


void CGammaCompositorCAP::ConfigureCaptureBufferSize(const stm_pixel_capture_buffer_descr_t * const   pBuffer,
                                                      CaptureQueueBufferInfo               &qbinfo,
                                                      GammaCaptureSetup_t                  *pCaptureSetup)
{
  uint32_t width       = qbinfo.BufferFrameRect.width;
  uint32_t top_height  = qbinfo.BufferHeight;
  uint32_t bot_height  = CAP_CMW_BOT_EQ_TOP_HEIGHT;

  if(qbinfo.isInputInterlaced && !qbinfo.isBufferInterlaced)
    top_height = top_height/2;

  pCaptureSetup->CMW = (bot_height | (top_height << 16) | width);

  pCaptureSetup->buffer_size = qbinfo.BufferHeight * pBuffer->cap_format.stride;

}


bool CGammaCompositorCAP::ConfigureCaptureColourFmt(const stm_pixel_capture_buffer_descr_t * const   pBuffer,
                                                      CaptureQueueBufferInfo               &qbinfo,
                                                      GammaCaptureSetup_t                  *pCaptureSetup)
{
  uint32_t ulCtrl = 0;
  uint32_t ulCtrlEndianess = 0;
  uint32_t buffer_address = 0;

  switch(qbinfo.BufferFormat.format)
  {
    case STM_PIXEL_FORMAT_RGB565:
      ulCtrl = CAP_CTL_RGB_565;
      buffer_address = pBuffer->rgb_address;
      break;

    case STM_PIXEL_FORMAT_RGB888:
      ulCtrl = CAP_CTL_RGB_888;
      buffer_address = pBuffer->rgb_address;
      break;

    case STM_PIXEL_FORMAT_ARGB1555:
      ulCtrl = CAP_CTL_ARGB_1555;
      buffer_address = pBuffer->rgb_address;
      break;

    case STM_PIXEL_FORMAT_ARGB8565:
      ulCtrl = CAP_CTL_ARGB_8565;
      buffer_address = pBuffer->rgb_address;
      break;

    case STM_PIXEL_FORMAT_ARGB8888:
      ulCtrl = CAP_CTL_ARGB_8888;
      buffer_address = pBuffer->rgb_address;
      break;

    case STM_PIXEL_FORMAT_BGRA8888:
      ulCtrl = CAP_CTL_ARGB_8888;
      ulCtrlEndianess = CAP_CTL_BIGENDIAN;
      buffer_address = pBuffer->rgb_address;
      break;

    case STM_PIXEL_FORMAT_ARGB4444:
      ulCtrl = CAP_CTL_ARGB_4444;
      buffer_address = pBuffer->rgb_address;
      break;

    case STM_PIXEL_FORMAT_YCbCr422R:
      ulCtrl = CAP_CTL_YCbCr422R;
      buffer_address = pBuffer->luma_address;
      break;

    case STM_PIXEL_FORMAT_YUV:
      ulCtrl = CAP_CTL_YCbCr888;
      buffer_address = pBuffer->luma_address;
      break;

    case STM_PIXEL_FORMAT_RAW_10B10B10B_SP:
      ulCtrl = CAP_CTL_32_BITS_WORD;
      buffer_address = pBuffer->rgb_address;
      break;

    default:
      TRC( TRC_ID_ERROR, "Unknown colour format." );
      return false;
  }
  ulCtrl = (ulCtrl & CAP_CTL_FORMAT_MASK) << CAP_CTL_FORMAT_OFFSET;
  ulCtrl |= ulCtrlEndianess;

  if(m_CompoHardwareFeatures.colorspace_convert)
    {
      if((qbinfo.BufferFormat.color_space == STM_PIXEL_CAPTURE_BT709) ||
         (qbinfo.BufferFormat.color_space == STM_PIXEL_CAPTURE_BT709_FULLRANGE))
        ulCtrl |= CAP_CTL_BF_709_SELECT;
    }

  pCaptureSetup->CTL |= ulCtrl;

  pCaptureSetup->VTP = buffer_address;
  if(qbinfo.isInputInterlaced)
    {
      pCaptureSetup->PMP = pBuffer->cap_format.stride * 2;
      pCaptureSetup->VBP = buffer_address + pBuffer->cap_format.stride;
    }
  else
    {
      pCaptureSetup->PMP = pBuffer->cap_format.stride;
      pCaptureSetup->VBP = buffer_address;
    }

  return true;
}

bool CGammaCompositorCAP::CheckInputParams(stm_pixel_capture_input_params_t params)
{
  bool is_params_supported = true;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  is_params_supported = CCapture::CheckInputParams(params);

  if ((params.color_space == STM_PIXEL_CAPTURE_BT2020)
      && (((params.pixel_format != STM_PIXEL_FORMAT_RGB_10B10B10B_SP)
           && (params.pixel_format != STM_PIXEL_FORMAT_RAW_10B10B10B_SP))
          || !m_CompoHardwareFeatures.bt2020_r))
  {
    TRC( TRC_ID_ERROR, "Invalid Capture Input Params!");
    is_params_supported = false;
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return is_params_supported;
}

void CGammaCompositorCAP::ConfigureCaptureInput(const stm_pixel_capture_buffer_descr_t * const   pBuffer,
                                                      CaptureQueueBufferInfo               &qbinfo,
                                                      GammaCaptureSetup_t                  *pCaptureSetup)
{
  uint32_t ulCtrl = 0;

  /*
   * The video inputs are begin captured in ARGB8888 format with
   * alpha 0..128 range.
   *
   * If the selected input is video1 or video2 then we should enable
   * RGB color space and disable YCbCr to RGB convertion.
   */
  if(!m_CompoHardwareFeatures.compo_cap_v3_0)
  {
    if((m_pCurrentInput.input_id == CAP_CTL_SOURCE_VID1) || (m_pCurrentInput.input_id == CAP_CTL_SOURCE_VID2))
    {
      qbinfo.isInputRGB = true;
    }
  }

  if(!m_CompoHardwareFeatures.ycbcr_input)
  {
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "Force RGB input selection - no support of YCbCr input");
    qbinfo.isInputRGB = true;
  }

  if((qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_RGB565)   ||
     (qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_RGB888)   ||
     (qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_ARGB1555) ||
     (qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_ARGB4444) ||
     (qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_ARGB8565) ||
     (qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_BGRA8888) ||
     (qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_ARGB8888))
  {
    qbinfo.isInputRGB = true;
  }

  if(qbinfo.isInputRGB)
  {
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "RGB input selected" );
    ulCtrl  = ((m_pCurrentInput.input_id | CAPTURE_INPUT_CS_RGB) & CAP_CTL_SEL_SOURCE_MASK) << CAP_CTL_SEL_SOURCE_OFFSET;
  }
  else
  {
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "YCbCr input selected" );
    ulCtrl  = ((m_pCurrentInput.input_id) & CAP_CTL_SEL_SOURCE_MASK) << CAP_CTL_SEL_SOURCE_OFFSET;
  }

  if(m_CompoHardwareFeatures.compo_cap_v3_0
     || m_CompoHardwareFeatures.compo_cap_v3_0_6)
  {
    ulCtrl |= (m_ulTimingID << CAP_CTL_VTG_SELECT_OFFSET_HW_V3_0);
  }
  else
  {
    ulCtrl |= (m_ulTimingID ? CAP_CTL_VTG_SELECT:0);
  }

  if(!m_CompoHardwareFeatures.ycbcr_input)
  {
    if((qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_YUV) ||
       (qbinfo.BufferFormat.format == STM_PIXEL_FORMAT_YCbCr422R))
    {
      if(m_CompoHardwareFeatures.conversion_matrix)
      {
        /*
         * The matrixmode should define which conversion matrix to be used
         * We have two conversion matrix coeffecients:
         * Matrix coefficiens 601, RGBs => YCbCr (index == 0)
         * Matrix coefficiens 709, RGBs => YCbCr (index == 1)
         */
        int matrixmode=0;

        if((qbinfo.BufferFormat.color_space == STM_PIXEL_CAPTURE_BT709) ||
           (qbinfo.BufferFormat.color_space == STM_PIXEL_CAPTURE_BT709_FULLRANGE))
          matrixmode=1;

        ulCtrl |= CAP_CTL_RGB2YCBCR_ENA;
        pCaptureSetup->MXn = ((uint32_t )m_CMatrix.pData + ((int)matrixmode*MATRIX_ENTRY_SIZE));
      }
      else
      {
        TRC( TRC_ID_ERROR, "Color conversion from RGB to YCbCr is not supported");
      }
    }
  }

  pCaptureSetup->CTL |= ulCtrl;
}


int CGammaCompositorCAP::GetFormats(stm_pixel_capture_format_t* pFormats,
                                    uint32_t formats_cnt) const
{
  uint32_t num_formats = 0;

  /*
   * This function should return the supported buffer formats according
   * to the current input setting.
   */
  if((m_InputParams.active_window.width != 0)
     && (m_InputParams.active_window.height != 0)
     && (m_InputParams.src_frame_rate != 0))
  {
    uint32_t i;
    bool isInputRGB = ((m_InputParams.color_space == STM_PIXEL_CAPTURE_RGB)
                       || (m_InputParams.color_space == STM_PIXEL_CAPTURE_RGB_VIDEORANGE));

    vibe_os_zero_memory(c_Formats, (STM_PIXEL_FORMAT_COUNT*sizeof(stm_pixel_capture_format_t)));
    for(i=0; i<m_nFormats;i++)
    {
      if(((c_surfaceFormats[i] == STM_PIXEL_FORMAT_YCbCr422R)
          || (c_surfaceFormats[i] == STM_PIXEL_FORMAT_YUV_8B8B8B_SP))
         && (isInputRGB))
      {
        /* COMPO doesn't support RGB ==> YCbCr CS conversion */
        continue;
      }
      else
      {
        c_Formats[num_formats] = c_surfaceFormats[i];
        num_formats++;
      }
    }

    if (num_formats > formats_cnt)
    {
      num_formats = formats_cnt;
    }

    if (num_formats>0)
      vibe_os_memcpy(pFormats, c_Formats, num_formats*sizeof(stm_pixel_capture_format_t));
  }
  else
  {
    /*
     * We don't yet set any input params so likely we can be supporting
     * any format.
     */
    num_formats = formats_cnt;
    if (num_formats > m_nFormats)
    {
      num_formats = m_nFormats;
    }

    if (num_formats>0)
      vibe_os_memcpy(pFormats, m_pSurfaceFormats, num_formats*sizeof(stm_pixel_capture_format_t));
  }

  return (int)num_formats;
}


int CGammaCompositorCAP::GetFrameSize(const stm_pixel_capture_format_t format,
                           stm_pixel_capture_format_frame_size_t *frame_sizes)
{
  /* No impact of the format on the resize capabilities */
  if((m_InputWindow.width == 0) || (m_InputWindow.height == 0))
  {
    *frame_sizes = (stm_pixel_capture_format_frame_size_t){0,0,0,0};
    return -ENOTSUPP;
  }
  else
  {
    *frame_sizes = (stm_pixel_capture_format_frame_size_t)
                   {m_InputWindow.width/16,
                    m_InputWindow.width,
                    m_InputWindow.height/16,
                    m_InputWindow.height};
    return 0;
  }
}


GammaCaptureSetup_t * CGammaCompositorCAP::PrepareCaptureSetup(const stm_pixel_capture_buffer_descr_t * const   pBuffer,
                                                          CaptureQueueBufferInfo               &qbinfo)
{
  GammaCaptureSetup_t     *pCaptureSetup;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  // Allocate a buffer for Capture data. This buffer will be freed by CQueue::ReleaseNode
  pCaptureSetup = (GammaCaptureSetup_t *) vibe_os_allocate_memory (sizeof(GammaCaptureSetup_t));
  if(!pCaptureSetup)
  {
    TRC( TRC_ID_ERROR, "Failed to allocate pCaptureSetup" );
    return 0;
  }
  vibe_os_zero_memory(pCaptureSetup, sizeof(GammaCaptureSetup_t));

  pCaptureSetup->buffer_address = (uint32_t)pBuffer;

  if(m_CompoHardwareFeatures.top_bot_field_cap)
    pCaptureSetup->CTL = (CAP_CTL_BFCAP_ENA | CAP_CTL_TFCAP_ENA);

  /*
   * Enable capture for next VSync (hardware will start on next TOP
   * field).
   */
  pCaptureSetup->CTL |= CAP_CTL_CAPTURE_ENA;
  if(m_CompoHardwareFeatures.sop)
  {
    pCaptureSetup->CTL |= CAP_CTL_SOP;
  }


  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return pCaptureSetup;
}


bool CGammaCompositorCAP::PrepareAndQueueNode(const stm_pixel_capture_buffer_descr_t* const pBuffer, CaptureQueueBufferInfo &qbinfo, struct stm_i_capture_buffer_desc *i_buffer)
{
  capture_node_h              capNode   = {0};
  GammaCaptureSetup_t        *pCaptureSetup = (GammaCaptureSetup_t *)NULL;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  // Prepare capture
  pCaptureSetup = PrepareCaptureSetup(pBuffer, qbinfo);
  if(!pCaptureSetup)
  {
    TRC( TRC_ID_ERROR, "Failed to allocate pCaptureSetup" );
    return false;
  }

  capNode = m_CaptureQueue->GetFreeNode();
  if(!capNode)
  {
    TRC( TRC_ID_ERROR, "Failed to get free capture node!" );
    goto failed_to_prepare_params;
  }

  if(!pBuffer->cap_format.stride)
  {
    TRC( TRC_ID_ERROR, "Invalid capture format stride value!" );
    goto failed_to_prepare_params;
  }

  if(!AdjustBufferInfoForScaling(pBuffer, qbinfo))
  {
    TRC( TRC_ID_ERROR, "Failed to get adjust new buffer for scaling");
    goto failed_to_prepare_params;
  }

  TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "one = 0x%x hsrcinc = 0x%x chsrcinc = 0x%x",m_fixedpointONE, qbinfo.hsrcinc,qbinfo.chroma_hsrcinc );
  TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "one = 0x%x vsrcinc = 0x%x cvsrcinc = 0x%x",m_fixedpointONE, qbinfo.vsrcinc,qbinfo.chroma_vsrcinc );

  /*
   * Configure the capture Colour Format.
   */
  if(!ConfigureCaptureColourFmt(pBuffer, qbinfo, pCaptureSetup))
  {
    TRC( TRC_ID_ERROR, "Failed to configure capture buffer format" );
    goto failed_to_prepare_params;
  }

  /*
   * Configure the capture Resize and Filter.
   */
  if(!ConfigureCaptureResizeAndFilters(pBuffer, qbinfo, pCaptureSetup))
  {
    TRC( TRC_ID_ERROR, "Failed to configure capture resize and filters" );
   goto failed_to_prepare_params;
  }

  /* Configure the capture source size */
  ConfigureCaptureWindowSize(pBuffer, qbinfo, pCaptureSetup);

  /* Configure the capture buffer (memory) size */
  ConfigureCaptureBufferSize(pBuffer, qbinfo, pCaptureSetup);

  /* Configure the capture input */
  ConfigureCaptureInput(pBuffer, qbinfo, pCaptureSetup);

  if(i_buffer)
    pCaptureSetup->i_buffer = *i_buffer;
  else
    pCaptureSetup->i_buffer = (struct stm_i_capture_buffer_desc){0};

  /* Set the new node data */
  m_CaptureQueue->SetNodeData(capNode,pCaptureSetup);

  pCaptureSetup = 0;  // This buffer is now under the control of queue_node

  /* Finally queue the new capture node */
  if(!m_CaptureQueue->QueueNode(capNode))
  {
    TRC( TRC_ID_ERROR, "Failed to queue the new capture node!" );
    goto failed_to_prepare_params;
  }

  /*
   * Until now the capture is started and a buffer will need to be
   * release later.
   */
  m_hasBuffersToRelease = true;

  TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "Node 0x%p successfuly queued!", capNode);

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return true;

failed_to_prepare_params:

  if(pCaptureSetup)
    vibe_os_free_memory(pCaptureSetup);

  if(capNode)
    m_CaptureQueue->ReleaseNode(capNode);

  return false;
}


void CGammaCompositorCAP::Flush(bool bFlushCurrentNode)
{
  uint32_t          nodeReleasedNb = 0;
  capture_node_h    cut_node;           // The Queue will be cut just before this node
  capture_node_h    tail = 0;           // Remaining part of the queue after the cut

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  if (bFlushCurrentNode)
  {
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "Capture Flush ALL nodes" );
  }
  else
  {
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "Capture Flush nodes not on processing" );
  }

  ///////////////////////////////////////    Critical section      ///////////////////////////////////////////

  // Take the lock to ensure that CaptureUpdateHW() cannot be executed while we update the Queue
  if ( vibe_os_down_semaphore(m_QueueLock) != 0 )
    return;

  if (bFlushCurrentNode)
  {
    // Flush ALL nodes

    // Cut the Queue at its head
    cut_node = m_CaptureQueue->GetNextNode(0);

    if (cut_node)
    {
      if (m_CaptureQueue->CutTail(cut_node))
      {
        tail = cut_node;
      }
    }

    // References to the previous, pending and current nodes are reset
    m_previousNode = 0;
    m_pendingNode  = 0;
    m_currentNode  = 0;

    /* nothing to release */
    m_hasBuffersToRelease = false;
  }
  else
  {
    // Flush the nodes not on processing

    // Cut the Queue after the pending node
    cut_node = m_CaptureQueue->GetNextNode(m_pendingNode);

    if (cut_node)
    {
      if (m_CaptureQueue->CutTail(cut_node))
      {
        tail = cut_node;
      }
    }
  }

  vibe_os_up_semaphore(m_QueueLock);
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////

  // It is now safe to release all the nodes in "tail"

  while(tail != 0)
  {
    capture_node_h nodeToRelease = tail;

    // Get the next node BEFORE releasing the current node (otherwise we lose the reference)
    tail = m_CaptureQueue->GetNextNode(tail);

    // Flush pushed buffer before releasing the node
    if(IsAttached() && m_PushGetInterface.push_buffer)
    {
      if(PushCapturedNode(nodeToRelease, 0, 0, true) == 0)
      {
        nodeReleasedNb++;
      }
    }
    else
    {
      m_CaptureQueue->ReleaseNode(nodeToRelease);
      nodeReleasedNb++;
      m_Statistics.PicReleased++;
    }
  }

  if(nodeReleasedNb == m_NumberOfQueuedBuffer)
  {
    m_NumberOfQueuedBuffer = 0;
    m_hasBuffersToRelease = false;
  }

  TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "%d nodes released.", nodeReleasedNb );

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );
}


int CGammaCompositorCAP::ReleaseBuffer(stm_pixel_capture_buffer_descr_t *pBuffer)
{
  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  /*
   * Don't allow the release of queued nodes for tunneled capture mode.
   * This will be done by the driver when stopping the capture.
   */
  if(IsAttached())
  {
    TRC( TRC_ID_ERROR, "Operation not supported" );
    return -ENOTSUPP;
  }

  if(m_hasBuffersToRelease && m_CaptureQueue)
  {
    capture_node_h       pNode = 0;
    GammaCaptureSetup_t *setup = (GammaCaptureSetup_t *)NULL;

    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "Capture = %u releasing previous node", GetID() );

    // Take the lock to ensure that CaptureUpdateHW() cannot be executed while we update the Queue
    if (vibe_os_down_semaphore(m_QueueLock) != 0 )
      return -EINTR;

    pNode = m_CaptureQueue->GetNextNode(0);

    vibe_os_up_semaphore(m_QueueLock);

    while(pNode != 0)
    {
      /* Check if it is the requested node for release */
      setup = (GammaCaptureSetup_t *)m_CaptureQueue->GetNodeData(pNode);
      if(setup && setup->buffer_address == (uint32_t)pBuffer)
        break;
      pNode = m_CaptureQueue->GetNextNode(pNode);
    }

    if(!setup || !pNode) // nothing to release!
    {
      TRC( TRC_ID_ERROR, "Nothing to release?" );
      return 0;
    }

    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "Got node 0x%p for release", pNode );

    /* Check if the node is already processed by the hw before releasing it */
    vibe_os_wait_queue_event(m_WaitQueueEvent, &setup->buffer_processed, 1, STMIOS_WAIT_COND_EQUAL, CAPTURE_RELEASE_WAIT_TIMEOUT);
    TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "Releasing Node %p (buffer = 0x%x | processed? (%d) )", pNode, setup->buffer_address, setup->buffer_processed );

    // Take the lock to ensure that CaptureUpdateHW() cannot be executed while we release the node
    if (vibe_os_down_semaphore(m_QueueLock) != 0 )
      return -EINTR;

    m_CaptureQueue->ReleaseNode(pNode);
    m_NumberOfQueuedBuffer--;

    if(!m_NumberOfQueuedBuffer)
      m_hasBuffersToRelease = false;

    m_Statistics.PicReleased++;

    vibe_os_up_semaphore(m_QueueLock);
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  /*
   * We may have a suspend event in progress which did forcing the
   * release of this buffer.
   */
  return m_bIsSuspended ? (-EAGAIN) : 0;
}


int CGammaCompositorCAP::NotifyEvent(capture_node_h pNode, stm_pixel_capture_time_t vsyncTime)
{
  int res=0;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  if(pNode && m_CaptureQueue->IsValidNode(pNode))
  {
    GammaCaptureSetup_t *setup = (GammaCaptureSetup_t *)m_CaptureQueue->GetNodeData(pNode);
    if(setup)
    {
      /* Mark this buffer as processed */
      setup->buffer_processed = 1;

      /* Set back the new node data */
      m_CaptureQueue->SetNodeData(pNode,setup);

      m_Statistics.PicCaptured++;

      /*
       * Fill Capture Time data
       */
      if(setup->buffer_address)
      {
        stm_pixel_capture_buffer_descr_t *pBuffer = (stm_pixel_capture_buffer_descr_t *)setup->buffer_address;
        /* Buffer filling started at the begin of previous TOP VSync time */
        pBuffer->captured_time = vsyncTime - m_FrameDuration;
      }

      if (setup->i_buffer.buffer_filled)
      {
        struct stm_i_capture_filled_buffer filled_buffer;

        vibe_os_zero_memory(&filled_buffer, sizeof(struct stm_i_capture_filled_buffer));

        filled_buffer.buffer_desc = &(setup->i_buffer);
        /* Buffer filling started at the begin of previous TOP VSync time */
        filled_buffer.captured_time = vsyncTime - m_FrameDuration;
        filled_buffer.content_is_valid = true;

        setup->i_buffer.buffer_filled(setup->i_buffer.user_data, &filled_buffer);

        /* Release the the allocated compo capture node */
        m_CaptureQueue->ReleaseNode(pNode);

        m_NumberOfQueuedBuffer--;

        if(!m_NumberOfQueuedBuffer)
          m_hasBuffersToRelease = false;

        m_Statistics.PicReleased++;
      }

      /* Notify captured buffer */
      res = CCapture::NotifyEvent();

      TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "'Notify Node %p (buffer = 0x%x | processed? (%d) )'", pNode, setup->buffer_address,setup->buffer_processed );
    }
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return res;
}


void CGammaCompositorCAP::writeFieldSetup(const GammaCaptureSetup_t *setup, bool isTopField)
{
  uint32_t  *regval;
  uint32_t   i,Add;

  TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "%s field !", isTopField ? "TOP" : "BOT" );

  WriteCaptureReg(GAM_CAPn_CWO , setup->CWO);
  WriteCaptureReg(GAM_CAPn_CWS , setup->CWS);
  WriteCaptureReg(GAM_CAPn_VTP , setup->VTP);
  WriteCaptureReg(GAM_CAPn_VBP , setup->VBP);
  WriteCaptureReg(GAM_CAPn_PMP , setup->PMP);
  WriteCaptureReg(GAM_CAPn_CMW , setup->CMW);

  if (setup->CTL & CAP_CTL_EN_H_RESIZE)
  {
    WriteCaptureReg(GAM_CAPn_HSRC , setup->HSRC);

    /* Set horizontal filter coeffs */
    if (setup->HSRC & CAP_HSRC_FILTER_EN)
    {
      Add = GAM_CAPn_HFC0;
      regval = (uint32_t *)setup->HFCn;
      if(regval)
      {
        for( i=0; i<(NB_HSRC_COEFFS/4); i++, Add+=4, regval+=1)
        {
          TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "'GAM_CAPn_HFC%d \t = 0x%08x'", i, *regval );
          WriteCaptureReg(Add,*regval);
        }
      }
    }
  }

  if (setup->CTL & CAP_CTL_EN_V_RESIZE)
  {
    WriteCaptureReg(GAM_CAPn_VSRC , setup->VSRC);

    /* Set vertical filter coeffs */
    if (setup->VSRC & CAP_VSRC_FILTER_EN)
    {
      Add = GAM_CAPn_VFC0;
      regval = (uint32_t *)setup->VFCn;
      if(regval)
      {
        for( i=0; i<(NB_VSRC_COEFFS/4); i++, Add+=4, regval+=1)
        {
          TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "'GAM_CAPn_VFC%d \t = 0x%08x'", i, *regval );
          WriteCaptureReg(Add,*regval);
        }
      }
    }
  }

  if (m_CompoHardwareFeatures.conversion_matrix
      && setup->CTL & CAP_CTL_RGB2YCBCR_ENA)
  {
    /* Set color conversion matrix coeffs */
    Add = GAM_CAPn_MX0;
    regval = (uint32_t *)setup->MXn;
    if(regval)
    {
      for( i=0; i<(MATRIX_ENTRY_SIZE/4); i++, Add+=4, regval+=1)
      {
        TRC( TRC_ID_COMPO_CAPTURE_DEBUG, "GAM_CAPn_MX%d \t = 0x%08x", i, *regval );
        WriteCaptureReg(Add,*regval);
      }
    }
  }

  /*
   * For Tunneled compo capture we update CTL register only if we have
   * changed configuration.
   *
   * For Single Shot capture mode we need to always program the CTL register.
   */
  if(IsAttached())
  {
    if(setup->CTL != m_ulCaptureCTL)
    {
      m_ulCaptureCTL = setup->CTL;
      WriteCaptureReg(GAM_CAPn_CTL , m_ulCaptureCTL);
    }
  }
  else
  {
    WriteCaptureReg(GAM_CAPn_CTL , setup->CTL);
  }

  m_Statistics.PicInjected++;

#if defined(CAPTURE_DEBUG_REGISTERS)
  if(ctx == 1000)
  {
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_CTL \t = 0x%08x'",  ReadCaptureReg(GAM_CAPn_CTL) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_CWO \t = 0x%08x'",  ReadCaptureReg(GAM_CAPn_CWO) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_CWS \t = 0x%08x'",  ReadCaptureReg(GAM_CAPn_CWS) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_VTP \t = 0x%08x'",  ReadCaptureReg(GAM_CAPn_VTP) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_VBP \t = 0x%08x'",  ReadCaptureReg(GAM_CAPn_VBP) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_PMP \t = 0x%08x'",  ReadCaptureReg(GAM_CAPn_PMP) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_CMW \t = 0x%08x'",  ReadCaptureReg(GAM_CAPn_CMW) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_HSRC \t = 0x%08x'", ReadCaptureReg(GAM_CAPn_HSRC) );
    TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_VSRC \t = 0x%08x'", ReadCaptureReg(GAM_CAPn_VSRC) );

    for(i=0; i < NB_HSRC_COEFFS/4; i++)
      TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_HFC%d \t = 0x%08x'", i, ReadCaptureReg((GAM_CAPn_HFC0 + (4*i))) );

    for(i=0; i < NB_VSRC_COEFFS/4; i++)
      TRC( TRC_ID_COMPO_CAPTURE_REG, "'GAM_CAPn_VFC%d \t = 0x%08x'", i, ReadCaptureReg((GAM_CAPn_VFC0 + (4*i))) );

    ctx = 0;
  }
  ctx++;
#endif
}


int CGammaCompositorCAP::Start(void)
{
  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  if(!m_isStarted)
  {
    /* Do not start hw right now!! */
    CCapture::Start();
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return 0;
}


int CGammaCompositorCAP::Stop(void)
{
  int res;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  if(m_isStarted)
  {
    /*
     * Take the lock to ensure that CaptureUpdateHW() cannot be executed
     * while we are stopping the hardware and flushing the node queue.
     */
    while ((res = vibe_os_down_semaphore(m_QueueLock)) != 0)
    {
      if(res == -ERESTARTSYS)
      {
        /* Ignore error following kill signal */
        continue;
      }
      else
      {
        /*
         * Something was happening while trying to aquire the lock! Exit
         * with error!
         */
        return res;
      }
    }

    /* Ask harware to stop processing during next VSync */
    uint32_t ulCtrl = ReadCaptureReg(GAM_CAPn_CTL);
    ulCtrl &= ~CAP_CTL_CAPTURE_ENA;
    WriteCaptureReg(GAM_CAPn_CTL , ulCtrl);

    vibe_os_up_semaphore(m_QueueLock);

    CCapture::Stop();
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return 0;
}


void CGammaCompositorCAP::ProcessNewCaptureBuffer(stm_pixel_capture_time_t vsyncTime, uint32_t timingevent)
{
  capture_node_h       nodeCandidateForCapture = 0;
  bool                 isTopField=true;
  bool                 isInputInterlaced=false;

  // Take the lock to ensure that CaptureUpdateHW() cannot be executed while we update the Queue
  if (vibe_os_down_semaphore(m_QueueLock) != 0 )
    goto nothing_to_process_in_capture;

  isTopField        = !!(timingevent & STM_TIMING_EVENT_TOP_FIELD);
  isInputInterlaced = !!((m_InputParams.flags & STM_PIXEL_CAPTURE_BUFFER_INTERLACED) == STM_PIXEL_CAPTURE_BUFFER_INTERLACED);

  TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "isTopField = %d  | isInputInterlaced = %d", (int)isTopField, (int)isInputInterlaced);

  if ((!m_isStarted) || (m_bIsSuspended))
  {
    // The driver is suspended or stopped
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "The driver is %s!", m_bIsSuspended ? "suspended":"stopped");

    goto nothing_to_process_in_capture;
  }

  /*
   * Invalidate Nodes when VSync signal occurs.
   */
  m_Statistics.PicVSyncNb++;

  if (m_previousNode && (!m_CaptureQueue->IsValidNode(m_previousNode)) )
  {
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Error invalid m_previousNode 0x%p!",m_previousNode );
    m_previousNode = 0;
  }

  if (m_currentNode && (!m_CaptureQueue->IsValidNode(m_currentNode)) )
  {
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Error invalid m_currentNode 0x%p!",m_currentNode );
    m_currentNode = 0;
  }

  if (m_pendingNode && (!m_CaptureQueue->IsValidNode(m_pendingNode)) )
  {
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Error invalid m_pendingNode 0x%p!",m_pendingNode);
    m_pendingNode = 0;
  }

  /*
   * Compo capture is producing frames even if the input is interlaced.
   * The driver should ensure to start the capture hardware within next
   * TOP VSync. Therefore we are doing the register programming during
   * the BOTTOM Vsync and we are skip processing if it is TOP field in
   * Interlaced input mode).
   */
  if(isTopField || !isInputInterlaced)
  {
     /*
     * If there was an already processed buffer then it should be notified
     * during TOP field when capture isn't accessing anymore memory for
     * this buffer.
     */
    if((m_previousNode) && m_CaptureQueue->IsValidNode(m_previousNode))
    {
      // Push back the previous node if it was not yet done during capture
      // event.
      if(PushCapturedNode(m_previousNode, vsyncTime, 1, true) < 0)
      {
        /* Skip this event */
        TRC( TRC_ID_ERROR, "Push Captured Node failed!!!" );
        goto skip_capture_event;
      }
      m_previousNode = 0;
      m_Statistics.PicCaptured++;
    }

    if(isInputInterlaced)
    {
      /* unblock waiting dequeue calls */
      vibe_os_wake_up_queue_event(m_WaitQueueEvent);

      vibe_os_up_semaphore(m_QueueLock);
      return;
    }
  }

  // A new TOP VSync has happen so:
  //  - the PendingNode is going to become the CurrentNode
  //  - the CurrentNode is going to be released (if the pending picture is a new one)
  //  - a new node is going to be prepared for next VSync and will become the PendingNode
  CCapture::UpdateCurrentNode(vsyncTime);

  // The current node become the previous node so it will notified later
  m_previousNode = m_currentNode;

  // The pending node become the current node
  m_currentNode  = m_pendingNode;

  // The pending node become the current node
  m_pendingNode = 0;

  // Now find a candidate for next VSync
  if (m_currentNode == 0)
  {
    // No picture was currently captured: Get the first picture of the queue
    nodeCandidateForCapture = m_CaptureQueue->GetNextNode(0);

    if (nodeCandidateForCapture)
      TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "No buffer currently queued. Use the 1st node of the queue: %p", nodeCandidateForCapture );
  }
  else
  {
    // Get the next buffer in the queue
    nodeCandidateForCapture = m_CaptureQueue->GetNextNode(m_currentNode);

    if (nodeCandidateForCapture)
      TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Move to next node: %p", nodeCandidateForCapture );
  }

  if (!nodeCandidateForCapture)
  {
    if (GetCaptureNode(nodeCandidateForCapture, true) < 0)
    {
      /* Skip this event */
      TRC( TRC_ID_ERROR, "Get Capture Node failed!!!" );
      goto nothing_to_process_in_capture;
    }
  }

  if((nodeCandidateForCapture) && m_CaptureQueue->IsValidNode(nodeCandidateForCapture))
  {
    stm_i_push_get_sink_get_desc_t   *pGetBufferDesc = (stm_i_push_get_sink_get_desc_t *)m_CaptureQueue->GetNodeData(nodeCandidateForCapture);
    stm_pixel_capture_buffer_descr_t  CaptureBuffer = { 0 };
    CaptureQueueBufferInfo            qbinfo        = { 0 };
    GammaCaptureSetup_t              *pCaptureSetup = (GammaCaptureSetup_t *)NULL;

    if(pGetBufferDesc)
    {
      TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "'Capture will be processing Node (%p) and Buffer (%p) for the new VSync'", nodeCandidateForCapture, (void *)pGetBufferDesc);

      if(pGetBufferDesc->video_buffer_addr == 0)
      {
        TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Got Node with invalid buffer address (%p)", m_Sink);
        /* Release the node */
        m_CaptureQueue->ReleaseNode(nodeCandidateForCapture);
        m_Statistics.PicReleased++;

        if (GetCaptureNode(nodeCandidateForCapture, true) < 0)
        {
          TRC( TRC_ID_ERROR, "Error in GetCapturedNode(%p)", m_Sink);
          // Nothing to process! Skip this event.
          goto nothing_to_process_in_capture;
        }
        pGetBufferDesc = (stm_i_push_get_sink_get_desc_t *)m_CaptureQueue->GetNodeData(nodeCandidateForCapture);
      }

      /*
       * Setup the capture buffer format and descriptor according to new push
       * buffer descriptor data.
       */
      if(pGetBufferDesc)
      {
        CaptureBuffer.cap_format     = m_CaptureFormat;
        CaptureBuffer.bytesused      = pGetBufferDesc->height * pGetBufferDesc->pitch;
        CaptureBuffer.length         = pGetBufferDesc->height * pGetBufferDesc->pitch;
        if(CaptureBuffer.cap_format.format == STM_PIXEL_FORMAT_RGB_8B8B8B_SP)
        {
          CaptureBuffer.rgb_address   = pGetBufferDesc->video_buffer_addr;
        }
        else
        {
          CaptureBuffer.luma_address   = pGetBufferDesc->video_buffer_addr;
          CaptureBuffer.chroma_offset  = pGetBufferDesc->chroma_buffer_offset;
        }
        CaptureBuffer.captured_time  = vsyncTime + m_FrameDuration;

        if(!GetQueueBufferInfo(&CaptureBuffer, qbinfo))
        {
          TRC( TRC_ID_ERROR, "Failed to get new buffer queue info data");
          goto skip_capture_event;
        }

        if(!AdjustBufferInfoForScaling(&CaptureBuffer, qbinfo))
        {
          TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Failed to get adjust new buffer for scaling");
          goto skip_capture_event;
        }

        TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "one = 0x%x hsrcinc = 0x%x chsrcinc = 0x%x",m_fixedpointONE, qbinfo.hsrcinc,qbinfo.chroma_hsrcinc );
        TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "one = 0x%x vsrcinc = 0x%x cvsrcinc = 0x%x",m_fixedpointONE, qbinfo.vsrcinc,qbinfo.chroma_vsrcinc );

        // Prepare capture
        pCaptureSetup = PrepareCaptureSetup(&CaptureBuffer, qbinfo);
        if(!pCaptureSetup)
        {
          TRC( TRC_ID_ERROR, "Failed to allocate pCaptureSetup" );
          goto skip_capture_event;
        }

        /*
         * Configure the capture Colour Format.
         */
        if(!ConfigureCaptureColourFmt(&CaptureBuffer, qbinfo, pCaptureSetup))
        {
          TRC( TRC_ID_ERROR, "Failed to configure capture buffer format" );
          vibe_os_free_memory(pCaptureSetup);
          m_CaptureQueue->ReleaseNode(nodeCandidateForCapture);
          goto skip_capture_event;
        }

        /*
         * Configure the capture Resize and Filter.
         */
        if(!ConfigureCaptureResizeAndFilters(&CaptureBuffer, qbinfo, pCaptureSetup))
        {
          TRC( TRC_ID_ERROR, "Failed to configure capture resize and filters" );
          vibe_os_free_memory(pCaptureSetup);
          m_CaptureQueue->ReleaseNode(nodeCandidateForCapture);
          goto skip_capture_event;
        }

        /* Configure the capture source size */
        ConfigureCaptureWindowSize(&CaptureBuffer, qbinfo, pCaptureSetup);

        /* Configure the capture buffer (memory) size */
        ConfigureCaptureBufferSize(&CaptureBuffer, qbinfo, pCaptureSetup);

        /* Configure the capture input */
        ConfigureCaptureInput(&CaptureBuffer, qbinfo, pCaptureSetup);

        /* Write capture cfg */
        writeFieldSetup(pCaptureSetup, true);

        /* Setup is no more needed */
        vibe_os_free_memory(pCaptureSetup);

        SetPendingNode(nodeCandidateForCapture);
      }
    }
  }

  vibe_os_up_semaphore(m_QueueLock);

  /* unblock waiting dequeue calls */
  vibe_os_wake_up_queue_event(m_WaitQueueEvent);

  return;

skip_capture_event:
  m_Statistics.PicSkipped++;
  vibe_os_up_semaphore(m_QueueLock);

  /* unblock waiting dequeue calls */
  vibe_os_wake_up_queue_event(m_WaitQueueEvent);

  return;

nothing_to_process_in_capture:
  {
    // The Capture is disabled to avoid writing an invalid buffer
    uint32_t ulCtrl = ReadCaptureReg(GAM_CAPn_CTL);
    ulCtrl &= ~CAP_CTL_CAPTURE_ENA;
    WriteCaptureReg(GAM_CAPn_CTL , ulCtrl);

    /* Invalidate previous CTL value */
    m_ulCaptureCTL = CAPTURE_HW_INVALID_CTL;
  }

  /* unblock waiting dequeue calls */
  vibe_os_wake_up_queue_event(m_WaitQueueEvent);

  vibe_os_up_semaphore(m_QueueLock);
}


void CGammaCompositorCAP::CaptureUpdateHW(stm_pixel_capture_time_t vsyncTime, uint32_t timingevent)
{
  capture_node_h       nodeCandidateForCapture = 0;
  GammaCaptureSetup_t *setup                   = (GammaCaptureSetup_t *)NULL;
  bool                 isTopField=true;
  bool                 isInputInterlaced=false;

  TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "vsyncTime = %llu", vsyncTime );
  m_Statistics.PicVSyncNb++;

  isTopField        = !!(timingevent & STM_TIMING_EVENT_TOP_FIELD);
  isInputInterlaced = !!((m_InputParams.flags & STM_PIXEL_CAPTURE_BUFFER_INTERLACED) == STM_PIXEL_CAPTURE_BUFFER_INTERLACED);

  // Take the lock to ensure that CaptureUpdateHW() cannot be executed while we update the Queue
  if ( vibe_os_down_semaphore(m_QueueLock) != 0 )
    return;

  if (!m_CaptureQueue)
  {
    // The queue is not yet created
    TRC( TRC_ID_ERROR, "The queue is not yet created!" );
    goto nothing_to_inject_in_capture;
  }

  // Check node validity
  if (m_previousNode && (!m_CaptureQueue->IsValidNode(m_previousNode)) )
  {
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Error invalid m_previousNode 0x%p!",m_previousNode );
    m_previousNode = 0;
  }

  // Check node validity
  if (m_currentNode && (!m_CaptureQueue->IsValidNode(m_currentNode)) )
  {
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Error invalid m_currentNode 0x%p!",m_currentNode );
    m_currentNode = 0;
  }

  if (m_pendingNode && (!m_CaptureQueue->IsValidNode(m_pendingNode)) )
  {
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Error invalid m_pendingNode 0x%p!",m_pendingNode );
    m_pendingNode = 0;
  }

  if ((!m_isStarted) || (m_bIsSuspended))
  {
    // The driver is suspended or stopped
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "The driver is %s!", m_bIsSuspended ? "suspended":"stopped");

    /* For non tunneled capture ... notify previous and current node before existing */
    if(m_currentNode != 0)
    {
      NotifyEvent(m_currentNode, vsyncTime);
      m_currentNode = 0;
    }
    goto nothing_to_inject_in_capture;
  }

  // A new BOTTOM VSync has happen so:
  //  - the pendingNode is going to become the currentNode
  //  - the currentNode is going to be notfied within next TOP VSync
  //  - a new node is going to be prepared for next VSync and will become the pendingNode

  m_previousNode = m_currentNode;
  m_currentNode = m_pendingNode;
  /*
   * Compo capture is producing frames even if the input is interlaced.
   * The driver should ensure to start the capture hardware within next
   * TOP VSync. Therefore we are doing the register programming during
   * the BOTTOM Vsync and we are skip processing if it is TOP field in
   * Interlaced input mode).
   */
  if(isTopField || !isInputInterlaced)
  {
     /*
     * If there was an already processed buffer then it should be notified
     * during TOP field when capture isn't accessing anymore memory for
     * this buffer.
     */
    if(m_previousNode != 0)
    {
      NotifyEvent(m_previousNode, vsyncTime);
      m_previousNode = 0;
    }

    if(isInputInterlaced)
    {
      /* unblock waiting dequeue calls */
      vibe_os_wake_up_queue_event(m_WaitQueueEvent);

      vibe_os_up_semaphore(m_QueueLock);
      return;
    }
  }

  CCapture::UpdateCurrentNode(vsyncTime);

  // Get the next buffer in the queue
  nodeCandidateForCapture = m_CaptureQueue->GetNextNode(m_currentNode);

  // No new node available do not update HW
  if (nodeCandidateForCapture == m_currentNode)
  {
    nodeCandidateForCapture = 0;
  }

  if (nodeCandidateForCapture)
  {
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "Move to next node: %p", nodeCandidateForCapture );
  }

  if(nodeCandidateForCapture)
  {
    setup = (GammaCaptureSetup_t *)m_CaptureQueue->GetNodeData(nodeCandidateForCapture);
    if(setup)
    {
      /* Don't re-write an already processed node! */
      if(setup->buffer_processed == 1)
      {
        m_pendingNode = 0;
        goto nothing_to_inject_in_capture;
      }

      writeFieldSetup(setup, true);

      SetPendingNode(nodeCandidateForCapture);

      TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "There are still %d valid node to process!", m_NumberOfQueuedBuffer);
    }
  }
  else
  {
    /* Stop hardware processing during next TOP field. */
    TRC( TRC_ID_COMPO_CAPTURE_UPDATE, "No more pending nodes to process... Will stop hardware!");
    m_pendingNode = 0;
    goto nothing_to_inject_in_capture;
  }

  /* unblock waiting dequeue calls */
  vibe_os_wake_up_queue_event(m_WaitQueueEvent);

  vibe_os_up_semaphore(m_QueueLock);
  return;

nothing_to_inject_in_capture:
  {
    // The Capture is disabled to avoid writing an invalid buffer
    uint32_t ulCtrl = ReadCaptureReg(GAM_CAPn_CTL);
    ulCtrl &= ~CAP_CTL_CAPTURE_ENA;
    WriteCaptureReg(GAM_CAPn_CTL , ulCtrl);

    /* Invalidate previous CTL value */
    m_ulCaptureCTL = CAPTURE_HW_INVALID_CTL;
  }

  /* unblock waiting dequeue calls */
  vibe_os_wake_up_queue_event(m_WaitQueueEvent);

  vibe_os_up_semaphore(m_QueueLock);
}


int CGammaCompositorCAP::SetInputParams(stm_pixel_capture_input_params_t params)
{
  int retval = -EINVAL;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  retval = CCapture::SetInputParams(params);
  if(!retval)
  {
    /*
     * Make sure that we are dealing with Capture Decimation which is now
     * enabled for UHD modes with high frame rate (i.e 2160P@60Hz) at
     * Mixer level.
     */
    if( (m_InputParams.src_frame_rate > 30000)
     && (m_InputParams.active_window.height > 1080) )
    {
      m_InputParams.active_window.width /= 2;
    }
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return retval;
}

int CGammaCompositorCAP::GetInputParams(stm_pixel_capture_input_params_t *params)
{
  int retval = -EINVAL;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  retval = CCapture::GetInputParams(params);
  if(!retval)
  {
    /*
     * Check if the Capture Decimation is now enabled at Mixer level.
     * We should return back original input params begin submitted
     * by the user when calling SetInputParams().
     */
    if( (params->src_frame_rate > 30000)
     && (params->active_window.height > 1080) )
    {
      params->active_window.width *= 2;
    }
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return retval;
}

int CGammaCompositorCAP::SetInputWindow(stm_pixel_capture_rect_t input_window)
{
  int retval = -EOPNOTSUPP;

  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  if( (m_InputParams.src_frame_rate > 30000)
   && (m_InputParams.active_window.height > 1080) )
  {
    input_window.width /= 2;
  }

  retval = CCapture::SetInputWindow(input_window);

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return retval;
}

int CGammaCompositorCAP::GetInputWindow(stm_pixel_capture_rect_t *input_window)
{
  TRCIN( TRC_ID_COMPO_CAPTURE, "" );

  CCapture::GetInputWindow(input_window);

  /*
   * Check if the Capture Decimation is now enabled at Mixer level.
   * We should return back original input window calling SetInputWindow().
   */
  if( (m_InputParams.src_frame_rate > 30000)
   && (m_InputParams.active_window.height > 1080) )
  {
    input_window->width *= 2;
  }

  TRCOUT( TRC_ID_COMPO_CAPTURE, "" );

  return 0;
}
