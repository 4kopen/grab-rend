/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef _PIXEL_CAPTURE_DEVICE_H__
#define _PIXEL_CAPTURE_DEVICE_H__

#include <stm_display_types.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <stm_pixel_capture.h>
#include <capture_device_priv.h>

class CCapture;


struct stm_pixel_capture_pm_s
{
  uint32_t id;
  uint32_t state;
  int (*runtime_get)(const uint32_t type, const uint32_t id);
  int (*runtime_put)(const uint32_t type, const uint32_t id);
};

struct stm_pixel_capture_s
{
  uint32_t                     handle;
  uint32_t                     type;
  void                       * lock;
  struct stm_pixel_capture_s * owner;

  /* device power struct */
  struct stm_pixel_capture_pm_s pm;
};

enum DevicePowerState {
  CAPTURE_DEVICE_ACTIVE,
  CAPTURE_DEVICE_ACTIVE_STANDBY   = 0x1,
  CAPTURE_DEVICE_SUSPENDED        = 0x2,
  CAPTURE_DEVICE_FROZEN           = 0x4
};

class CPixelCaptureDevice
{
public:
  CPixelCaptureDevice(stm_pixel_capture_device_type_t type);
  virtual ~CPixelCaptureDevice();

  // Create Device specific resources after initial mappings have
  // been completed.
  virtual bool Create(uint32_t base_address,uint32_t size, stm_pixel_capture_hw_features_t hw_features);

  // Access to Captures instances
  uint32_t  GetNumberOfCaptures(void)  const { return m_numCaptures; }
  CCapture* GetCapture(uint32_t index) const { return (index<m_numCaptures)?m_pCaptures[index]:0; }

  stm_pixel_capture_device_type_t GetCaptureType(void) const {return m_Type;}

  /*
   * Called after VSYNC interrupt to update the capture for the specified input.
   * This is done on CCapture so that the correct processing can be done
   * for all hardware clocked by a specific timing generator.
   */
  void UpdateCaptureDevice(uint32_t timingID, stm_pixel_capture_time_t vsyncTime, uint32_t timingevent);

  /*
   * Power Management stuff.
   */
  virtual int Freeze(void);
  virtual int Suspend(void);
  virtual int Resume(void);

protected:

  CCapture             **m_pCaptures;    // Available capture instances
  uint32_t               m_numCaptures;  // Number of Captures

  stm_pixel_capture_device_type_t m_Type;             // Capture device type (COMPO-CAP or DVP-CAP)

  stm_pixel_capture_time_t        m_LastVTGEventTime;

  /*
   * Helper for capture creation code to add an input to the device's list of
   * inputs.
   */
  bool AddCapture(CCapture *);
  void RemoveCaptures(void);

  /*
   * Helper for stm_registry management code to add capture objects to the
   * registry's database.
   */
  void RegistryInit(void);
  void RegistryTerm(void);

  virtual bool CreateCaptures(uint32_t base_address,uint32_t size,stm_pixel_capture_hw_features_t hw_features);

  // Power Managment state
  bool               m_bIsSuspended; // Has the HW been powered off
  bool               m_bIsFrozen;
}; /* CPixelCaptureDevice */


/*
 * There will be one device specific implementation of this function in each
 * valid driver configuration. This is how you get hold of the device object
 */
extern CPixelCaptureDevice* AnonymousCreateCaptureDevice(stm_pixel_capture_device_type_t type);

#endif /* _PIXEL_CAPTURE_DEVICE_H__ */
