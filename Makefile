modules modules_install help clean tests tests_install:
	$(MAKE) -C linux $@

##############################
# Headers management
# Install kpi headers to staging area with same sudirs
KPI_HEADERS_1_INSTALL_DIR=$(KERNEL_STAGING_DIR)/include
KPI_HEADERS_1 :=
KPI_HEADERS_1 += $(wildcard include/*.h)
KPI_HEADERS_1 += $(wildcard private/include/*.h)
KPI_HEADERS_1_DST := $(addprefix $(KPI_HEADERS_1_INSTALL_DIR)/,$(notdir $(KPI_HEADERS_1)))

VPATH :=
VPATH += include
VPATH += private/include
VPATH += linux/include
VPATH += linux/kernel/include/linux/stm


module_install_headers: $(KPI_HEADERS_1_DST)

$(KPI_HEADERS_1_INSTALL_DIR)/%: %
	install --mode=644 -D $< $@


module_clean_headers:
	$(RM) $(KPI_HEADERS_1_DST)

###############################
# Userspace headers

USERSPACE_HEADERS_INSTALL_DIR = $(SYSROOT_PREFIX)/usr/include/linux

USERSPACE_HEADERS_LIST :=

install_userspace_headers: $(USERSPACE_HEADERS_LIST)

$(USERSPACE_HEADERS_INSTALL_DIR)/%: %
	install --mode=644 -D $< $@

clean_userspace_headers:
	$(RM) $(USERSPACE_HEADERS_LIST)


# end of file
