/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


/******************************************************************************
  I N C L U D E    F I L E S
******************************************************************************/
/* Standard Includes ----------------------------------------------*/
#include <linux/types.h>
#include <linux/platform_device.h>
#include <linux/stm/gpio.h>
#include <linux/stm/stih418.h>

/* Local Includes -------------------------------------------------*/
#include <hdmirxplatform.h>
#include "stddefs_hdmirx.h"

/**************************C O D E**************************************/
stm_hdmirx_platform_route_t stm_hdmirx_route_data[] =
{
  {
    .id = 0,
    .phy = {
      .start_addr = 0x08F80000,
      .end_addr = 0x08F87FFF,
    },
    .core = {
      .start_addr = 0x08D10000,
      .end_addr = 0x08D17fff,
    },
    .clock_gen = {
      .start_addr = 0x08D10000, //??
      .end_addr = 0x08D17fff, //??
      .audio_clk_gen_id = 1,
      .video_clk_gen_id = 0,
    },
    .irq_num = STIH418_IRQ(110),
    .irq_csm_num = STIH418_IRQ(140),
    .output_pixel_width = STM_HDMIRX_ROUTE_OUTPUT_PIXEL_WIDTH_10_BITS,
    .i2s_out_clk_scale_factor = 256,
    .rterm_mode = 0x3,
    .rterm_val  = 0x3,
  },
};
stm_hdmirx_platform_soc_t stm_hdmirx_soc_data =
{
  .num_routes = ARRAY_SIZE(stm_hdmirx_route_data),
  .route = stm_hdmirx_route_data,
  .csm = {
    .start_addr = 0x094A0770,
    .end_addr = 0x094A476F,
  },
  .meas_clk_freq_hz = 30000000,
};

stm_hdmirx_platform_port_t stm_hdmirx_port_data[] =
{
  {
    .id = 0,
    .csm_port_id = {0, 0, 1},
    .pd_config = {
      .option = STM_HDMIRX_POWER_DETECT_OPTION_GPIO,
      .pin.pd_pio = stm_gpio(STIH418_GPIO(5), 5),
    },
    .hpd_pio = stm_gpio(STIH418_GPIO(5), 4),
    .scl_pio = stm_gpio(STIH418_GPIO(5), 7),
    .sda_pio = stm_gpio(STIH418_GPIO(5), 6),
    .edid_wp = stm_gpio(STIH418_GPIO(2), 5),
    .route_connectivity_mask = 0x3,
    .internal_edid = FALSE,
    .enable_hpd = TRUE,
    .enable_ddc2bi = FALSE,
    .ext_mux = FALSE,
    .eq_mode = STM_HDMIRX_ROUTE_EQ_MODE_MEDIUM_GAIN,
    .eq_config = {
      .low_freq_gain = 0x7,
      .high_freq_gain = 0x0,
    },
    .op_mode = STM_HDMIRX_ROUTE_OP_MODE_AUTO,
  },

};

stm_hdmirx_platform_board_t stm_hdmirx_board_data =
{
  .num_ports = ARRAY_SIZE(stm_hdmirx_port_data),
  .port = stm_hdmirx_port_data,
  .set_ext_mux = NULL,
  .ext_mux_i2c.bus = 2,
  .ext_mux_i2c.dev_addr = 0xAE,	// A0  and then right shift while assign
};

stm_hdmirx_platform_data_t stm_hdmirx_platform_data =
{
  .board = &stm_hdmirx_board_data,
  .soc = &stm_hdmirx_soc_data,
};

stm_hdmirx_platform_data_t * stmhdmirx_get_platform_data(void)
{
  return (&stm_hdmirx_platform_data);
}
