/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/



#include <linux/kernel.h>
#include <linux/export.h>

#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/slab.h>   /* Needed for kmalloc and kfree */
#include <asm/io.h>

#include <linux/bpa2.h>
#include <linux/fb.h>
#include <linux/io.h>

#include <stm_pixel_capture.h>

#include <vibe_debug.h>

/*#include <capture.h>*/

static int   cap_type      = 0;
static int   kpi_test      = 0;

module_param(cap_type, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(cap_type, "Set the capture device type : 0 = COMPO | 1 = DVP");

module_param(kpi_test, int, S_IRUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(kpi_test, "Execute in KPI test mode : 0 = Normal Mode | 1 = KPI Test Mode");

extern int  pixel_capture_display_start(uint32_t number_buffers, stm_pixel_capture_device_type_t type);
extern void pixel_capture_display_stop(void);

/* Simple test case to capture main mixer content to memory */

static int __init stm_capturetest_init(void)
{
  struct fb_info *info = NULL;
  stm_pixel_capture_device_type_t  type;
  stm_pixel_capture_buffer_descr_t c_buffer;
  stm_pixel_capture_h pixel_capture = NULL;
  const char *input_name;
  uint32_t input_id = 0;
  int res=0;

  memset(&c_buffer, 0, sizeof(stm_pixel_capture_buffer_descr_t));

  switch (cap_type)
  {
    case 1:
      type = STM_PIXEL_CAPTURE_DVP;
      break;
    case 0:
    default:
      type = STM_PIXEL_CAPTURE_COMPO;
      break;
  }

  /* Now get the Aux display buffer to be used by capture */
  res = num_registered_fb - 1;
  if (registered_fb[res])
  {
    info = registered_fb[res];
    printk ("found a registered framebuffer @ %p\n", info);
  }

  if (info)
  {
    struct module *owner = info->fbops->owner;

    if (try_module_get (owner))
    {
      printk ("try_module_get\n");
      if (info->fbops->fb_open && !info->fbops->fb_open (info, 0))
      {
        switch (info->var.bits_per_pixel)
        {
          case 16: c_buffer.cap_format.format = STM_PIXEL_FORMAT_RGB565; break;
          case 24: c_buffer.cap_format.format = STM_PIXEL_FORMAT_RGB888; break;
          case 32: c_buffer.cap_format.format = STM_PIXEL_FORMAT_ARGB8888; break;
          default:
            if (info->fbops->fb_release)
              info->fbops->fb_release (info, 0);
            module_put (owner);
            info = NULL;
            break;
        }

        if ((c_buffer.cap_format.format != STM_PIXEL_FORMAT_NONE) && ( info != NULL ))
        {
          c_buffer.rgb_address = info->fix.smem_start;
          /*buffer_size = info->fix.smem_len;*/
          c_buffer.cap_format.stride = info->var.xres * info->var.bits_per_pixel / 8;
          c_buffer.cap_format.width = info->var.xres;
          c_buffer.cap_format.height = info->var.yres_virtual;
          c_buffer.bytesused = c_buffer.cap_format.stride * c_buffer.cap_format.height;
          c_buffer.length = c_buffer.cap_format.stride * c_buffer.cap_format.height;
          printk ("fb: %x %u %u %u %u\n", c_buffer.rgb_address, c_buffer.bytesused, c_buffer.cap_format.stride,
                   c_buffer.cap_format.width, c_buffer.cap_format.height);
        }
      }
      else
      {
        printk ("no fb_open()\n");
        info = NULL;
      }
      module_put (owner);
    }
    else
    {
      printk ("failed try_module_get\n");
      info = NULL;
    }
  }

  if(info == NULL)
  {
    printk ("Failed to Start Pixel Capture tests...\n");
    return -EINVAL;
  }

  printk ("Starting Pixel Capture STKPI tests...\n");

  if (kpi_test) {
    stm_pixel_capture_status_t status;
    stm_pixel_capture_buffer_format_t format;
    stm_pixel_capture_rect_t   input_window;
    stm_pixel_capture_input_params_t input_params;
    stm_pixel_capture_params_t capture_stream_params;
    uint32_t temp;

    /* STKPI Basic test */
    res = stm_pixel_capture_open(type, 0, &pixel_capture);
    if(res < 0)
      printk ("ERROR : can't get a pixel capture handle with id = %d | err = %d\n",0, res);

    printk("stm_pixel_capture_set_input --> %d\n",input_id);
    res = stm_pixel_capture_set_input(pixel_capture, input_id);
    if(res < 0)
      printk ("ERROR : can't set a capture input with id = %d | err = %d\n",input_id, res);

    res = stm_pixel_capture_get_input(pixel_capture, &input_id);
    if(res < 0)
      printk ("ERROR : can't get a capture input with id = %d | err = %d\n",input_id, res);
    printk("stm_pixel_capture_get_input <-- %d\n",input_id);

    res = stm_pixel_capture_enum_inputs(pixel_capture, input_id, &input_name);
    if(res < 0)
      printk ("ERROR : can't get a capture input name with id = %d | err = %d\n",input_id, res);

    printk("Capture Input %d name is (%s)\n",input_id, input_name);
    res = stm_pixel_capture_get_status( pixel_capture, &status);
    if (res < 0)
      printk("ERROR : can't get pixel capture status | err = %d\n", res);
    else
      printk("INFO : Pixel Capture status is : %08x\n", (uint32_t)status);
    res = stm_pixel_capture_get_format( pixel_capture, &format);
    if (res < 0)
      printk("ERROR : can't get pixel capture buffer format | err = %d\n", res);
    else
      printk("INFO : Pixel Capture buffer format is :\n\t"
         "width  : %d\n"
         "height : %d\n"
         "stride : %d\n"
         "format : %d\n", format.width, format.height, format.stride, format.format);
    res = stm_pixel_capture_get_input_window( pixel_capture, &input_window);
    if (res < 0)
      printk("ERROR : can't get pixel capture input window | err = %d\n", res);
    else
      printk("INFO : Pixel Capture input window is :\n\t"
         "x  : %d\n"
         "y : %d\n"
         "width : %d\n"
         "height : %d\n", input_window.x, input_window.y, input_window.width, input_window.height);
    res = stm_pixel_capture_get_input_params( pixel_capture, &input_params);
    if (res < 0)
      printk("ERROR : can't get pixel capture input params | err = %d\n", res);
    else
      printk("INFO : Pixel Capture input params are :\n\t"
         "htotal  : %d\n"
         "vtotal : %d\n"
         "vsync_width : %d\n"
         "src_frame_rate : %d\n"
         "pixel_format : %d\n...\n", input_params.htotal, input_params.vtotal, input_params.vsync_width, input_params.src_frame_rate, input_params.pixel_format);
    res = stm_pixel_capture_get_stream_params( pixel_capture, &capture_stream_params);
    if (res < 0)
      printk("ERROR : can't get pixel capture stream params | err = %d\n", res);
    else
      printk("INFO : Pixel Capture is happening at %d useconds per frame\n", (uint32_t)capture_stream_params.timeperframe);
    capture_stream_params.timeperframe++; //watch out : timeperframe may not be used non-initialized here in case the previous kpi call stm_pixel_capture_get_stream_params fails
    res = stm_pixel_capture_set_stream_params( pixel_capture, capture_stream_params);
    if (res < 0)
      printk("ERROR : can't set pixel capture stream params | err = %d\n", res);
    else {
      temp = (uint32_t)capture_stream_params.timeperframe;
      stm_pixel_capture_get_stream_params( pixel_capture, &capture_stream_params);
      printk("INFO : Pixel Capture is now happening at %d useconds per frame (%s) \n",
        (uint32_t)capture_stream_params.timeperframe,
        temp == capture_stream_params.timeperframe ? "KPI Call was Ok" : "KPI Call didn't set timeperframe as requested");
    }
    stm_pixel_capture_close(pixel_capture);
    return res;
  }
  res = pixel_capture_display_start(0, type);
  if(res < 0)
    printk ("ERROR : can't execute extra tests err = %d\n", res);

  TRC(TRC_ID_MAIN_INFO, "Pixel Capture Test Display Started err = %d\n", res);
  return res;
}

static void __exit stm_capturetest_exit(void)
{
  printk ("Unloading Pixel Capture Test Module...\n");
  pixel_capture_display_stop();
}

/******************************************************************************
 *  Modularization
 */

#ifdef MODULE
module_init(stm_capturetest_init);
module_exit(stm_capturetest_exit);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Pixel Capture Test Module.");
MODULE_AUTHOR("Akram BEN BELGACEM <akram.ben-belgacem@st.com>");
#endif /* MODULE */
