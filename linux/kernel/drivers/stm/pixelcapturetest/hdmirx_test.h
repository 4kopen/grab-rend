/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef CAPTURE_TEST_HDMIRX_H
#define CAPTURE_TEST_HDMIRX_H

#ifdef HDMIRX_MODE_DETECTION_TEST

/* Standard Includes ----------------------------------------------*/
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/syscalls.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/fcntl.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/slab.h>
#include <linux/time.h>

/* Local Includes -------------------------------------------------*/
#include <stm_hdmirx.h>
#include <hdmirx_drv.h>
#include <vibe_debug.h>

#ifndef HDMI_TEST_PRINT
#define HDMI_TEST_PRINT(a,b...)  TRC(TRC_ID_DVP_CAPTURE_DEBUG, "stmhdmirx: %s: " a, __FUNCTION__ ,##b)
#endif

uint32_t hdmirx_mode_detection_test_start(stm_pixel_capture_input_params_t  *InputParams_p);
void hdmirx_mode_detection_test_stop(void);

#endif /* HDMIRX_MODE_DETECTION_TEST */

#endif /* CAPTURE_TEST_HDMIRX_H */
