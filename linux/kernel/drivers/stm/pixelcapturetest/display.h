/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef CAPTURE_TEST_DISPLAY_H
#define CAPTURE_TEST_DISPLAY_H

#include <stm_display_types.h>
#include <stm_pixel_capture.h>

struct stm_display_io_windows {
  stm_pixel_capture_rect_t  input_window;  // Capture Input Frame Rect
  stm_rect_t                output_window; // Capture Buffer size == Plane input size
};

struct stm_capture_display_context {
  stm_display_device_h        hDev;
  stm_display_output_h        hOutput;
  stm_display_plane_h         hPlane;
  stm_display_source_h        hSource;
  stm_display_source_queue_h  hQueue;

  struct stm_display_io_windows      io_windows;

  wait_queue_head_t           frameupdate;
  uint32_t                    timingID;
  int                         tunneling;
};

int setup_main_display(uint32_t timingID, stm_pixel_capture_input_params_t *InputParams,
                                  struct stm_capture_display_context      **pDisplayContext,
                                  struct stm_display_io_windows            io_windows,
                                  char *plane_name,
                                  int main_to_aux,
                                  int stream_inj_ena,
                                  int tunneling);

int display_fill_buffer_descriptor(stm_display_buffer_t * pDisplayBuffer,
                            stm_pixel_capture_buffer_descr_t CaptureBuffer,
                            stm_pixel_capture_input_params_t InputParams);

void free_main_display_ressources(struct stm_capture_display_context       *pDisplayContext);

#endif /* CAPTURE_TEST_DISPLAY_H */
