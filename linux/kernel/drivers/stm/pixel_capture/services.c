/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#include <linux/types.h>

#include <stmdisplay.h>

#include <vibe_os.h>


//////////////////////////////////////////////////////////////////////////////
// Services required by C++ code. These are typically part of the C++ run time
// but we dont have a run time!

void *_Znwj(unsigned int size)
{
  if(size>0)
    return vibe_os_allocate_memory(size);

  return NULL;
}

void *_Znaj(unsigned int size)
{
  if(size>0)
    return vibe_os_allocate_memory(size);

  return NULL;
}

void* __builtin_new(size_t size)
{
  if(size>0)
    return vibe_os_allocate_memory(size);

  return NULL;
}

void* __builtin_vec_new(size_t size)
{
  return __builtin_new(size);
}

void _ZdlPv(void *ptr)
{
  if(ptr)
    vibe_os_free_memory(ptr);
}

void _ZdaPv(void *ptr)
{
  if(ptr)
    vibe_os_free_memory(ptr);
}

void __builtin_delete(void *ptr)
{
  if(ptr)
    vibe_os_free_memory(ptr);
}

void __builtin_vec_delete(void* ptr)
{
  __builtin_delete(ptr);
}


void __pure_virtual(void)
{
}

void __cxa_pure_virtual(void)
{
}

