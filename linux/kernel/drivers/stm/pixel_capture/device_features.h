/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __BDISPII_DEVICE_FEATURES_H__
#define __BDISPII_DEVICE_FEATURES_H__

#include <linux/types.h>
#include <stm_pixel_capture.h>
#include <capture_device_priv.h>

struct _stm_capture_hw_features {
  char                     *name;
  stm_pixel_capture_device_type_t type;
  stm_pixel_capture_hw_features_t hw_features;
};

#endif /* __BDISPII_DEVICE_FEATURES_H__ */
