/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/kdev_t.h>
#include <linux/slab.h>
#include <linux/fs.h>

#include <linux/cdev.h>

#include "class.h"
#include "capture.h"

#include <vibe_debug.h>

struct stm_capture_class_device {
  struct device                     *sccd_device;
  dev_t                              sccd_devt;
};


static struct class *stm_capture_class;
static dev_t stm_capture_devt;


#if !defined(KBUILD_SYSTEM_INFO)
#define KBUILD_SYSTEM_INFO "<unknown>"
#endif
#if !defined(KBUILD_USER)
#define KBUILD_USER "<unknown>"
#endif
#if !defined(KBUILD_SOURCE)
#define KBUILD_SOURCE "<unknown>"
#endif
#if !defined(KBUILD_VERSION)
#define KBUILD_VERSION "<unknown>"
#endif
#if !defined(KBUILD_DATE)
#define KBUILD_DATE "<unknown>"
#endif

struct stm_capture_class_device *
stm_capture_classdev_init (int                                i,
         struct device                     *parent,
         void                              *dev_data,
         struct device_attribute           *dev_attrs)
{
  struct stm_capture_class_device *sccd;
  int res = 0;
  char name[20];

  sccd = kzalloc (sizeof (*sccd), GFP_KERNEL);
  if (sccd == NULL)
    return ERR_PTR (-ENOMEM);

  sccd->sccd_devt = MKDEV (MAJOR (stm_capture_devt), i);

  snprintf(name, sizeof (name), "stm-capture.%u", i);
  sccd->sccd_device = device_create (stm_capture_class, parent,
             sccd->sccd_devt,
             dev_data, name);
  if (IS_ERR (sccd->sccd_device)) {
    TRC(TRC_ID_ERROR, "Failed to create class device %d", i);
    res = PTR_ERR (sccd->sccd_device);
    goto out;
  }

  if (dev_attrs) {
    for (i = 0; attr_name (dev_attrs[i]); ++i) {
      res = device_create_file (sccd->sccd_device,
              &dev_attrs[i]);
      if (res)
        break;
    }

    if (res) {
      while (--i >= 0)
        device_remove_file (sccd->sccd_device,
                &dev_attrs[i]);
      goto out_device;
    }
  }

  return sccd;

out_device:
  device_destroy (stm_capture_class, sccd->sccd_devt);

out:
  kfree (sccd);
  return ERR_PTR (res);
}

int
stm_capture_classdev_deinit (struct stm_capture_class_device *sccd)
{
  device_destroy (stm_capture_class, sccd->sccd_devt);

  kfree (sccd);

  TRC(TRC_ID_PIXEL_CAPTURE, "Deinit class ok");

  return 0;
}


static ssize_t stm_capture_class_show_device_configuration(struct device *dev,
                                       struct device_attribute *attr, char *buf)
{
  ssize_t sz = 0;
  struct stm_capture_config *cap_cfg = (struct stm_capture_config *)dev_get_drvdata(dev);

  if(!cap_cfg->pixel_capture)
    return sz;

  sz += snprintf(&buf[sz], PAGE_SIZE - sz, "Capture Device Name \t: %s\n", cap_cfg->name);
  sz += snprintf(&buf[sz], PAGE_SIZE - sz, "Capture Device Type \t: %d\n", cap_cfg->device_type);
  sz += snprintf(&buf[sz], PAGE_SIZE - sz, "Capture Device ID   \t: %d\n", cap_cfg->device_id);

  sz += (ssize_t)stm_pixel_capture_debug_setup(cap_cfg->pixel_capture, &buf[sz], (PAGE_SIZE - sz));

  return sz;
}


static struct device_attribute stm_capture_dev_attrs[] = {
  __ATTR(debug_info, S_IRUGO, stm_capture_class_show_device_configuration, NULL),
  __ATTR_NULL
};


static CLASS_ATTR_STRING(info, S_IRUGO,
      "Build Source:  " KBUILD_SOURCE "\n"
      "Build Version: " KBUILD_VERSION "\n"
      "Build User:    " KBUILD_USER "\n"
      "Build Date:    " KBUILD_DATE "\n"
      "Build System:  " KBUILD_SYSTEM_INFO "\n");


int __init
stm_capture_class_init (int n_devices)
{
  int res;

  stm_capture_class = class_create (THIS_MODULE, "stm-capture");
  if (IS_ERR (stm_capture_class)) {
    TRC(TRC_ID_ERROR, "Couldn't create class device");
    return PTR_ERR (stm_capture_class);
  }
  stm_capture_class->dev_attrs = stm_capture_dev_attrs;

  if ((res = class_create_file (stm_capture_class,
              &class_attr_info.attr)) < 0) {
    TRC(TRC_ID_ERROR, "Couldn't create class device info attr");
    return -ENOMEM;
  }

  if ((res = alloc_chrdev_region (&stm_capture_devt, 0, n_devices,
          "stm-capture")) < 0) {
    TRC(TRC_ID_ERROR, "Couldn't allocate device numbers");
    return res;
  }

  return res;
}


void
stm_capture_class_cleanup (int n_devices)
{
  class_destroy (stm_capture_class);

  unregister_chrdev_region (stm_capture_devt, n_devices);
}
