/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __STM_PIXEL_CAPTURE_H__
#define __STM_PIXEL_CAPTURE_H__

#include <linux/device.h>
#include <linux/types.h>
#include <stm_display.h>
#include <linux/stm/stmcoredisplay.h>

#include "class.h"
#include "device_features.h"

#define STM_CAPTURE_MAX_DEVICES   4
#define STM_CAPTURE_MAX_INSTANCES 4
#define STM_CAPTURE_MAX_CALLBACKS 2

#define STM_CAPTURE_MAX_NAME_LENGTH 32

struct stm_capture_clock {
  void            *clk;
  char             name[STM_CAPTURE_MAX_NAME_LENGTH];
  uint32_t         clk_freq;
  bool             enabled;
};

struct stm_capture_config {
  int                             device_id;
  stm_pixel_capture_h             pixel_capture;
  stm_pixel_capture_device_type_t device_type;
  char                            name[STM_CAPTURE_MAX_NAME_LENGTH];
  stm_pixel_capture_hw_features_t hw_features;

  /* Runtime Power status */
  int                             rpm_suspended;

  /* Capture Processing clock management */
  struct stm_capture_clock        proc_clk;
};

int stm_capture_register_device (struct device                     *dev,
         struct device_attribute           *attrs,
         void                              *data);
int stm_capture_unregister_device (void *data);

#endif /* __STM_BDISPII_AQ_H__ */
