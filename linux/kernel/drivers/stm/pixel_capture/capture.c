/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/kref.h>
#include <linux/device.h>
#include <linux/mutex.h>

#include "capture.h"

#include <vibe_debug.h>


struct stm_pixel_capture_device_s
{
  struct stm_capture_class_device *sccd;

  void                     *data;
  struct mutex              mutex;
};

static struct stm_pixel_capture_device_s capture_devices[STM_CAPTURE_MAX_DEVICES];

int
stm_capture_register_device (struct device                     *dev,
           struct device_attribute           *attrs,
           void                              *data)
{
  unsigned int i;

  for (i = 0; i < ARRAY_SIZE (capture_devices); ++i) {
    TRC(TRC_ID_PIXEL_CAPTURE, "Try pixel capture device id %d!", i);
    if (!capture_devices[i].data) {
      struct stm_pixel_capture_device_s *capture = &capture_devices[i];
      TRC(TRC_ID_PIXEL_CAPTURE, "Get pixel capture device id %d!", i);

      capture->sccd = stm_capture_classdev_init (i,
                   dev,
                   data,
                   attrs);
      if (IS_ERR (capture->sccd))
        return PTR_ERR (capture->sccd);

      capture->data = data;

      mutex_init(&capture->mutex);

      return 0;
    }
  }

  return -ENOMEM;
}

int
stm_capture_unregister_device (void *data)
{
  unsigned int i;

  TRC(TRC_ID_PIXEL_CAPTURE, "Unregister device with data %p!", data);
  for (i = 0; i < ARRAY_SIZE (capture_devices); ++i) {
    if (capture_devices[i].data == data) {
      int res = stm_capture_classdev_deinit (capture_devices[i].sccd);

      capture_devices[i].data = NULL;

      return res;
    }
  }

  return -ENODEV;
}
