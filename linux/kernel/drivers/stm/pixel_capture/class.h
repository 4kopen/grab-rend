/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __STM_CAPTURE_CLASS_H__
#define __STM_CAPTURE_CLASS_H__

#include <linux/init.h>
#include <linux/device.h>
#include <linux/mm.h>
#include <linux/fs.h>

#include <stm_pixel_capture.h>

struct stm_capture_class_device;

int __init stm_capture_class_init (int n_devices);
void stm_capture_class_cleanup (int n_devices);

struct stm_capture_class_device *
stm_capture_classdev_init (int                                i,
			   struct device                     *parent,
			   void                              *dev_data,
			   struct device_attribute           *dev_attrs);

int
stm_capture_classdev_deinit (struct stm_capture_class_device *dev);


#endif /* __STM_CAPTURE_CLASS_H__ */
