#
# CONFIG_CPU_SUBTYPE_STXXXX is no more defined for all coming ST chips
# based on arm CPU. So redefine it here just to keep sh support inside the driver
#
EXTRA_CFLAGS += -I$(STG_TOPDIR)/hdmirx/soc
EXTRA_CFLAGS += -I$(STG_TOPDIR)/hdmirx/include

ifeq (y, $(filter y, $(CONFIG_MACH_STM_STIH418)))

EXTRA_CFLAGS += -I$(STG_TOPDIR)/hdmirx/soc/stiH418

STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/hdmirx/soc/stiH418/, \
  stiH418_hdmirx_drv.c)
endif
