/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef _stiH418_REG_H
#define _stiH418_REG_H

#define     HDRX_CSM_MODULE_ADDRS_OFFSET            0x003CCUL
#define     HDRX_I2C_MASTER_MODULE_ADDRS_OFFSET     0x000F4UL
#define     HDRX_IFM_INSTRUMENT_PU_ADDRS_OFFSET     0x00500UL
#define     HDRX_PACKET_MEMORY_ADDRS_BASE_OFFSET    0x01000UL

#endif
