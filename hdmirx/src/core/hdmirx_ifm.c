/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


/* Standard Includes ----------------------------------------------*/
#include "stddefs_hdmirx.h"
#include <stm_hdmirx_os.h>

/* Include of Other module interface headers --------------------------*/

/* Local Includes -------------------------------------------------*/
#include <hdmirx_drv.h>
#include <hdmirx_core_export.h>
#include <hdmirx_ifm.h>
#include <hdmirx_utility.h>
#include <InternalTypes.h>
#include <hdmirx_ifm_regoffsets.h>

/* Private Typedef -----------------------------------------------*/

#define ENABLE_AFR_AFM_CTRL

/* Private macro's ------------------------------------------------*/

#define     TCLK_FREQ_HZ    30000000UL	/* T Clock */

#define     H_SYNC_MIN		15000UL	/* measured in Hz */
#define     V_SYNC_MIN		20	/* measured in Hz */

#define     H_WATCHDOG		    (((TCLK_FREQ_HZ/H_SYNC_MIN) + (TCLK_FREQ_HZ/H_SYNC_MIN)/2)>>6)
#define     V_WATCHDOG		    ((((TCLK_FREQ_HZ/511)/V_SYNC_MIN) + ((TCLK_FREQ_HZ/511)/V_SYNC_MIN)/2)>>7)
#define     IFMWATCHDOG_VAL     ((V_WATCHDOG<<8)|H_WATCHDOG)

#define OVERLAP_HFREQ 31250
#define OVERLAP_VFREQ 500
#define DELTA_HFREQ     15
#define DELTA_VFREQ     15

#define HSHIFT 4
/* Private Variables -----------------------------------------------*/

static sthdmirx_timing_desc_t timings_reference[] = {
{1,  /*PRAT*/ 25175000, 25200000, /*HRAT*/ 31469, 31469, /*VRAT*/ 59940, 60000, /*HBPD*/ 48, /*HSPW*/ 96, /*HSPD*/ 16,  /*HRES*/ 640,  /*HTOT*/ 800,  /*VBPD*/ 33, /*VSPW*/ 2,  /*VSPD*/ 10,/*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{2,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 31469, 31469, /*VRAT*/ 59940, 60000, /*HBPD*/ 60, /*HSPW*/ 62, /*HSPD*/ 16,  /*HRES*/ 720,  /*HTOT*/ 858,  /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{3,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 31469, 31469, /*VRAT*/ 59940, 60000, /*HBPD*/ 60, /*HSPW*/ 62, /*HSPD*/ 16,  /*HRES*/ 720,  /*HTOT*/ 858,  /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{4,  /*PRAT*/ 74175000, 74250000, /*HRAT*/ 44955, 44955, /*VRAT*/ 59940, 60000, /*HBPD*/ 220,/*HSPW*/ 40, /*HSPD*/ 110, /*HRES*/ 1280, /*HTOT*/ 1650, /*VBPD*/ 20, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 720,  /*VTOT*/ 750,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{5,  /*PRAT*/ 74175000, 74250000, /*HRAT*/ 33716, 33716, /*VRAT*/ 59940, 60000, /*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 88,  /*HRES*/ 1920, /*HTOT*/ 2200, /*VBPD*/ 15, /*VSPW*/ 5,  /*VSPD*/ 2, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{6,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59940, 60000, /*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{7,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59940, 60000, /*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{8,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 240,  /*VTOT*/ 262,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{9,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 240,  /*VTOT*/ 262,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{8,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 5, /*VRES*/ 240,  /*VTOT*/ 263,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{9,  /*PRAT*/ 27000000, 27027000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 5, /*VRES*/ 240,  /*VTOT*/ 263,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{10, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59940, 60000, /*HBPD*/ 228,/*HSPW*/ 248,/*HSPD*/ 76,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{11, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59940, 60000, /*HBPD*/ 228,/*HSPW*/ 248,/*HSPD*/ 76,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{12, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 228,/*HSPW*/ 248,/*HSPD*/ 76,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 240,  /*VTOT*/ 262,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{13, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 228,/*HSPW*/ 248,/*HSPD*/ 76,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 240,  /*VTOT*/ 262,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{12, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 228,/*HSPW*/ 248,/*HSPD*/ 76,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 5, /*VRES*/ 240,  /*VTOT*/ 263,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{13, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 15734, 15734, /*VRAT*/ 59826, 59826, /*HBPD*/ 228,/*HSPW*/ 248,/*HSPD*/ 76,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 5, /*VRES*/ 240,  /*VTOT*/ 263,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{14, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 31469, 31469, /*VRAT*/ 59940, 60000, /*HBPD*/ 120,/*HSPW*/ 124,/*HSPD*/ 32,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{15, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 31469, 31469, /*VRAT*/ 59940, 60000, /*HBPD*/ 120,/*HSPW*/ 124,/*HSPD*/ 32,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{16, /*PRAT*/ 148350000,148500000,/*HRAT*/ 67432, 67432, /*VRAT*/ 59940, 60000, /*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 88,  /*HRES*/ 1920, /*HTOT*/ 2200, /*VBPD*/ 36, /*VSPW*/ 5,  /*VSPD*/ 4, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{17, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 31250, 31250, /*VRAT*/ 50000, 50000, /*HBPD*/ 68, /*HSPW*/ 64, /*HSPD*/ 12,  /*HRES*/ 720,  /*HTOT*/ 864,  /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{18, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 31250, 31250, /*VRAT*/ 50000, 50000, /*HBPD*/ 68, /*HSPW*/ 64, /*HSPD*/ 12,  /*HRES*/ 720,  /*HTOT*/ 864,  /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{19, /*PRAT*/ 74250000, 74250000, /*HRAT*/ 37500, 37500, /*VRAT*/ 50000, 50000, /*HBPD*/ 220,/*HSPW*/ 40, /*HSPD*/ 440, /*HRES*/ 1280, /*HTOT*/ 1980, /*VBPD*/ 20, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 720,  /*VTOT*/ 750,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{20, /*PRAT*/ 74250000, 74250000, /*HRAT*/ 28125, 28125, /*VRAT*/ 50000, 50000, /*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 528, /*HRES*/ 1920, /*HTOT*/ 2640, /*VBPD*/ 15, /*VSPW*/ 5,  /*VSPD*/ 2, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{21, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 50000, 50000, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{22, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 50000, 50000, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{23, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 288,  /*VTOT*/ 312,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{24, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 288,  /*VTOT*/ 312,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{23, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 3, /*VRES*/ 288,  /*VTOT*/ 313,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{24, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 3, /*VRES*/ 288,  /*VTOT*/ 313,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{23, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 288,  /*VTOT*/ 314,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{24, /*PRAT*/ 27000000, 27000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 288,  /*VTOT*/ 314,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{25, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 50000, 50000, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{26, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 50000, 50000, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{27, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 288,  /*VTOT*/ 312,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{28, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 288,  /*VTOT*/ 312,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{27, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 3, /*VRES*/ 288,  /*VTOT*/ 313,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{28, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 3, /*VRES*/ 288,  /*VTOT*/ 313,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{27, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 288,  /*VTOT*/ 314,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{28, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 15625, 15625, /*VRAT*/ 49761, 49761, /*HBPD*/ 276,/*HSPW*/ 252,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 288,  /*VTOT*/ 314,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{29, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 31250, 31250, /*VRAT*/ 50000, 50000, /*HBPD*/ 136,/*HSPW*/ 128,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{30, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 31250, 31250, /*VRAT*/ 50000, 50000, /*HBPD*/ 136,/*HSPW*/ 128,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{31, /*PRAT*/ 148500000,148500000,/*HRAT*/ 56250, 56250, /*VRAT*/ 50000, 50000, /*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 528, /*HRES*/ 1920, /*HTOT*/ 2640, /*VBPD*/ 36, /*VSPW*/ 5,  /*VSPD*/ 4, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{32, /*PRAT*/ 74175000, 74250000, /*HRAT*/ 26973, 27000, /*VRAT*/ 23980, 24000, /*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 638, /*HRES*/ 1920, /*HTOT*/ 2750, /*VBPD*/ 36, /*VSPW*/ 5,  /*VSPD*/ 4, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{33, /*PRAT*/ 74250000, 74250000, /*HRAT*/ 28125, 28125, /*VRAT*/ 25000, 25000, /*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 528, /*HRES*/ 1920, /*HTOT*/ 2640, /*VBPD*/ 36, /*VSPW*/ 5,  /*VSPD*/ 4, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{34, /*PRAT*/ 74175000, 74250000, /*HRAT*/ 33716, 33750, /*VRAT*/ 29970, 30000, /*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 88,  /*HRES*/ 1920, /*HTOT*/ 2200, /*VBPD*/ 36, /*VSPW*/ 5,  /*VSPD*/ 4, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{35, /*PRAT*/ 108000000,108108000,/*HRAT*/ 31469, 31500, /*VRAT*/ 59940, 60000, /*HBPD*/ 240,/*HSPW*/ 248,/*HSPD*/ 64,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{36, /*PRAT*/ 108000000,108108000,/*HRAT*/ 31469, 31500, /*VRAT*/ 59940, 60000, /*HBPD*/ 240,/*HSPW*/ 248,/*HSPD*/ 64,  /*HRES*/ 2880, /*HTOT*/ 3432, /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{37, /*PRAT*/ 108000000,108000000,/*HRAT*/ 31250, 31250, /*VRAT*/ 50000, 50000, /*HBPD*/ 272,/*HSPW*/ 256,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{38, /*PRAT*/ 108000000,108000000,/*HRAT*/ 31250, 31250, /*VRAT*/ 50000, 50000, /*HBPD*/ 272,/*HSPW*/ 256,/*HSPD*/ 48,  /*HRES*/ 2880, /*HTOT*/ 3456, /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{39, /*PRAT*/ 72000000, 72000000, /*HRAT*/ 31250, 31250, /*VRAT*/ 50000, 50000, /*HBPD*/ 184,/*HSPW*/ 168,/*HSPD*/ 32,  /*HRES*/ 1920, /*HTOT*/ 2304, /*VBPD*/ 57, /*VSPW*/ 5,  /*VSPD*/ 23,/*VRES*/ 1080, /*VTOT*/ 1250, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{40, /*PRAT*/ 148500000,148500000,/*HRAT*/ 56250, 56250, /*VRAT*/ 100000,100000,/*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 528, /*HRES*/ 1920, /*HTOT*/ 2640, /*VBPD*/ 15, /*VSPW*/ 5,  /*VSPD*/ 2, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{41, /*PRAT*/ 148500000,148500000,/*HRAT*/ 75000, 75000, /*VRAT*/ 100000,100000,/*HBPD*/ 220,/*HSPW*/ 40, /*HSPD*/ 440, /*HRES*/ 1280, /*HTOT*/ 1980, /*VBPD*/ 20, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 720,  /*VTOT*/ 750,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{42, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 62500, 62500, /*VRAT*/ 100000,100000,/*HBPD*/ 68, /*HSPW*/ 64, /*HSPD*/ 12,  /*HRES*/ 720,  /*HTOT*/ 864,  /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{43, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 62500, 62500, /*VRAT*/ 100000,100000,/*HBPD*/ 68, /*HSPW*/ 64, /*HSPD*/ 12,  /*HRES*/ 720,  /*HTOT*/ 864,  /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{44, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 62500, 62500, /*VRAT*/ 100000,100000,/*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{45, /*PRAT*/ 54000000, 54000000, /*HRAT*/ 62500, 62500, /*VRAT*/ 100000,100000,/*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{46, /*PRAT*/ 148352000,148500000,/*HRAT*/ 67432, 67500, /*VRAT*/ 119880,120000,/*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 88,  /*HRES*/ 1920, /*HTOT*/ 2200, /*VBPD*/ 15, /*VSPW*/ 5,  /*VSPD*/ 2, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{47, /*PRAT*/ 148352000,148500000,/*HRAT*/ 89909, 90000, /*VRAT*/ 119880,120000,/*HBPD*/ 220,/*HSPW*/ 40, /*HSPD*/ 110, /*HRES*/ 1280, /*HTOT*/ 1650, /*VBPD*/ 20, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 720,  /*VTOT*/ 750,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{48, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 62937, 63000, /*VRAT*/ 119880,120000,/*HBPD*/ 60, /*HSPW*/ 62, /*HSPD*/ 16,  /*HRES*/ 720,  /*HTOT*/ 858,  /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{49, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 62937, 63000, /*VRAT*/ 119880,120000,/*HBPD*/ 60, /*HSPW*/ 62, /*HSPD*/ 16,  /*HRES*/ 720,  /*HTOT*/ 858,  /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{50, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 31469, 31500, /*VRAT*/ 119880,120000,/*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{51, /*PRAT*/ 54000000, 54054000, /*HRAT*/ 31469, 31500, /*VRAT*/ 119880,120000,/*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{52, /*PRAT*/ 108000000,108000000,/*HRAT*/ 125000,125000,/*VRAT*/ 200000,200000,/*HBPD*/ 68, /*HSPW*/ 64, /*HSPD*/ 12,  /*HRES*/ 720,  /*HTOT*/ 864,  /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{53, /*PRAT*/ 108000000,108000000,/*HRAT*/ 125000,125000,/*VRAT*/ 200000,200000,/*HBPD*/ 68, /*HSPW*/ 64, /*HSPD*/ 12,  /*HRES*/ 720,  /*HTOT*/ 864,  /*VBPD*/ 39, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{54, /*PRAT*/ 108000000,108000000,/*HRAT*/ 62500, 62500, /*VRAT*/ 200000,200000,/*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{55, /*PRAT*/ 108000000,108000000,/*HRAT*/ 62500, 62500, /*VRAT*/ 200000,200000,/*HBPD*/ 138,/*HSPW*/ 126,/*HSPD*/ 24,  /*HRES*/ 1440, /*HTOT*/ 1728, /*VBPD*/ 19, /*VSPW*/ 3,  /*VSPD*/ 2, /*VRES*/ 576,  /*VTOT*/ 625,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{56, /*PRAT*/ 108000000,108108000,/*HRAT*/ 125874,126000,/*VRAT*/ 239760,240000,/*HBPD*/ 60, /*HSPW*/ 62, /*HSPD*/ 16,  /*HRES*/ 720,  /*HTOT*/ 858,  /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{57, /*PRAT*/ 108000000,108108000,/*HRAT*/ 125874,126000,/*VRAT*/ 239760,240000,/*HBPD*/ 60, /*HSPW*/ 62, /*HSPD*/ 16,  /*HRES*/ 720,  /*HTOT*/ 858,  /*VBPD*/ 30, /*VSPW*/ 6,  /*VSPD*/ 9, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{58, /*PRAT*/ 108000000,108108000,/*HRAT*/ 62937, 63000, /*VRAT*/ 239760,240000,/*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{59, /*PRAT*/ 108000000,108108000,/*HRAT*/ 62937, 63000, /*VRAT*/ 239760,240000,/*HBPD*/ 114,/*HSPW*/ 124,/*HSPD*/ 38,  /*HRES*/ 1440, /*HTOT*/ 1716, /*VBPD*/ 15, /*VSPW*/ 3,  /*VSPD*/ 4, /*VRES*/ 480,  /*VTOT*/ 525,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED,  STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE, STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE},
{60, /*PRAT*/ 59340000, 59400000, /*HRAT*/ 17985, 18000, /*VRAT*/ 23980, 24000, /*HBPD*/ 220,/*HSPW*/ 40, /*HSPD*/ 1760,/*HRES*/ 1280, /*HTOT*/ 3300, /*VBPD*/ 20, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 720,  /*VTOT*/ 750,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{61, /*PRAT*/ 74250000, 74250000, /*HRAT*/ 18750, 18750, /*VRAT*/ 25000, 25000, /*HBPD*/ 220,/*HSPW*/ 40, /*HSPD*/ 2420,/*HRES*/ 1280, /*HTOT*/ 3960, /*VBPD*/ 20, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 720,  /*VTOT*/ 750,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{62, /*PRAT*/ 74175000, 74250000, /*HRAT*/ 22477, 22500, /*VRAT*/ 29970, 30000, /*HBPD*/ 220,/*HSPW*/ 40, /*HSPD*/ 1760,/*HRES*/ 1280, /*HTOT*/ 3300, /*VBPD*/ 20, /*VSPW*/ 5,  /*VSPD*/ 5, /*VRES*/ 720,  /*VTOT*/ 750,  /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{63, /*PRAT*/ 296703000,297000000,/*HRAT*/ 134864,135000,/*VRAT*/ 119880,120000,/*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 88,  /*HRES*/ 1920, /*HTOT*/ 2200, /*VBPD*/ 36, /*VSPW*/ 5,  /*VSPD*/ 4, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{64, /*PRAT*/ 297000000,297000000,/*HRAT*/ 112500,112500,/*VRAT*/ 100000,100000,/*HBPD*/ 148,/*HSPW*/ 44, /*HSPD*/ 528, /*HRES*/ 1920, /*HTOT*/ 2640, /*VBPD*/ 36, /*VSPW*/ 5,  /*VSPD*/ 4, /*VRES*/ 1080, /*VTOT*/ 1125, /*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE, STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
/* LLC UHD/4K timings */
{1,/*'PRAT':*/ 296703000,297000000,/*HRAT*/ 67430,67500,/*VRAT*/ 29970,30000,/*HSPW*/ 88,/*HTOT*/ 4400,/*HRES*/ 3840,/*VSPW*/ 10,/*VBPD*/ 72,/*HBPD*/ 296,/*VTOT*/2250,/*VRES*/ 2160,/*HSPD*/ 176, /*VSPD*/ 8,/*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE,/*HSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE,/*VSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{2,/*'PRAT':*/ 297000000,297000000,/*HRAT*/ 56250,56250,/*VRAT*/ 25000,25000,/*HSPW*/ 88,/*HTOT*/ 5280,/*HRES*/ 3840,/*VSPW*/ 10,/*VBPD*/ 72,/*HBPD*/ 296,/*VTOT*/2250,/*VRES*/ 2160,/*HSPD*/ 1056,/*VSPD*/ 8,/*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE,/*HSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE,/*VSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{3,/*'PRAT':*/ 296703000,297000000,/*HRAT*/ 53950,54000,/*VRAT*/ 23980,24000,/*HSPW*/ 88,/*HTOT*/ 5500,/*HRES*/ 3840,/*VSPW*/ 10,/*VBPD*/ 72,/*HBPD*/ 296,/*VTOT*/2250,/*VRES*/ 2160,/*HSPD*/ 1276,/*VSPD*/ 8,/*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE,/*HSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE,/*VSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
{4,/*'PRAT':*/ 297000000,297000000,/*HRAT*/ 54000,54000,/*VRAT*/ 24000,24000,/*HSPW*/ 88,/*HTOT*/ 5500,/*HRES*/ 4096,/*VSPW*/ 10,/*VBPD*/ 72,/*HBPD*/ 296,/*VTOT*/2250,/*VRES*/ 2160,/*HSPD*/ 1020,/*VSPD*/ 8,/*SCAN*/ STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE,/*HSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE,/*VSPP*/ STM_HDMIRX_SIGNAL_POLARITY_POSITIVE},
};

static uint8_t timings_ref_count = sizeof(timings_reference)/sizeof(timings_reference[0]);

/* Private functions prototypes ------------------------------------*/

BOOL sthdmirx_IFM_waitVSync_time(sthdmirx_IFM_context_t *pIfmCtrl,
                                 U8 NoOfVsyncs);

/* Interface procedures/functions ----------------------------------*/
void sthdmirx_video_prop_changed_data_fill(const hdmirx_route_handle_t *pInpHandle,
    stm_hdmirx_video_property_t *pVidPropData);

/******************************************************************************
 FUNCTION       : sthdmirx_IFM_try_hdmi_std_signal_timing()
 USAGE        	: fill timings params if vic is available in avi infoframes
 INPUT        	:
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
BOOL sthdmirx_IFM_try_hdmi_std_signal_timing(hdmirx_route_handle_t *pInpHandle, stm_hdmirx_signal_timing_t *pSigTimingData)
{
  stm_hdmirx_video_property_t VidProp;
  uint8_t index = 0;
  uint32_t PixClk_margin = 0;

  if (!sthdmirx_CORE_is_HDMI_signal(pInpHandle))
  {
    TRC(TRC_ID_HDMI_RX_CORE,"IFM: signal is not hdmi");
    return FALSE;
  }
  sthdmirx_video_prop_changed_data_fill(pInpHandle, &VidProp);
  if ((VidProp.video_timing_code != 0)&&((VidProp.hdmi_video_format == 0)||(VidProp.hdmi_video_format == 1)))
  {
    for (;index < timings_ref_count; index++)
    {
      if (timings_reference[index].vic != VidProp.video_timing_code)
        continue;

      /* check for gaming modes */
      if ((timings_reference[index].v_total_lines < 315)&&
          (timings_reference[index].v_total_lines != pSigTimingData->vtotal))
        continue;

      PixClk_margin = (timings_reference[index].pixclk * 6)/1000;

      if ((pSigTimingData->pixel_clock_hz <= (timings_reference[index].pixclk - PixClk_margin))||
           (pSigTimingData->pixel_clock_hz >= (PixClk_margin + timings_reference[index].pixclk)))
      {
        return FALSE;
      }

      /*Verify the number of lines*/
      if ((pSigTimingData->vtotal <= (timings_reference[index].v_total_lines - 10))||
           (pSigTimingData->vtotal >= (10 + timings_reference[index].v_total_lines)))
      {
        return FALSE;
      }

      pSigTimingData->pixel_clock_hz /= VidProp.pixel_repeat_factor+1;

      pSigTimingData->htotal = timings_reference[index].h_total_pixels/(VidProp.pixel_repeat_factor+1);
      pSigTimingData->vtotal = timings_reference[index].v_total_lines;
      pSigTimingData->hactive_start = timings_reference[index].h_back_porch + timings_reference[index].h_pulse_width;
      pSigTimingData->hactive_start /= VidProp.pixel_repeat_factor+1;

      pSigTimingData->hactive  = timings_reference[index].h_active_pixels/(VidProp.pixel_repeat_factor+1);
      pSigTimingData->vactive_start = timings_reference[index].v_back_porch + timings_reference[index].v_pulse_width;
      pSigTimingData->vactive_start *= (timings_reference[index].scan_type == STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED)?2:1;

      pSigTimingData->vActive = timings_reference[index].v_active_lines;
      pSigTimingData->hsync_polarity = timings_reference[index].h_polarity;
      pSigTimingData->vsync_polarity = timings_reference[index].v_polarity;
      pSigTimingData->scan_type = timings_reference[index].scan_type;
      TRC(TRC_ID_HDMI_RX_CORE,"IFM: found mode with vic %d", timings_reference[index].vic);
      return TRUE;
    }
  }

  TRC(TRC_ID_HDMI_RX_CORE,"IFM: no mode found ");
  return FALSE;
}

/******************************************************************************
 FUNCTION       : sthdmirx_IFM_try_dvi_std_signal_timing()
 USAGE        	: fill timings params if measuremnts are close to a known format
 INPUT        	: measured timings from IFM/IBD
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_try_dvi_std_signal_timing(hdmirx_route_handle_t *pInpHandle, stm_hdmirx_signal_timing_t *pSigTimingData)
{
  uint8_t index = 0, double_clk_factor = 1;

  if (pInpHandle->sMeasCtrl.mStatus & SIG_STS_DOUBLE_CLK_MODE_PRESENT)
  {
    double_clk_factor = 2;
    TRC(TRC_ID_HDMI_RX_CORE,"IFM: double clocked format");
  }
  for (;index < timings_ref_count; index++)
  {
    uint32_t PixClk_margin = (timings_reference[index].pixclk * 6)/1000;
    uint16_t h_start_tmp = 0, h_start = 0, v_start = 0;

    if ((pSigTimingData->pixel_clock_hz <= (timings_reference[index].pixclk - PixClk_margin))||
	     (pSigTimingData->pixel_clock_hz >= (PixClk_margin + timings_reference[index].pixclk)))
    {
      continue;
    }

    if (timings_reference[index].scan_type != pSigTimingData->scan_type)
    {
      continue;
    }
    if (timings_reference[index].v_active_lines != pSigTimingData->vActive)
    {
      continue;
    }
    if (pSigTimingData->vtotal != timings_reference[index].v_total_lines)
    {
      continue;
    }
    if (pSigTimingData->htotal != timings_reference[index].h_total_pixels/double_clk_factor)
    {
      continue;
    }
    if (pSigTimingData->hactive < (timings_reference[index].h_active_pixels/double_clk_factor - 2))
    {
      continue;
    }
    if (pSigTimingData->hactive > (timings_reference[index].h_active_pixels/double_clk_factor + 2))
    {
      continue;
    }
    h_start = (timings_reference[index].h_back_porch + timings_reference[index].h_pulse_width)/double_clk_factor;
    if (timings_reference[index].h_polarity == STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE)
    {
       h_start_tmp = timings_reference[index].h_pulse_width/double_clk_factor;
    }
    h_start_tmp += pSigTimingData->hactive_start;
    if (h_start_tmp < (h_start-3))
    {
      continue;
    }
    if (h_start_tmp > (h_start+3))
    {
      continue;
    }

    v_start = timings_reference[index].v_back_porch + timings_reference[index].v_pulse_width;
    v_start *= (timings_reference[index].scan_type == STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED)?2:1;
    if (pSigTimingData->vactive_start < (v_start-2))
    {
      continue;
    }
    if (pSigTimingData->vactive_start > (v_start+2))
    {
      continue;
    }

    pSigTimingData->pixel_clock_hz /= double_clk_factor;
    pSigTimingData->htotal = timings_reference[index].h_total_pixels/double_clk_factor;
    pSigTimingData->vtotal = timings_reference[index].v_total_lines;
    pSigTimingData->hactive_start = h_start;
    pSigTimingData->hactive  = timings_reference[index].h_active_pixels/double_clk_factor;
    pSigTimingData->vactive_start = v_start;
    pSigTimingData->vActive = timings_reference[index].v_active_lines;
    pSigTimingData->hsync_polarity = timings_reference[index].h_polarity;
    pSigTimingData->vsync_polarity = timings_reference[index].v_polarity;
    pSigTimingData->scan_type = timings_reference[index].scan_type;
    TRC(TRC_ID_HDMI_RX_CORE,"IFM: found mode with vic %d", timings_reference[index].vic);
    return;
  }
  TRC(TRC_ID_HDMI_RX_CORE,"IFM: no mode found");
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_instrument_initialize()
 USAGE        	:   Initialize the Instrumentation block in HDMIRx
 INPUT        	:   Instrument Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_instrument_initialize(sthdmirx_IFM_context_t *pIfmCtrl)
{
  /* Initialize the Instrumentation Block of HDMI */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_AFR_CONTROL), (HDMI_RX_AFR_DISP_IFM_ERR_EN |
                                 HDMI_RX_AFR_DISP_IBD_ERR_EN | HDMI_RX_AFR_DISP_CLK_ERR_ENN));

#ifdef ENABLE_AFR_AFM_CTRL
  /* Enable the Auto Force BackGround */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_AFB_CONTROL), (HDMI_RX_AFB_IFM_ERR_EN |
                                 HDMI_RX_AFB_IBD_ERR_EN | HDMI_RX_AFB_CLK_ERR_EN));
#endif

  /* disable all IFM related interrupts */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_IRQ_MASK), 0x00);
  /* Clear all pending status bits */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_IRQ_STATUS), 0xFFFF);

  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_IFM_CLK_CONTROL), 0x00000000);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INSTRUMENT_POLARITY_CONTROL), 0x00000000);
  /* Reset the IFM & IBD Block  after the system powerUp */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INST_SOFT_RESETS), 0x03);
  STHDMIRX_DELAY_1ms(1);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INST_SOFT_RESETS), 0x00);

  TRC(TRC_ID_HDMI_RX_CORE,"sthdmirx_IFM_instrument_initialize : Initialization done \n");

  return;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_init()
 USAGE        	:   Initialize the IFM module in HDMIRx
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_init(sthdmirx_IFM_context_t *pIfmCtrl)
{

  /* Enable HDMI IFM - HDMI_IFM_EN */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_IFM_CTRL),
                       (HDMI_IFM_EN | HDMI_IFM_MEASEN | HDMI_IFM_HOFFSETEN
                        | HDMI_IFM_FIELD_EN | HDMI_IFM_INT_ODD_EN | HDMI_IFM_FIELD_DETECT_MODE | HDMI_IFM_FIELD_SEL));
  /* IFM Watchdog Configuration */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_IFM_WATCHDOG), IFMWATCHDOG_VAL);

  /* IFM H Line Configuration */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_IFM_HLINE), 0x30);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INST_HARD_RESETS), 0x00);

  /* IFM Input Timing Change detection thresholds */
  HDMI_WRITE_REG_DWORD((U32) ((U32) pIfmCtrl->ulBaseAddress +
                              HDMI_RX_IFM_CHANGE), 0x2D);	/*16 TCLKS */

  /* IFM Input Control Configuration - IMD - 0x05 Enables Input Capture and Interlaced input */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_CONTROL), 0x05);

  /* When input timing reaches to programmed line, input path is reseted & prepared for next field */
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_FRAME_RESET_LINE), 0x04);

  sthdmirx_IFM_IBD_config(pIfmCtrl, STHDRX_IBD_MEASURE_SOURCE,
                          STHDRX_IBD_MEASURE_DE);
  sthdmirx_IFM_IBD_config(pIfmCtrl, STHDRX_IBD_MEASURE_WINDOW, 0);
  sthdmirx_IFM_IBD_config(pIfmCtrl, STHDRX_IBD_ENABLE, 1);

  /* HDMI Instrumentation Block initialization */
  sthdmirx_IFM_instrument_initialize(pIfmCtrl);

  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD : Initialization done !!\n");

}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_signal_timing_get()
 USAGE        	:   Records the IBD Parameters
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
BOOL sthdmirx_IFM_signal_timing_get(sthdmirx_IFM_context_t *pIfmCtrl)
{
  sthdmirx_IFM_timing_params_t *pIfmParams = &pIfmCtrl->IFMTimingData;
  U32 ulHperiod, ulHpulse, ulVperiod, ulVpulse, ulVperiod2;
  uint32_t ColorDepthCode, ColorMult, ColorDiv;
  BOOL isInterlaced;
  U16 vstart;
  pIfmParams->VPulse = 1;
  pIfmParams->HPulse = 1;
  pIfmParams->HFreq_Hz = 1;
  pIfmParams->VFreq_Hz = 1;
  pIfmParams->H_SyncPolarity = pIfmParams->V_SyncPolarity =
                                 STM_HDMIRX_SIGNAL_POLARITY_POSITIVE;


  /* Get pixel clock from Line clock using color depth */
  ColorDepthCode = (HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDRX_COLOR_DEPTH_STATUS)) & HDRX_VID_COLOR_DEPTH);
  switch(ColorDepthCode) {
    case 5 : ColorMult = 4; ColorDiv = 5; break; /* 30 bpp */
    case 6 : ColorMult = 2; ColorDiv = 3; break; /* 36 bpp */
    case 7 : ColorMult = 1; ColorDiv = 2; break; /* 48 bpp */
    default: ColorMult = 1; ColorDiv = 1; /* 24 bpp or other */
  }
  pIfmParams->PixClk_Hz = (pIfmParams->LineClk_Hz * ColorMult) / ColorDiv;

  ulVperiod = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_IFM_VS_PERIOD)) & HDMI_RX_IFM_VS_PERIODBitFieldMask;
  ulVpulse  = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_IFM_VS_PULSE)) & HDMI_RX_IFM_VS_PULSEBitFieldMask;

  isInterlaced = ((HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IRQ_STATUS)) & HDMI_RX_INPUT_INTLC_ERR_MASK) != HDMI_RX_INPUT_INTLC_ERR_MASK );

  if (isInterlaced)
    pIfmParams->ScanType = STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED;
  else
    pIfmParams->ScanType = STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE;

  ulVperiod2=(isInterlaced)?((ulVperiod-ulVperiod%2)*2+1):ulVperiod;

  ulHperiod = (HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_IFM_HS_PERIOD)) & HDMI_RX_IFM_HS_PERIODBitFieldMask)*100;
  ulHpulse  = (HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_IFM_HS_PULSE)) & HDMI_RX_IFM_HS_PULSEBitFieldMask);

  /* Check and determine H & V sync polarity based on pulse width */
  if (ulHpulse > (ulHperiod / 2))
    {
      pIfmParams->H_SyncPolarity = STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE;
      ulHpulse = ulHperiod - ulHpulse;
    }
  if (ulVpulse > (ulVperiod / 2))
    {
      pIfmParams->V_SyncPolarity = STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE;
      ulVpulse = ulVperiod - ulVpulse;
    }

  if (!(ulHperiod && ulVperiod && ulVperiod2)) //Check whether divider is zero
    return FALSE;

  pIfmParams->HPeriod = (U16) ulHperiod;
  pIfmParams->VPeriod = (U16) ulVperiod;
  pIfmParams->HPulse = (U16) ulHpulse;
  pIfmParams->VPulse = (U16) ulVpulse;
  pIfmParams->HFreq_Hz = (U32)(TCLK_FREQ_HZ*100/ulHperiod);
  if(isInterlaced)
    pIfmParams->VFreq_Hz = (U16) (TCLK_FREQ_HZ*100/(ulHperiod*ulVperiod2/20));
  else
    pIfmParams->VFreq_Hz = (U16) (TCLK_FREQ_HZ*100/(ulHperiod*ulVperiod2/10));

  pIfmParams->HTotal = (U16)HDMI_READ_REG_DWORD((U32)((U32)pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IBD_HTOTAL)) & HDMI_INPUT_IBD_HTOTALBitFieldMask;
  pIfmParams->VTotal = (U16)HDMI_READ_REG_DWORD((U32)((U32)pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IBD_VTOTAL)) & HDMI_INPUT_IBD_VTOTALBitFieldMask;
  pIfmParams->Hstart = (U16)(HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IBD_HSTART)) & HDMI_INPUT_IBD_HSTARTBitFieldMask)+HSHIFT;
  pIfmParams->Width  = (U16)HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IBD_HWIDTH)) & HDMI_INPUT_IBD_HWIDTHBitFieldMask;
  pIfmParams->Vstart = (U16)HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IBD_VSTART)) & HDMI_INPUT_IBD_VSTARTBitFieldMask;
  pIfmParams->Length = (U16)HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IBD_VLENGTH)) & HDMI_INPUT_IBD_VLENGTHBitFieldMask;

  /*wait for one frame time */
  vstart = (U16)HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress + HDMI_RX_INPUT_IBD_VSTART)) & HDMI_INPUT_IBD_VSTARTBitFieldMask;

  if (vstart < pIfmParams->Vstart)
    {
      /* use the minimum value of v start */
      pIfmParams->Vstart = vstart;
    }

  if ((pIfmParams->HTotal == 0) || (pIfmParams->VTotal == 0))
    return FALSE;

  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: PixClk:%u",       pIfmParams->PixClk_Hz);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: HFeq:%d",         pIfmParams->HFreq_Hz);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: VFreq:%d Hz \n",  (pIfmParams->VFreq_Hz / 10));
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: HPulse:%d",       pIfmParams->HPulse);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: HPerios:%d",      pIfmParams->HPeriod);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: Vpulse:%d",       pIfmParams->VPulse);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: SCAN: %s\n",      isInterlaced?"I":"P");
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: VPeriod:%d\n",    pIfmParams->VPeriod);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: Start   H =%d\n", pIfmParams->Hstart);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: Start   V =%d\n", pIfmParams->Vstart);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: Active  H =%d\n", pIfmParams->Width);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: Active  V =%d\n", pIfmParams->Length);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: Total   H =%d\n", pIfmParams->HTotal);
  TRC(TRC_ID_HDMI_RX_CORE,"IFM_IBD: Total   V =%d\n", pIfmParams->VTotal);

  return TRUE;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_active_window_set()
 USAGE        	:   Sets the Active Window
 INPUT        	:   IFM Parameter structure, VStart, HStart, VLength, HWidth
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_active_window_set(sthdmirx_IFM_context_t *pIfmCtrl,
                                    sthdmirx_timing_window_param_t *mWindow)
{
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_H_ACT_START), mWindow->Hstart);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_H_ACT_WIDTH), mWindow->Width);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_V_ACT_START_ODD), mWindow->Vstart);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_V_ACT_START_EVEN), mWindow->Vstart);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_V_ACT_LENGTH), mWindow->Length);
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_IBD_meas_window_set()
 USAGE        	:   Sets the Measurement Window for Input Boundary Detection
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_IBD_meas_window_set(sthdmirx_IFM_context_t *pIfmCtrl,
                                      sthdmirx_timing_window_param_t *mWindow)
{
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_HMEASURE_START), mWindow->Hstart);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_HMEASURE_WIDTH), mWindow->Width);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_VMEASURE_START), mWindow->Vstart);
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_VMEASURE_LENGTH), mWindow->Length);
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_IBD_config()
 USAGE        	:   Records the IBD Parameters
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_IBD_config(sthdmirx_IFM_context_t *pIfmCtrl,
                             sthdmirx_IBD_ctrl_option_t Option, U8 uValue)
{
  U32 uIbdControlWord;

  uIbdControlWord = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                        HDMI_RX_INPUT_IBD_CONTROL)) & 0x01ff;

  switch (Option)
    {
    case STHDRX_IBD_MEASURE_SOURCE:
    {
      if (uValue == STHDRX_IBD_MEASURE_DATA)
        CLEAR_BIT(uIbdControlWord, HDMI_RX_INPUT_MEASURE_DE_nDATA);
      else if (uValue == STHDRX_IBD_MEASURE_DE)
        SET_BIT(uIbdControlWord, HDMI_RX_INPUT_MEASURE_DE_nDATA);
    }
    break;

    case STHDRX_IBD_MEASURE_WINDOW:
    {
      if (uValue == 0)
        CLEAR_BIT(uIbdControlWord, HDMI_RX_INPUT_IBD_MEASWINDOW_EN);
      else
        SET_BIT(uIbdControlWord, HDMI_RX_INPUT_IBD_MEASWINDOW_EN);
    }
    break;

    case STHDRX_IBD_RGB_SELECT:
    {
      CLEAR_BIT(uIbdControlWord, HDMI_RX_INPUT_IBD_RGB_SEL_MASK);

      if (uValue == STHDRX_IBD_RED_FIELD_SEL)
        SET_BIT(uIbdControlWord, (U16) (0x1 << 2));
      else if (uValue == STHDRX_IBD_GREEN_FIELD_SEL)
        SET_BIT(uIbdControlWord, (U16) (0x2 << 2));
      else if (uValue == STHDRX_IBD_BLUE_FIELD_SEL)
        SET_BIT(uIbdControlWord, (U16) (0x3 << 2));
    }
    break;

    case STHDRX_IBD_THRESHOLD:
    {
      CLEAR_BIT(uIbdControlWord, HDMI_RX_INPUT_DET_THOLD_SEL_MASK);
      SET_BIT(uIbdControlWord,
              ((uValue & HDMI_RX_INPUT_DET_THOLD_SEL_MASK) << 4));
    }
    break;

    case STHDRX_IBD_ENABLE:
      if (uValue == 0)
        CLEAR_BIT(uIbdControlWord, HDMI_RX_INPUT_IBD_EN);
      else
        SET_BIT(uIbdControlWord, HDMI_RX_INPUT_IBD_EN);
      break;

    default:
      break;
    }

  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_IBD_CONTROL), uIbdControlWord);

}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_HSync_polarity_get()
 USAGE        	:   Returns the Polarity of the HSync
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
stm_hdmirx_signal_polarity_t
sthdmirx_IFM_HSync_polarity_get(sthdmirx_IFM_context_t *pIfmCtrl)
{
  U16 H_Pulse, H_Period;
  stm_hdmirx_signal_polarity_t SigPolarity =
    STM_HDMIRX_SIGNAL_POLARITY_POSITIVE;

  H_Pulse = (U16) (HDMI_READ_REG_DWORD((U32) (pIfmCtrl->ulBaseAddress +
                                       HDMI_RX_IFM_HS_PULSE)) & (HDMI_RX_IFM_HS_PULSEBitFieldMask));
  H_Period = (U16) (HDMI_READ_REG_DWORD((U32) (pIfmCtrl->ulBaseAddress +
                                        HDMI_RX_IFM_HS_PERIOD)) & (HDMI_RX_IFM_HS_PERIODBitFieldMask));
  if (H_Pulse > (H_Period / 2))
    {
      SigPolarity = STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE;
    }

  TRC(TRC_ID_HDMI_RX_CORE,"IFM: HSync Polarity :%d \n", SigPolarity);
  return SigPolarity;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_VSync_polarity_get()
 USAGE        	:   Returns the Polarity of the VSync
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
stm_hdmirx_signal_polarity_t
sthdmirx_IFM_VSync_polarity_get(sthdmirx_IFM_context_t *pIfmCtrl)
{
  U16 V_Pulse, V_Period;
  stm_hdmirx_signal_polarity_t SigPolarity =
    STM_HDMIRX_SIGNAL_POLARITY_POSITIVE;

  V_Pulse = (U16) (HDMI_READ_REG_DWORD((U32) (pIfmCtrl->ulBaseAddress +
                                       HDMI_RX_IFM_VS_PULSE)) & (HDMI_RX_IFM_VS_PULSEBitFieldMask));
  V_Period = (U16) (HDMI_READ_REG_DWORD((U32) (pIfmCtrl->ulBaseAddress +
                                        HDMI_RX_IFM_VS_PERIOD)) & (HDMI_RX_IFM_VS_PERIODBitFieldMask));
  if (V_Pulse > (V_Period / 2))
    {
      SigPolarity = STM_HDMIRX_SIGNAL_POLARITY_NEGATIVE;
    }

  TRC(TRC_ID_HDMI_RX_CORE,"IFM: VSync Polarity :%d \n", SigPolarity);
  return SigPolarity;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_pixel_clk_freq_get()
 USAGE        	:   Calculate the pixel clock
 INPUT        	:   IFM Parameter structure
 RETURN       	:   Pixel Clock.
 USED_REGS	    :
******************************************************************************/
U32 sthdmirx_IFM_pixel_clk_freq_get(sthdmirx_IFM_context_t *pIfmCtrl, sthdmirx_IFM_timing_params_t IfmParam)
{
  U32 ulPixelClock = 0;
  U32 HTotal, VTotal;

  HTotal = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                     HDMI_RX_INPUT_IBD_HTOTAL)) & HDMI_INPUT_IBD_HTOTALBitFieldMask;
  VTotal = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                     HDMI_RX_INPUT_IBD_VTOTAL)) & HDMI_INPUT_IBD_VTOTALBitFieldMask;

  ulPixelClock = (U32) ((HTotal) * (VTotal) * (IfmParam.VFreq_Hz)) / 10;

  TRC(TRC_ID_HDMI_RX_CORE,"IFM: Pixel Clock Freq : %d\n", ulPixelClock);

  return ulPixelClock;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_signal_scantype_get()
 USAGE        	:   Returns Signal Scan Type ( Progressive or interlace)
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
#define HTOTAL_DELTA_CHECK      5

stm_hdmirx_signal_scan_type_t
sthdmirx_IFM_signal_scantype_get(sthdmirx_IFM_context_t *pIfmCtrl)
{

  sthdmirx_IFM_waitVSync_time(pIfmCtrl,1);

  if (sthdmirx_IFM_is_mode_overlapping(pIfmCtrl) == TRUE)
    {
      U32 HTotal;
      HTotal = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                         HDMI_RX_INPUT_IBD_HTOTAL)) & HDMI_INPUT_IBD_HTOTALBitFieldMask;

      TRC(TRC_ID_HDMI_RX_CORE,"\n  Overlap modes found .......................... !!!!!!!! \n");
      if (HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                    HDMI_RX_INPUT_IRQ_STATUS)) & HDMI_RX_INPUT_INTLC_ERR_MASK)
        {
          if ((abs(HTotal - 2300) >= HTOTAL_DELTA_CHECK)) //for 1080i australian mode
            return STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE;
        }
      return STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED;
    }
  else
    {
      if (HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                    HDMI_RX_INPUT_IRQ_STATUS)) & HDMI_RX_INPUT_INTLC_ERR_MASK)
        {
          return STM_HDMIRX_SIGNAL_SCAN_TYPE_PROGRESSIVE;
        }
      return STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED;
    }
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_waitVSync_time()
 USAGE        	:   Wait for requested VSync Time.
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
BOOL sthdmirx_IFM_waitVSync_time(sthdmirx_IFM_context_t *pIfmCtrl,
                                 U8 NoOfVsyncs)
{
  U32 DW_BeginTime;

  if (NoOfVsyncs == 0)
    {
      NoOfVsyncs++;
    }

  while (NoOfVsyncs--)
    {
      DW_BeginTime = stm_hdmirx_time_now();

      HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                 HDMI_RX_INPUT_IRQ_STATUS), HDMI_RX_INPUT_VS_MASK);
      while (!(HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                         HDMI_RX_INPUT_IRQ_STATUS)) & HDMI_RX_INPUT_VS_MASK))
        {
          if ((stm_hdmirx_time_minus(stm_hdmirx_time_now(),
                                     DW_BeginTime)) >= (M_NUM_TICKS_PER_MSEC(50)))
            {
              return FALSE;
            }
        }
    }
  return TRUE;

}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_get_AFRsignal_detect_status()
 USAGE        	:   return the AFR signal detect status, if AFR is triggered
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
BOOL sthdmirx_IFM_get_AFRsignal_detect_status(sthdmirx_IFM_context_t *pIfmCtrl)
{
  if (HDMI_READ_REG_DWORD((U32) ((U32) pIfmCtrl->ulBaseAddress +
                                 HDMI_RX_INPUT_IRQ_STATUS)) & HDMI_RX_INPUT_AFR_DETECT)
    {
      return TRUE;
    }
  return FALSE;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_clear_AFRsignal_detect_status()
 USAGE        	:   Clear the Auto Free Run Detect status
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_clear_AFRsignal_detect_status(
  sthdmirx_IFM_context_t *pIfmCtrl)
{
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_IRQ_STATUS), HDMI_RX_INPUT_AFR_DETECT);
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_clear_interlace_decode_error_status()
 USAGE        	:   Clear the Auto Free Run Detect status
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_clear_interlace_decode_error_status(
  sthdmirx_IFM_context_t *pIfmCtrl)
{
  HDMI_WRITE_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                             HDMI_RX_INPUT_IRQ_STATUS), HDMI_RX_INPUT_INTLC_ERR);
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_set_australianmode_interlace_detect(sthdmirx_IFM_context_t *pIfmCtrl)
 USAGE        	:   Australian mode detection
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_set_australianmode_interlace_detect(
  sthdmirx_IFM_context_t *pIfmCtrl)
{
  // Use the raw measurement, Re-Generated
  HDMI_CLEAR_REG_BITS_WORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                 HDMI_RX_IFM_CTRL), HDMI_IFM_FIELD_DETECT_MODE);
  HDMI_SET_REG_BITS_WORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                               HDMI_RX_IFM_CTRL), HDMI_IFM_ODD_INV);
  //This bit programming is added as the Australian mode LH was not getting detected .
  HDMI_WRITE_REG_DWORD(((U32) pIfmCtrl->ulBaseAddress +
                        HDMI_RX_IFM_CHANGE), 0x5267);

  return;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_reset_australianmode_interlace_detect(sthdmirx_IFM_context_t *pIfmCtrl)
 USAGE        	:   Reset australian mode detection
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
void sthdmirx_IFM_reset_australianmode_interlace_detect(
  sthdmirx_IFM_context_t *pIfmCtrl)
{
  // Use the raw measurement, Re-Generated
  HDMI_SET_REG_BITS_WORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                               HDMI_RX_IFM_CTRL), HDMI_IFM_FIELD_DETECT_MODE);
  HDMI_CLEAR_REG_BITS_WORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                 HDMI_RX_IFM_CTRL), HDMI_IFM_ODD_INV);
  //This bit programming is added as the Australian mode LH was not getting detected .
  HDMI_WRITE_REG_DWORD(((U32) pIfmCtrl->ulBaseAddress +
                        HDMI_RX_IFM_CHANGE), 0x67);

  return;
}

/******************************************************************************
 FUNCTION       :   sthdmirx_IFM_is_mode_overlapping()
 USAGE        	:   Set window mode detection
 INPUT        	:   IFM Parameter structure
 RETURN       	:
 USED_REGS	    :
******************************************************************************/
BOOL sthdmirx_IFM_is_mode_overlapping(sthdmirx_IFM_context_t *pIfmCtrl)
{
  U32 ulHperiod, ulVperiod;
  U32 Hfreq, Vfreq;

  ulHperiod = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                        HDMI_RX_IFM_HS_PERIOD)) & HDMI_RX_IFM_HS_PERIODBitFieldMask;
  ulVperiod = HDMI_READ_REG_DWORD((U32)((U32) pIfmCtrl->ulBaseAddress +
                                        HDMI_RX_IFM_VS_PERIOD)) & HDMI_RX_IFM_VS_PERIODBitFieldMask;

  if (ulHperiod == 0 || ulVperiod == 0)
  {
    TRC(TRC_ID_ERROR,"\n  Hperiod or Vperiod is equal to 0 !!!!!!!! \n");
    return (FALSE);
  }

  Hfreq = (U32) ((TCLK_FREQ_HZ / (ulHperiod)));
  Vfreq = (U32) ((U32) (TCLK_FREQ_HZ * 10) / (U32) (ulVperiod * ulHperiod));

  /* Check is the mode is overlapping (576p , 1080i Aus modes) */
  if (((Hfreq >= (OVERLAP_HFREQ - DELTA_HFREQ)) &&
       (Hfreq <= (OVERLAP_HFREQ + DELTA_HFREQ))) &&
      ((Vfreq >= (OVERLAP_VFREQ - DELTA_VFREQ)) &&
       (Vfreq <= (OVERLAP_VFREQ + DELTA_VFREQ))))
    {
      TRC(TRC_ID_HDMI_RX_CORE,"\n\n Overlap True \n");
      return (TRUE);
    }
  else
    {
      TRC(TRC_ID_HDMI_RX_CORE,"\n\n Hfreq %d", Hfreq);
      TRC(TRC_ID_HDMI_RX_CORE,"   Vfreq %d\n", Vfreq);
      TRC(TRC_ID_HDMI_RX_CORE,"\n\n Overlap FALSE \n");
      return (FALSE);
    }

}

/* End of file */
