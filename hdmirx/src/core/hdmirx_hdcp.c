/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


/* Standard Includes ----------------------------------------------*/

/* Include of Other module interface headers --------------------------*/

/* Local Includes -------------------------------------------------*/

#include <hdmirx_drv.h>
#include <hdmirx_core.h>
#include <InternalTypes.h>
#include <hdmirx_RegOffsets.h>
#include <linux/ktime.h>
#include <stm_display.h>
#include <linux/kthread.h>
#include <stm_hdmirx.h>
#include <linux/freezer.h>

/* Private Typedef -----------------------------------------------*/

/* Private Defines ------------------------------------------------*/

/* Private macro's ------------------------------------------------*/

/* Private Variables -----------------------------------------------*/

/* Private functions prototypes ------------------------------------*/
int sthdmirx_Core_HDCP_provide_downstream_ksv_thread(void * data);
static int thread_vib_ksvfifo[2] = { 0, 0 };
/* Interface procedures/functions ----------------------------------*/
/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_init
 USAGE        :     Hdcp Initialization
 INPUT        :
 RETURN       :
 USED_REGS    :
******************************************************************************/
void sthdmirx_CORE_HDCP_init(const hdmirx_handle_t Handle)
{
  hdmirx_route_handle_t *Handle_p;
  U32 Read_reg;
  Handle_p = (hdmirx_route_handle_t *) Handle;
  /**************************************************************************
      1. Clock Switch to TCLK should be there
      2. Key must be preloaded to Key Ram by secure processor
  ****************************************************************************/
  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDRX_KEYHOST_CONTROL), 0x10);
  STHDMIRX_DELAY_1ms(100);
  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDRX_KEYHOST_CONTROL), 0x01);
  /*HDCP key loading*/
  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDCP_T1_INIT_CTRL), 0x01);
  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDCP_KEY_RD_CTRL), 0x01);

  STHDMIRX_DELAY_1ms(1000);
  /*Loading of the keys*/

  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDCP_KEY_RD_STATUS), 0xFF);
  STHDMIRX_DELAY_1ms(200);

  Read_reg= HDMI_READ_REG_WORD((U32)(Handle_p->BaseAddress + HDCP_KEY_RD_STATUS));
  #if 0 /*for debug purpose*/
  if(Read_reg==2)
    TRC(TRC_ID_ERROR,"HDMI_HDCP: Error in loading the keys !!!!!\n");
  #endif
  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDCP_T1_INIT_CTRL), 0x0);
  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDCP_KEY_RD_CTRL), 0x0);

  HDMI_WRITE_REG_WORD((Handle_p->BaseAddress + HDRX_KEYHOST_CONTROL), 0x2);
  if ((Handle_p->HdmiRxMode != STM_HDMIRX_ROUTE_OP_MODE_DVI))
    {
      /* HDCP 1.1 without options and Enable HDCP with encrypted keys */
      HDMI_SET_REG_BITS_WORD((U32)(Handle_p->BaseAddress + HDRX_HDCP_CONTROL),
                          (HDRX_HDCP_HDMI_CAPABLE | HDRX_HDCP_AVMUTE_AUTO_EN |
                           HDRX_HDCP_AVMUTE_IGNORE));
    }
  /* Timebase is computed for a TCLK f 30MHz */
  HDMI_WRITE_REG_DWORD((U32) (Handle_p->BaseAddress + HDRX_HDCP_TIMEBASE ), 0x002DC6C0);

  /* Watchdog is computed to wait 5 second for the downstreams to be ready */
  HDMI_WRITE_REG_WORD((U32) (Handle_p->BaseAddress + HDRX_HDCP_TIMEOUT ), 0x320);

  /* Set register AKSV to be monitored to generate authencition detected event*/
  HDMI_WRITE_REG_WORD((U32) (Handle_p->BaseAddress + HDRX_HDCP_TBL_IRQ_INDEX ), 0x10);

  /* Disable I2C communication */
  HDMI_WRITE_REG_BYTE((U32) (Handle_p->BaseAddress + HDRX_HDCP_ADDR), 0x00);

  /* Clear authentication */
  HDMI_WRITE_REG_BYTE((U32)(Handle_p->BaseAddress + HDRX_HDCP_STATUS_EXT),
                      HDRX_HDCP_RE_AUTHENTICATION);

  /* Enable key loading */
  HDMI_SET_REG_BITS_WORD((U32)(Handle_p->BaseAddress + HDRX_HDCP_CONTROL),
                         HDRX_HDCP_BKSV_LOAD_EN);

  /* Delay should be here for 1 ms */
  STHDMIRX_DELAY_1ms(1);

  /* Clear authentication */
  HDMI_WRITE_REG_BYTE((U32)(Handle_p->BaseAddress + HDRX_HDCP_STATUS_EXT),
                      HDRX_HDCP_RE_AUTHENTICATION);

  /*Disable HDCP */
  HDMI_CLEAR_REG_BITS_WORD((U32)(Handle_p->BaseAddress + HDRX_HDCP_CONTROL),
                           HDRX_HDCP_EN);


  Handle_p->DownstreamKsvSize = 0;
  Handle_p->repeater_fn_support =true;
  Handle_p->RepeaterReady =false;
  memset(&Handle_p->DownstreamKsvList[0], 0, 127*5);
  Handle_p->ksv_thread=NULL;
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_start_KSV_task
 USAGE        :     start Hdcp KSV fifo filling task
 INPUT        :
 RETURN       :
 USED_REGS    :
******************************************************************************/
int sthdmirx_CORE_HDCP_start_KSV_task(const hdmirx_route_handle_t * Handle)
{
  hdmirx_route_handle_t *Handle_p;
  int ret;
  Handle_p = (hdmirx_route_handle_t *) Handle;

  //Handle_p->ksv_thread->exit = 0;
  //Handle_p->ksv_thread->thread = NULL;

  if (Handle_p->ksv_thread != NULL) // Already started
    return 0;

  ret= stm_hdmirx_thread_create(&Handle_p->ksv_thread,
                                (void (*)(void *))sthdmirx_Core_HDCP_provide_downstream_ksv_thread,
                                (void *)Handle_p,
                                "HDMIRx_KSV_FIFO_task", thread_vib_ksvfifo);

  if (ret)
    {
      TRC(TRC_ID_ERROR,"%s:hdmirx task creation fail \n",
                 __func__);
      return -ENOMEM;
    }
  return 0;
}
/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_stop_KSV_task
 USAGE        :     stop Hdcp KSV fifo filling task
 INPUT        :
 RETURN       :
 USED_REGS    :
******************************************************************************/
int sthdmirx_CORE_HDCP_stop_KSV_task(const hdmirx_route_handle_t * Handle)
{
  hdmirx_route_handle_t *Handle_p;
  int ret;
  Handle_p = (hdmirx_route_handle_t *) Handle;
  //dHandle_p->HdmiRx_Task->exit = 1;	/*Gets task out of while(1) loop */
  /*stop a thread created by kthread_create() */

  if (Handle_p->ksv_thread == NULL) // Already stopped
    return 0;

  ret = stm_hdmirx_thread_wait(&Handle_p->ksv_thread);
  Handle_p->ksv_thread=NULL;
  if (ret)
    TRC(TRC_ID_ERROR,"%s Failed in stop the HdmiRx task = %d\n",  __func__, ret);
  return ret;
}
/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_enable
 USAGE        :     Enable/Disable the hdcp blocks
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_CORE_HDCP_enable(const hdmirx_handle_t Handle, BOOL B_Enable)
{
  if (B_Enable)
    {
       HDMI_CLEAR_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
                                        (HDRX_HDCP_DOWNSTREAM_READY | HDRX_HDCP_KSV_LIST_READY)) ;

       HDMI_SET_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_BSTATUS),
                                        0x0);
       HDMI_SET_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) +
                                    HDRX_HDCP_CONTROL), (HDRX_HDCP_EN | HDRX_HDCP_DECRYPT_EN |
                                                         HDRX_HDCP_BKSV_LOAD_EN));
    }
  else
    {
      HDMI_CLEAR_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) +
                                     HDRX_HDCP_CONTROL), HDRX_HDCP_EN);
    }

}


/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_set_HDCP_TWSaddrs
 USAGE        :     Set The I2C slave Address
 INPUT        :
 RETURN       :
 USED_REGS    :
******************************************************************************/
void sthdmirx_CORE_set_HDCP_TWSaddrs(const hdmirx_handle_t Handle, U8 Addrs)
{
  U8 tWSAddrs;
  tWSAddrs = HDMI_READ_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) +
                                 HDRX_HDCP_ADDR)) & 0x80;
  tWSAddrs |= (Addrs & 0x7f);
  tWSAddrs |=0x80;
  HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_ADDR),
                      tWSAddrs);
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_is_encryption_enabled
 USAGE        :
 INPUT        :
 RETURN       :
 USED_REGS    :
******************************************************************************/
stm_hdmirx_route_hdcp_decryption_status_t sthdmirx_CORE_HDCP_is_encryption_enabled(
                                const hdmirx_handle_t Handle)
{
  stm_hdmirx_route_hdcp_decryption_status_t FrameStatus= STM_HDMIRX_ROUTE_HDCP_FRAME_NOT_DECRYPTED;

  if (HDMI_READ_REG_BYTE((GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_STATUS))
      & HDRX_HDCP_DEC_STATUS)
    {
      FrameStatus = STM_HDMIRX_ROUTE_HDCP_FRAME_DECRYPTED;
    }

  return FrameStatus;
}
/******************************************************************************
 FUNCTION     :   sthdmirx_CORE_HDCP_Frame_Decryption_Loss
 USAGE        :   Get HDCP Authentication Detection
 INPUT        :   None
 RETURN       :
 USED_REGS    :
******************************************************************************/
BOOL sthdmirx_CORE_HDCP_Frame_Decryption_Loss(
     hdmirx_route_handle_t *pInpHandle)
{

  if (HDMI_READ_REG_BYTE((GET_CORE_BASE_ADDRS(pInpHandle) + HDRX_HDCP_STATUS))
      & HDRX_HDCP_DEC_STATUS)
    {
      return FALSE;
    }

  return TRUE;
}
/******************************************************************************
  FUNCTION     :     sthdmirx_CORE_HDCP_is_repeater_enabled
  USAGE        :
  INPUT        :
  RETURN       :
  USED_REGS    :
 ******************************************************************************/
BOOL sthdmirx_CORE_HDCP_is_repeater_enabled(const hdmirx_handle_t Handle)
{
  if (HDMI_READ_REG_BYTE(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL)
      & HDRX_HDCP_REPEATER)
   {
     return TRUE;
   }
  return FALSE;
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_is_repeater_ready
 USAGE        :
 INPUT        :
 RETURN       :
 USED_REGS    :
******************************************************************************/
BOOL sthdmirx_CORE_HDCP_is_repeater_ready(const hdmirx_handle_t Handle)
{
  if (HDMI_READ_REG_WORD(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_STATUS)
         & HDRX_HDCP_STATE_WAIT_DWNSTRM_STS)
    {
      return TRUE;
    }
  return FALSE;
}

/******************************************************************************
 FUNCTION     :   sthdmirx_CORE_HDCP_status_get
 USAGE        :   Get HDCP authentication status from Hdmi Hardware
 INPUT        :   None
 RETURN       :
 USED_REGS    :   HDRX_SDP_STATUS, HDRX_SDP_STATUS
******************************************************************************/
stm_hdmirx_route_hdcp_status_t sthdmirx_CORE_HDCP_status_get(const
    hdmirx_route_handle_t *pInpHandle)
{
  stm_hdmirx_route_hdcp_status_t Status;
  if (HDMI_READ_REG_BYTE((GET_CORE_BASE_ADDRS(pInpHandle) +
                          HDRX_HDCP_STATUS)) & HDRX_HDCP_AUTHENTICATION)
    {
      Status = STM_HDMIRX_ROUTE_HDCP_STATUS_AUTHENTICATION_DETECTED;
      if (HDMI_READ_REG_BYTE((U32) (GET_CORE_BASE_ADDRS(pInpHandle) +
                                    HDRX_SDP_STATUS)) & HDRX_NOISE_DETECTED)
        {
          Status = STM_HDMIRX_ROUTE_HDCP_STATUS_NOISE_DETECTED;
        }
    }
  else
    {
      Status = STM_HDMIRX_ROUTE_HDCP_STATUS_NOT_AUTHENTICATED;
    }

  return Status;
}
/******************************************************************************
 FUNCTION     :   sthdmirx_CORE_HDCP_Authentication_Detection
 USAGE        :   Get HDCP Authentication Detection
 INPUT        :   None
 RETURN       :
 USED_REGS    :
******************************************************************************/
BOOL sthdmirx_CORE_HDCP_Authentication_Detection(
     hdmirx_route_handle_t *const pInpHandle)
{
  hdmirx_route_handle_t *Handle_p = (hdmirx_route_handle_t *)pInpHandle;
  if (HDMI_READ_REG_BYTE((GET_CORE_BASE_ADDRS(pInpHandle) +
      HDRX_HDCP_STATUS_EXT )) & HDRX_HDCP_RE_AUTHENTICATION)
    {
        Handle_p->RepeaterReady = false;
        if (HDMI_READ_REG_BYTE(GET_CORE_BASE_ADDRS(pInpHandle) + HDRX_HDCP_UPSTREAM_CTRL)& HDRX_HDCP_REPEATER)
        {
          HDMI_CLEAR_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(pInpHandle) + HDRX_HDCP_UPSTREAM_CTRL),
                                   (HDRX_HDCP_KSV_LIST_READY | HDRX_HDCP_DOWNSTREAM_READY));

          HDMI_SET_REG_BITS_BYTE((U32)(GET_CORE_BASE_ADDRS(pInpHandle) + HDRX_HDCP_UPSTREAM_CTRL),
                                   HDRX_HDCP_KSV_FIFO_RESET );

          stm_hdmirx_delay_us(5);
        }

      HDMI_SET_REG_BITS_BYTE( (U32)(GET_CORE_BASE_ADDRS(pInpHandle) + HDRX_HDCP_STATUS_EXT), HDRX_HDCP_RE_AUTHENTICATION);

    return TRUE;
    }

  return FALSE;
}
/******************************************************************************
 FUNCTION     :   sthdmirx_CORE_HDCP_Authentication_clear
 USAGE        :   Get HDCP Authentication Detection
 INPUT        :   None
 RETURN       :
 USED_REGS    :
******************************************************************************/
void sthdmirx_CORE_HDCP_Authentication_Clear(
     hdmirx_route_handle_t *const pInpHandle)
{

  if (HDMI_READ_REG_BYTE((GET_CORE_BASE_ADDRS(pInpHandle) +
      HDRX_HDCP_STATUS_EXT )) & HDRX_HDCP_RE_AUTHENTICATION)
    {
      HDMI_SET_REG_BITS_BYTE( (U32)(GET_CORE_BASE_ADDRS(pInpHandle) + HDRX_HDCP_STATUS_EXT), HDRX_HDCP_RE_AUTHENTICATION);

    }
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_set_repeater_mode_enable
 USAGE        :     Enable/Disable the hdcp repeater mode
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_CORE_HDCP_set_repeater_mode_enable(const hdmirx_handle_t Handle, BOOL B_Enable)
{
  hdmirx_route_handle_t *Handle_p = (hdmirx_route_handle_t *)Handle;
  Handle_p->RepeaterReady = false;
  if (B_Enable)
    {
      HDMI_SET_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
                                HDRX_HDCP_REPEATER);
      HDMI_SET_REG_BITS_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
                                HDRX_HDCP_KSV_FIFO_RESET );
    }
    else {
      HDMI_CLEAR_REG_BITS_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
                                (HDRX_HDCP_REPEATER | HDRX_HDCP_KSV_LIST_READY | HDRX_HDCP_DOWNSTREAM_READY)) ;
    }
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_set_repeater_downstream_status
 USAGE        :     Set Downstream info in status register
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_CORE_HDCP_set_repeater_downstream_status(const hdmirx_handle_t Handle,
                                                       stm_hdmirx_hdcp_downstream_status_t *status)
{
   U32 reg =
   (status->max_cascade_exceeded?HDRX_HDCP_MAX_CASCADE_EXCEEDED:0x0) |
   ((status->depth & 0x07) << 8) |
   (status->max_devs_exceeded?HDRX_HDCP_MAX_DEV_EXCEEDED:0x0) |
   (status->device_count & 0x7F);
    HDMI_WRITE_REG_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_BSTATUS),
                             reg);
}

/**
*      SHA_Ft - Computing Function used in SHA Algorithm.
*      @t: round number
*      @B,C,D: Variable to hash
*
*      RETURNS:
*      the result of the hash

*/

static uint32_t SHA_Ft(U8 t ,U32 B,U32 C,U32 D)
{
  if(t<20)
    {
      return (B & C) | ((0xFFFFFFFF-B) & D);
    }
  else if (t<40)
    {
      return (B ^ C ^ D);
    }
  else if (t<60)
    {
      return (B & C) | (B & D) | (C & D) ;
    }
  else/*t<80*/
    {
      return (B ^ C ^ D);
    }
}

/**
*      SHA_K - Rounding Constant used in SHA-1 Algorithm.
*      @t: round number
*
*      RETURNS:
*      the result of the SHA-1
*/
static uint32_t SHA_K(U8 t)
{
  if(t<20)
    {
      return 0x5a827999;
    }
  else if (t<40)
    {
      return 0x6ed9eba1;
    }
  else if (t<60)
    {
      return 0x8f1bbcdc;
    }
  else/*t<80*/
    {
      return 0xca62c1d6;
    }
}

/**
*      SHA_Shift - Rolling to the left by n bits shifted back from right
*              Shift_n(X) = (X << n) OR (X >> 32-n);
*      @t: round number
*
*      RETURNS:
*      the result of the SHA shift
*/
static U32 SHA_Shift(U8 n, U32 X)
{
  return ( (X << n) | (X >> (32-n)) );
}
/**
*      SHA_1 -
*      @ArrayKSV_p: Pointer  to the table containing the KSV, BStatus and Mi value
*      @ByteNumber: Size of the structure ArrayKSV_p in byte
*      @Result_p: pointer to the memory to store the result of the SHA-1 algorithm
*/
static void SHA_1(U8* ArrayKSV_p, U16 ByteNumber, U32* Result_p)
{
  /*const K = [0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6]; generated an error here*/
  U16 N_512_pad,i,t,j;
  U32 Msg_bit_Length;/*Limitation to the SHA, only 32 not 64 bit total size*/
  U32 A,B,C,D,E,H0,H1,H2,H3,H4,W[80],TEMP;


  /*-------------------------------------------------------------------------------
Step 1 : Padding
-------------------------------------------------------------------------------*/
  N_512_pad = ( (ByteNumber+8) / 64)+1; /*for 3 bytes-> 1 block, 55 -> 1, 56 ? ->, 57 -> 2*/
  ArrayKSV_p[ByteNumber] = 0x80; /*1 and 000 0000 are padded */

  for(i=ByteNumber+1;i<N_512_pad*64-4;i++)
    {
      ArrayKSV_p[i] = 0;
    }
  Msg_bit_Length = ByteNumber * 8;
  ArrayKSV_p[N_512_pad*64-4] = Msg_bit_Length>>24;
  ArrayKSV_p[N_512_pad*64-3] = Msg_bit_Length>>16;
  ArrayKSV_p[N_512_pad*64-2] = Msg_bit_Length>>8;
  ArrayKSV_p[N_512_pad*64-1] = Msg_bit_Length;

  /*-------------------------------------------------------------------------------
Padding is OK till this point
Step 2 :
   * Computing Hash by 512 bit blocks for(i)
-------------------------------------------------------------------------------*/

  H0 = 0x67452301;
  H1 = 0xEFCDAB89;
  H2 = 0x98BADCFE;
  H3 = 0x10325476;
  H4 = 0xC3D2E1F0;

  for(i=0;i<N_512_pad;i++) /* (N_512_pad) blocks will be computed */
    {
      A = H0; B = H1; C = H2; D = H3; E = H4;
      for (j=0;j<16;j++) /* 16 loop, 4 each => 64 byte, 512 bit block treated*/
        {
          W[j] = ArrayKSV_p[(i*64)+4*j] * 16777216 + ArrayKSV_p[(i*64)+4*j+1]*65536 + ArrayKSV_p[(i*64)+4*j+2] *256 + ArrayKSV_p[(i*64)+4*j+3];

        }
      for (t=16;t<80;t++)
        {
          W[t] = SHA_Shift(1,W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16]);
        }
      for (t=0;t<80;t++)
        {
          TEMP = SHA_Shift(5,(A)) + SHA_Ft(t,B,C,D) + E + W[t] + SHA_K(t);
          E = D; D = C; C = SHA_Shift(30,(B)); B = A; A = TEMP;
        }
      H0 = H0 + A; H1 = H1 + B; H2 = H2 + C; H3 = H3 + D; H4 = H4 + E;
    }

  Result_p[0] = H0;
  Result_p[1] = H1;
  Result_p[2] = H2;
  Result_p[3] = H3;
  Result_p[4] = H4;
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_set_downstream_ksv_list
 USAGE        :     Set Downstream info in status register
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_CORE_HDCP_set_downstream_ksv_list(hdmirx_route_handle_t * Handle,
                                                U8 *ksv_list,
                                                U32 size)
{
  int i;
  U32 hash_result[5];
  U8 value[sizeof(U8)*11*64];
  U32 m0[2];
  memset( &value[0], 0, sizeof(U8)*64*11);

  memcpy(&Handle->DownstreamKsvList[0], &ksv_list[0], sizeof(U8)*127*5);
  Handle->DownstreamKsvSize = size;
  HDMI_SET_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
      HDRX_HDCP_DOWNSTREAM_READY);


  for (i=0 ; i<Handle->DownstreamKsvSize ; i++)
    {
      value[i] = Handle->DownstreamKsvList[i];
      TRC(TRC_ID_HDMI_RX_CORE,"From HDMIRX driver KSV[%d] =0x%x\n", i, value[i]);
    }

  HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_TBL_RD_INDEX ), 0x41) ;
  value[i++] = HDMI_READ_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_TBL_RD_DATA )) ;

  HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_TBL_RD_INDEX ), 0x42) ;
  value[i++] = HDMI_READ_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_TBL_RD_DATA )) ;
  TRC(TRC_ID_HDMI_RX_CORE,"Bstatus value is : 0x%02x%02x\n",value[i-1],value[i-2]);
  m0[0] = HDMI_READ_REG_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_M0_0 ));
  TRC(TRC_ID_HDMI_RX_CORE,"M0 value of HDRX_HDCP_M0_0 register : 0x%08x\n",m0[0]);
  m0[1] = HDMI_READ_REG_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_M0_1 ));
  TRC(TRC_ID_HDMI_RX_CORE,"M0 value of HDRX_HDCP_M0_1 register : 0x%08x\n",m0[1]);


  value[i++] = (m0[0] >> 0 ) & 0xff;
  value[i++] = (m0[0] >> 8 ) & 0xff;
  value[i++] = (m0[0] >> 16) & 0xff;
  value[i++] = (m0[0] >> 24) & 0xff;
  value[i++] = (m0[1] >> 0 ) & 0xff;
  value[i++] = (m0[1] >> 8 ) & 0xff;
  value[i++] = (m0[1] >> 16) & 0xff;
  value[i++] = (m0[1] >> 24) & 0xff;

  SHA_1(&(value[0]), i, &(hash_result[0]));

  TRC(TRC_ID_HDMI_RX_CORE,"Write V' into register : 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",hash_result[0],hash_result[1],hash_result[2],hash_result[3],hash_result[4]);

  HDMI_WRITE_REG_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_V0_0 ), hash_result[0]);
  HDMI_WRITE_REG_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_V0_1 ), hash_result[1]);
  HDMI_WRITE_REG_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_V0_2 ), hash_result[2]);
  HDMI_WRITE_REG_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_V0_3 ), hash_result[3]);
  HDMI_WRITE_REG_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_V0_4 ), hash_result[4]);
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_set_repeater_status_ready
 USAGE        :     Provide Fifo Size, Depth, V' and KSV Fifo list to the Upstream
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_CORE_HDCP_set_repeater_status_ready(hdmirx_route_handle_t * Handle, BOOL B_Ready)
{
  Handle->RepeaterReady = B_Ready;
}
/******************************************************************************
 FUNCTION     :     Thread to provide KSV Fifo to upstream
 USAGE        :     Provide Fifo Size, Depth, V' and KSV Fifo list to the Upstream
 INPUT        :     Handle
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
#define FIFO_SIZE 10
int sthdmirx_Core_HDCP_provide_downstream_ksv_thread(void * data) {
  U8 status;
  int i,j;
  unsigned int read_cnt;
  U8 ksv_list[127*5];
  U32 size;
  bool standby = false;

  hdmirx_route_handle_t * Handle = (hdmirx_route_handle_t *) data;

  TRC(TRC_ID_HDMI_RX_CORE,"Starting KSV Fifo Thread\n");

  while(!kthread_should_stop()) {

      set_current_state(TASK_UNINTERRUPTIBLE);
      schedule_timeout(HZ/50);

      if (kthread_should_stop())
        break;

      if (!Handle->RepeaterReady)
        continue;

      size = Handle->DownstreamKsvSize;
      memcpy(&(ksv_list[0]), &(Handle->DownstreamKsvList[0]), 127*5);
      i = 0;
      if (HDMI_READ_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO_STS )) &
          HDRX_HDCP_KSV_FIFO_E) {
          // Write the FIFO_SIZE first bytes into the fifo
          for (j = 0 ; (i<size) && (j<FIFO_SIZE) ; j++){
              HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO),
                  ksv_list[i++]);
                  stm_hdmirx_delay_us(1);
          }
      }

      HDMI_SET_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
          HDRX_HDCP_KSV_LIST_READY);

      read_cnt = 0;
      standby=false;
      while (!kthread_should_stop() ) {

          if (!Handle->RepeaterReady) {
              HDMI_SET_REG_BITS_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
              HDRX_HDCP_KSV_FIFO_RESET );

              HDMI_CLEAR_REG_BITS_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO_CTRL),
              HDRX_HDCP_KSV_FIFO_SCL_STRCH_EN);
              break;
          }

          status = HDMI_READ_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO_STS ));

          if ( i == size) {
              i = 0;
              read_cnt++;
          }

          if(status & HDRX_HDCP_KSV_FIFO_F) { // FIFO full : wait for a read
              if (read_cnt!=0) {
                // FIFO Full and FIFO completly read at least once
                // Set the thread in standby to save CPU
                standby = true;
              }
              set_current_state(TASK_UNINTERRUPTIBLE);
              if (standby)
                schedule_timeout(HZ/50);
              else
                stm_hdmirx_delay_us(10);
              continue;
          }

          standby = false; //Not full reset standby

          if(status & HDRX_HDCP_KSV_FIFO_E) { // FIFO Empty : push 10 bytes
              for (j = 0 ; (i<size) && (j<FIFO_SIZE) ; j++){
                  HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO),
                      ksv_list[i++]);
              }
              continue;
          }

          if(status & HDRX_HDCP_KSV_FIFO_AE) { // FIFO Almost Empty : push 8 bytes
              for (j = 0 ; (i<size) && (j<8) ; j++){
                  HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO),
                      ksv_list[i++]);
              }
              continue;
          }

          if (status & HDRX_HDCP_KSV_FIFO_AF) { // FIFO Almost but not Full : push 1 bytes
              HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO),
                  ksv_list[i++]);
              continue;
          }

          if (status & HDRX_HDCP_KSV_FIFO_NF) {
              HDMI_WRITE_REG_BYTE((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_KSV_FIFO),
                  ksv_list[i++]);
              continue;
          }

          if(status & HDRX_HDCP_KSV_FIFO_ERROR) { // FIFO full : wait for a read
              HDMI_SET_REG_BITS_WORD((U32)(GET_CORE_BASE_ADDRS(Handle) + HDRX_HDCP_UPSTREAM_CTRL),
                  HDRX_HDCP_KSV_FIFO_RESET );
              i = 0;
              continue;
          }

      }
  }
  TRC(TRC_ID_HDMI_RX_CORE,"Stopping KSV Fifo Thread\n");
  return 0;
}

/******************************************************************************
 FUNCTION     :     sthdmirx_CORE_HDCP_Reset_Hdmi_Mode
 USAGE        :     Reset Hdmi Mode hdcp
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_CORE_HDCP_Reset_Hdmi_Mode(const hdmirx_handle_t Handle, BOOL B_Reset)
{

  if (B_Reset)
    {
       HDMI_SET_REG_BITS_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) +
                                    HDRX_HDCP_CONTROL), HDRX_HDCP_FORCE_HDMI_MODE_RST);
    }
  else
    {
      HDMI_CLEAR_REG_BITS_DWORD((U32)(GET_CORE_BASE_ADDRS(Handle) +
                                     HDRX_HDCP_CONTROL), HDRX_HDCP_FORCE_HDMI_MODE_RST);
    }
    stm_hdmirx_delay_us(10);
}

/* End of file */
