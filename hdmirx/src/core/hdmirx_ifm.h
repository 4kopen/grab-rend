/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __HDMIRX_IFM_H__
#define __HDMIRX_IFM_H__

#include "stddefs_hdmirx.h"
#include <stm_hdmirx.h>

typedef struct
{
  U16 Hstart;
  U16 Width;
  U16 Vstart;
  U16 Length;
} sthdmirx_timing_window_param_t;

typedef enum
{
  STHDRX_IBD_MEASURE_SOURCE,
  STHDRX_IBD_MEASURE_WINDOW,
  STHDRX_IBD_RGB_SELECT,
  STHDRX_IBD_THRESHOLD,
  STHDRX_IBD_ENABLE,
} sthdmirx_IBD_ctrl_option_t;

enum
{
  STHDRX_IBD_MEASURE_DATA,
  STHDRX_IBD_MEASURE_DE
};

enum
{
  STHDRX_IBD_RGB_FIELD_SEL,
  STHDRX_IBD_RED_FIELD_SEL,
  STHDRX_IBD_GREEN_FIELD_SEL,
  STHDRX_IBD_BLUE_FIELD_SEL
};

typedef struct
{
  U16 ScanType;
  U16 HPeriod;
  U16 VPeriod;
  U16 HPulse;
  U16 VPulse;
  U32 HFreq_Hz;
  U16 VFreq_Hz;
  U16 HTotal;
  U16 VTotal;
  U16 Hstart;
  U16 Width;
  U16 Vstart;
  U16 Length;
  U32 LineClk_Hz;
  U32 PixClk_Hz;
  stm_hdmirx_signal_polarity_t H_SyncPolarity;
  stm_hdmirx_signal_polarity_t V_SyncPolarity;
} sthdmirx_IFM_timing_params_t;

typedef struct {
  uint16_t vic;
  uint32_t pixclk_dived_1001;
  uint32_t pixclk;
  uint32_t h_freq_divided_1001;
  uint32_t h_freq;
  uint32_t v_freq_divided_1001;
  uint32_t v_freq;
  uint16_t h_back_porch;
  uint16_t h_pulse_width;
  uint16_t h_front_porch;
  uint16_t h_active_pixels;
  uint16_t h_total_pixels;
  uint16_t v_back_porch;
  uint16_t v_pulse_width;
  uint16_t v_front_porch;
  uint16_t v_active_lines;
  uint16_t v_total_lines;
  stm_hdmirx_signal_scan_type_t scan_type;
  stm_hdmirx_signal_polarity_t  h_polarity;
  stm_hdmirx_signal_polarity_t  v_polarity;
} sthdmirx_timing_desc_t;


typedef struct
{
  U32 ulBaseAddress;
  U32 ulMeasTCLK_Hz;
  BOOL bIsAWDCaptureEnable;
  sthdmirx_IFM_timing_params_t IFMTimingData;
} sthdmirx_IFM_context_t;


void sthdmirx_IFM_instrument_initialize(sthdmirx_IFM_context_t *pIfmCtrl);
void sthdmirx_IFM_init(sthdmirx_IFM_context_t *pIfmCtrl);
BOOL sthdmirx_IFM_signal_timing_get(sthdmirx_IFM_context_t *pIfmCtrl);
void sthdmirx_IFM_active_window_set(sthdmirx_IFM_context_t *pIfmCtrl,
                                    sthdmirx_timing_window_param_t *mWindow);
void sthdmirx_IFM_IBD_meas_window_set(sthdmirx_IFM_context_t *pIfmCtrl,
                                      sthdmirx_timing_window_param_t *mWindow);
void sthdmirx_IFM_IBD_config(sthdmirx_IFM_context_t *pIfmCtrl,
                             sthdmirx_IBD_ctrl_option_t Option, U8 uValue);
stm_hdmirx_signal_polarity_t
sthdmirx_IFM_HSync_polarity_get(sthdmirx_IFM_context_t *IFM_Control);
stm_hdmirx_signal_polarity_t
sthdmirx_IFM_VSync_polarity_get(sthdmirx_IFM_context_t *IFM_Control);
U32 sthdmirx_IFM_pixel_clk_freq_get(sthdmirx_IFM_context_t *pIfmCtrl, sthdmirx_IFM_timing_params_t IfmParam);
stm_hdmirx_signal_scan_type_t
sthdmirx_IFM_signal_scantype_get(sthdmirx_IFM_context_t *pIfmCtrl);
BOOL sthdmirx_IFM_get_AFRsignal_detect_status(
  sthdmirx_IFM_context_t *pIfmCtrl);
void sthdmirx_IFM_clear_AFRsignal_detect_status(
  sthdmirx_IFM_context_t *pIfmCtrl);
void sthdmirx_IFM_clear_interlace_decode_error_status(
  sthdmirx_IFM_context_t *pIfmCtrl);
BOOL sthdmirx_IFM_waitVSync_time(sthdmirx_IFM_context_t *pIfmCtrl,
                                 U8 NoOfVsyncs);
BOOL sthdmirx_IFM_is_mode_overlapping(sthdmirx_IFM_context_t *pIfmCtrl);
void sthdmirx_IFM_set_australianmode_interlace_detect(
  sthdmirx_IFM_context_t *pIfmCtrl);
void sthdmirx_IFM_reset_australianmode_interlace_detect(
  sthdmirx_IFM_context_t *pIfmCtrl);

#endif /*end of __HDMIRX_IFM_H__ */
