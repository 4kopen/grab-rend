/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


/* Standard Includes ----------------------------------------------*/

/* Include of Other module interface headers --------------------------*/

/* Local Includes -------------------------------------------------*/

#include "hdmirx_drv.h"
#include "hdmirx_core.h"
#include "hdmirx_utility.h"
#include "InternalTypes.h"
#include "hdmirx_core_export.h"
#include "hdmirx_system.h"
#include "stm_hdmirx_os.h"
#include "hdmirx_Combophy.h"
#include <hdmirx_RegOffsets.h>
#ifdef STHDMIRX_CLOCK_GEN_ENABLE
#include "hdmirx_clkgen.h"
#include "hdmirx_clkgenreg.h"
#endif

/* Private Typedef -----------------------------------------------*/

/* Private Defines ------------------------------------------------*/

/* Private macro's ------------------------------------------------*/

#define     HDMI_ABNORMAL_SYNC_TRESHOLD         200	/*msec. */
#define     MEASUREMENT_DONE_PERIOD             700	/*ms,  measurements have to be completed within this period */
#define     VTOTAL_THRESH                       100
#define     HTOTAL_THRESH                       100
#define     DVI_CLOCK_THRESH                    22	/*MHz */
#define     NO_SIGNAL_VIDEO_DDS_FREQ_HZ         96000000UL	/*96MHz */
#define     NO_SIGNAL_AUDIO_DDS_FREQ_HZ         12000000UL	/*12MHz =  128*fs = 128 * 48000 Hz */
#define     HDMI_AVI_INFOFRAME_AVBL_CHECK_TIMEOUT         200	/* 4x frame time msec. */

/* Private Constants -----------------------------------------------*/
const HDRX_AVI_INFOFRAME VGA_default_InfoFrame =
{
  .Y = 0,
  .A = 0,
  .B = 0,
  .C = 0,
  .EC = 0,
  .M = 1,
  .Q = 0,
  .R = 8,
  .S = 0,
  .SC = 0,
  .ITC = 0,
  .VIC = 1,
  .YQ = 0,
  .CN = 0,
  .PR = 0,
  .EndOfTopBar = 0,
  .StartOfBottomBar = 0,
  .EndOfLeftBar = 0,
  .StartOfRightBar = 0,
  .CheckSum = 0x56,
  .PB1_Rsvd = 0,
  .PB4_Rsvd = 0
};

/* Private Variables -----------------------------------------------*/
BOOL overlapFlag = FALSE;
extern BOOL InFsTransition;
static BOOL NeedVideoEvt = FALSE;

/* Private functions prototypes ------------------------------------*/
void sthdmirx_signal_monitoring_task(hdmirx_route_handle_t *const pInpHandle);
BOOL sthdmirx_is_hdcp_decryption_notification_required(
  hdmirx_route_handle_t *const pInpHandle);
BOOL sthdmirx_is_signal_change_notification_required(
  hdmirx_route_handle_t *const pInpHandle);

/* Interface procedures/functions ----------------------------------*/
/******************************************************************************
 FUNCTION     :     sthdmirx_reset_signal_process_handler
 USAGE        :     Reset the Signal processing handler, state machines.
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_reset_signal_process_handler(
  hdmirx_route_handle_t *const pInpHandle)
{
  CLEAR_BIT(pInpHandle->HdrxStatus,
            (HDRX_STATUS_LINK_READY | HDRX_STATUS_UNSTABLE |HDRX_STATUS_STABLE));
  SET_BIT(pInpHandle->HdrxStatus, (HDRX_STATUS_NO_SIGNAL));
  pInpHandle->HdrxState = HDRX_FSM_STATE_IDLE;
  pInpHandle->sMeasCtrl.MeasTimer = stm_hdmirx_time_now();

  /*make Rterm value back to 0x8 to make it 50ohm on no signal */
  pInpHandle->pHYControl.RTermValue = pInpHandle->rterm_val;

  /* Clear allsoftwae varaibles, which are related to packet memory acquition & storage */
  sthdmirx_CORE_clear_packet_memory(pInpHandle);

  /* Clear the Packet Noise detected Flag */
  pInpHandle->bIsPacketNoisedetected = FALSE;

  /* Clear HDCP status flag */
  pInpHandle->HDCPStatus = STM_HDMIRX_ROUTE_HDCP_STATUS_NOT_AUTHENTICATED;

  /*Reset the Audio Manager */
  sthdmirx_audio_reset(pInpHandle);

}

/******************************************************************************
 FUNCTION     :     sthdmirx_is_signal_change_notification_required
 USAGE        :     Check the condition to notify the signal chanhe notification.
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
BOOL sthdmirx_is_signal_change_notification_required(
  hdmirx_route_handle_t *const pInpHandle)
{
  BOOL bReturnStatus = FALSE;
  BOOL bCheckAudioStatus = FALSE;

  if (pInpHandle->HwInputSigType == HDRX_INPUT_SIGNAL_DVI)
    {
      bReturnStatus = TRUE;
    }
  else
    {
      if (pInpHandle->stDataAvblFlags.bIsAviInfoAvbl == TRUE)
        {
          bCheckAudioStatus = TRUE;
        }
      else
        {
          if (stm_hdmirx_time_minus(
                stm_hdmirx_time_now(),
                pInpHandle->ulAVIInfoFrameAvblTMO) >(M_NUM_TICKS_PER_MSEC(HDMI_AVI_INFOFRAME_AVBL_CHECK_TIMEOUT)))
            {
              /* fill the properties with the default parameters */
              pInpHandle->stInfoPacketData.eColorDepth = HDRX_GCP_CD_24BPP;
              memcpy((void *)&pInpHandle->stInfoPacketData.stAviInfo, (void *) & VGA_default_InfoFrame, sizeof(HDRX_AVI_INFOFRAME));
              /* Send a video property evt even if no infoframe received, keeping previous values */
              if (NeedVideoEvt == TRUE)
              {
                NeedVideoEvt = FALSE;
                pInpHandle->stNewDataFlags.bIsNewAviInfo = TRUE;
              }
              bCheckAudioStatus = TRUE;
              TRC(TRC_ID_HDMI_RX_SYS,"HDMI Signal without AVI infoFrame..TMO\n");
            }
        }

      if (bCheckAudioStatus == TRUE)
        {
          if ((pInpHandle->stAudioMngr.bIsAudioPktsPresent == FALSE)||
              ((pInpHandle->stAudioMngr.bIsAudioPktsPresent == TRUE)&&
               ((pInpHandle->stAudioMngr.stAudioMngrState == HDRX_AUDIO_MUTE) ||
                (pInpHandle->stAudioMngr.stAudioMngrState == HDRX_AUDIO_OUTPUTTED))))
            {
              bReturnStatus = TRUE;
            }
        }
    }
  return bReturnStatus;
}
/******************************************************************************
 FUNCTION     :     sthdmirx_is_hdcp_decryption_notification_required
 USAGE        :     Check the condition to notify the HDCP decryption notification.
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
BOOL sthdmirx_is_hdcp_decryption_notification_required(
  hdmirx_route_handle_t *const pInpHandle)
{
  stm_hdmirx_route_hdcp_decryption_status_t FrameStatus;
  FrameStatus = sthdmirx_CORE_HDCP_is_encryption_enabled(pInpHandle);
  if ( FrameStatus !=  pInpHandle->FrameStatus)
    {
       pInpHandle->FrameStatus=FrameStatus;
       return TRUE;
    }
  else
    {
       return FALSE;
    }
}

/******************************************************************************
 FUNCTION     :     sthdmirx_is_legal_resolution
 USAGE        :     Check if we get random values from width/length IBD registers
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
BOOL sthdmirx_is_legal_resolution(hdmirx_route_handle_t *pInpHandle)
{
  sthdmirx_IFM_timing_params_t *pIfmParams = &pInpHandle->IfmControl.IFMTimingData;
  U16 Width, Length;
  BOOL check_passed = TRUE;

  sthdmirx_IFM_signal_timing_get((sthdmirx_IFM_context_t *)&pInpHandle->IfmControl);

  Width = pIfmParams->Width & 0xFFFE;
  /* Measured VActive is 3842 instead of 3840 */
  if (Width > 3800) Width &= 0xFFFC;
  Length = pIfmParams->Length & 0xFFFE;

  if (pIfmParams->ScanType == STM_HDMIRX_SIGNAL_SCAN_TYPE_INTERLACED) Length = Length << 1;

  /* Resolutions beyond the system limits are most probably an abnormal measurement */
  if (Width > 4096) check_passed = FALSE;
  if (Length > 2160) check_passed = FALSE;

  if (Length > Width)  check_passed = FALSE;

  /* Check dimensions are 8-multiples */
  if ((Width  % 8 ) != 0) check_passed = FALSE;
  if ((Length % 8) != 0) check_passed = FALSE;

  if (Length == 0)
  {
    check_passed = FALSE;
  }
  else
  {
    unsigned int aspect_ratio;

    aspect_ratio = Width / Length;

    if (aspect_ratio > 3)
    {
      /* When above 3, expecting only integer aspect ratios */
      if ((Width % Length) != 0)
      {
        check_passed = FALSE;
      }
      else
      {
        switch(aspect_ratio)
        {
          case 5:
          case 6:
          case 10:
          case 12:
           check_passed = TRUE;
          break;
          default:
           check_passed = FALSE;
        }
      }
    }
  }

  return check_passed  ;
}

/******************************************************************************
 FUNCTION     :     sthdmirx_signal_monitoring_task
 USAGE        :     Monitors the Hdmi Hardware to get the input signal status,
                    which updates all hardware measurement results
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_signal_monitoring_task(hdmirx_route_handle_t *const pInpHandle)
{
  /* Check the link clock status */
  pInpHandle->sMeasCtrl.LinkClockstatus =sthdmirx_MEAS_is_linkclk_present(pInpHandle);
  if (pInpHandle->sMeasCtrl.LinkClockstatus == TRUE)  	/*if link clock present, go for signal monitor */
    {
      /* check the link clock availability & stability */
      sthdmirx_MEAS_linkclk_meas(pInpHandle);
      /*if link clock is stable, then we have to go for next level signal testing */
      if (pInpHandle->sMeasCtrl.IsLinkClkStable)
        {
          /* set the pll as per input link freq */
          if (pInpHandle->sMeasCtrl.IsPLLSetupDone == FALSE)
            {
              sthdmirx_PHY_PLL_setup(
                &pInpHandle->pHYControl,
                pInpHandle->sMeasCtrl.CurrentTimingInfo.LinkClockKHz, FALSE);
              pInpHandle->sMeasCtrl.IsPLLSetupDone = TRUE;
              return;/* do the adaptive Equalization in next cycle */
            }

          /*Run the Adaptive signal Equalizer */
          sthdmirx_PHY_adaptive_signal_equalizer_handler(pInpHandle);

          /* if Equalization is done, start the measurement. */
          if ((pInpHandle->pHYControl.EqualizationType != SIGNAL_EQUALIZATION_ADAPTIVE) ||
              ((pInpHandle->pHYControl.AdaptiveEqHandle.EqState == EQ_STATE_DONE) &&
               (pInpHandle->pHYControl.EqualizationType == SIGNAL_EQUALIZATION_ADAPTIVE)))
            {

              sthdmirx_MEAS_vertical_timing_meas(pInpHandle);
              sthdmirx_MEAS_horizontal_timing_meas(pInpHandle);
              /*Measurement data is available */
              if (pInpHandle->sMeasCtrl.mStatus &
                  (SIG_STS_HTIMING_MEAS_DATA_AVBL | SIG_STS_VTIMING_MEAS_DATA_AVBL))
                {
                  /* update the measurement timeout */
                  pInpHandle->sMeasCtrl.MeasTimer = stm_hdmirx_time_now();
                  /*check the abnormal sync */
                  if((pInpHandle->sMeasCtrl.CurrentTimingInfo.HTotal <HTOTAL_THRESH) ||
                      (pInpHandle->sMeasCtrl.CurrentTimingInfo.VTotal <VTOTAL_THRESH) ||
                      (pInpHandle->sMeasCtrl.CurrentTimingInfo.LinkClockKHz/1000 <DVI_CLOCK_THRESH))
                    {
                      if(!CHECK_BIT(pInpHandle->sMeasCtrl.mStatus,SIG_STS_ABNORMAL_SYNC_TIMER_STARTED))
                        {
                          SET_BIT(pInpHandle->sMeasCtrl.mStatus,
                                  SIG_STS_ABNORMAL_SYNC_TIMER_STARTED);
                          pInpHandle->sMeasCtrl.ulAbnormalSyncTimer = stm_hdmirx_time_now();
                          TRC(TRC_ID_HDMI_RX_SYS," Abnormal sync Tmr started :0x%x\n",
                           pInpHandle->sMeasCtrl.ulAbnormalSyncTimer);
                        }
                      else if (stm_hdmirx_time_minus(stm_hdmirx_time_now(),
                                                     pInpHandle->sMeasCtrl.ulAbnormalSyncTimer) > (M_NUM_TICKS_PER_MSEC(HDMI_ABNORMAL_SYNC_TRESHOLD)))
                        {
                          TRC(TRC_ID_HDMI_RX_SYS," Abnormal sync Time:0x%x\n",(U32)stm_hdmirx_time_now());
                          SET_BIT(
                            pInpHandle->sMeasCtrl.mStatus,SIG_STS_UNSTABLE_TIMING_PRESENT);
                        }
                    }
                  else
                    {
                      if (CHECK_BIT(
                            pInpHandle->sMeasCtrl.mStatus,SIG_STS_ABNORMAL_SYNC_TIMER_STARTED))
                        {
                          TRC(TRC_ID_HDMI_RX_SYS,"Abnormal sync disappears\n");
                          CLEAR_BIT(pInpHandle->sMeasCtrl.mStatus,
                                    (SIG_STS_UNSTABLE_TIMING_PRESENT |
                                     SIG_STS_ABNORMAL_SYNC_TIMER_STARTED));
                        }
                    }
                  /* check the H, V Stability */
                  if (!CHECK_BIT(pInpHandle->sMeasCtrl.mStatus,
                                 SIG_STS_SYNC_STABILITY_CHECK_STARTED))
                    {
                      sthdmirx_MEAS_initialize_stabality_check_mode(
                        &pInpHandle->sMeasCtrl);
                      SET_BIT(pInpHandle->sMeasCtrl.mStatus,
                              SIG_STS_SYNC_STABILITY_CHECK_STARTED);
                    }
                  else
                    {
                      sthdmirx_MEAS_monitor_input_signal_stability(
                        &pInpHandle->sMeasCtrl);
                    }

                  /*check the double clock mode */
                  sthdmirx_MEAS_check_double_clk_mode(&pInpHandle->sMeasCtrl);
                  /*re-trigger the H&V Timing measurement */
                  CLEAR_BIT(pInpHandle->sMeasCtrl.mStatus,
                            (SIG_STS_HTIMING_MEAS_DATA_AVBL |
                             SIG_STS_VTIMING_MEAS_DATA_AVBL));

                  SET_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_STABLE);
                }
              else if ((stm_hdmirx_time_minus(stm_hdmirx_time_now(),
                                              pInpHandle->sMeasCtrl.MeasTimer)) >(M_NUM_TICKS_PER_MSEC(MEASUREMENT_DONE_PERIOD)))
                {
                  /* check the measurement timeout. */
                  TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx:Timeout...Measurement Data is not available\n");
                  SET_BIT(pInpHandle->sMeasCtrl.mStatus, SIG_STS_UNSTABLE_TIMING_PRESENT);
                }

            }
        }
    }
}

/******************************************************************************
 FUNCTION     :     sthdmirx_main_signal_process_handler
 USAGE        :     This fn is main Hdmi Signal process handler, it monitors all input signal activities.
 INPUT        :     None
 RETURN       :     None
 USED_REGS    :     None
******************************************************************************/
void sthdmirx_main_signal_process_handler(
  hdmirx_route_handle_t *const pInpHandle)
{

  static unsigned int stability_cnt;

  /*Is Signal Monitoring is needed */
  if (pInpHandle->bIsSignalDetectionStarted == FALSE)
    {
      if (pInpHandle->HdrxState != HDRX_FSM_STATE_IDLE)
        {
          if (pInpHandle->HdrxState == HDRX_FSM_STATE_SIGNAL_STABLE)
            {
              pInpHandle->HdrxState = HDRX_FSM_STATE_TRANSITION_TO_NOSIGNAL;
            }
        }
      return;
    }
  sthdmirx_signal_monitoring_task(pInpHandle);

  /* Check the Signal Transition */
  if (pInpHandle->sMeasCtrl.LinkClockstatus == TRUE)
    {
      if (CHECK_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_NO_SIGNAL))
        {
          if (hdmirx_get_src_plug_status((hdmirx_port_handle_t *)pInpHandle->PortHandle)==STM_HDMIRX_PORT_SOURCE_PLUG_STATUS_IN)
            {
              /* Transistion to NOSIGNAL TO SIGNAL */
              CLEAR_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_NO_SIGNAL);
              pInpHandle->HdrxState = HDRX_FSM_STATE_TRANSITION_TO_SIGNAL;
            }
          else
            {
              sthdmirx_CORE_enable_reset(pInpHandle, TRUE);
            }
          goto STATE_MACHINE;
        }
    }
  else
    {
      if (!CHECK_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_NO_SIGNAL))
        {
          /* Transistion to SIGNAL TO NO_SIGNAL */
          SET_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_NO_SIGNAL);
          pInpHandle->HdrxState = HDRX_FSM_STATE_TRANSITION_TO_NOSIGNAL;
          goto STATE_MACHINE;
        }
      /* if no signal state, remains in the idle state or i will decide one more state, NO_SIGNAL */
      if (CHECK_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_LINK_INIT))
        {
          CLEAR_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_LINK_INIT);
        }
      goto STATE_MACHINE;
    }

  /*check the clock measurement */
  if (pInpHandle->sMeasCtrl.IsLinkClkAvailable == FALSE)
    {
      TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx:Link clock measurement not completed yet\n");
      pInpHandle->HdrxState = HDRX_FSM_STATE_IDLE;
      pInpHandle->sMeasCtrl.MeasTimer = stm_hdmirx_time_now();
      goto STATE_MACHINE;
    }
  else if (pInpHandle->sMeasCtrl.IsLinkClkStable == FALSE)
    {
      TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx:Link Clock becomes Unstable\n");
      if (pInpHandle->HdrxState != HDRX_FSM_STATE_IDLE)
        {
          pInpHandle->HdrxState = HDRX_FSM_STATE_UNSTABLE_TIMING;
          goto STATE_MACHINE;
        }
    }
  else if (pInpHandle->sMeasCtrl.IsLinkClkStable == TRUE)
    {
      /*check the condition to start Equalization Process */
      if (pInpHandle->HdrxState == HDRX_FSM_STATE_IDLE)
        {
          stm_hdmirx_sema_wait(pInpHandle->hDevice->hdmirx_pm_sema);
          if (! pInpHandle->hDevice->is_standby)
          {
            pInpHandle->HdrxState = HDRX_FSM_STATE_START;
            sthdmirx_CORE_select_PHY_source(pInpHandle, 0x10, TRUE);	//Select Combophy
            TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx: SM started\n");
          }
          stm_hdmirx_sema_signal(pInpHandle->hDevice->hdmirx_pm_sema);
        }
    }

  /*Need to check the unstability flags, if signal become unstable, move to unstable state directly */
  if (CHECK_BIT(
        pInpHandle->sMeasCtrl.mStatus, SIG_STS_UNSTABLE_TIMING_PRESENT))
    {
      TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx:LClk becomes Unstable..Measurement declares\n");
      pInpHandle->HdrxState = HDRX_FSM_STATE_UNSTABLE_TIMING;
      goto STATE_MACHINE;
    }

  /* State Machine starts.   */
STATE_MACHINE:
  switch (pInpHandle->HdrxState)
    {

    case HDRX_FSM_STATE_IDLE:
      /* doing nothing, makes everything ready. free running mode. */
      break;

    case HDRX_FSM_STATE_UNSTABLE_TIMING:
      TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx: Tr2Unstable\n");
      sthdmirx_CORE_reset(pInpHandle,
                          (RESET_CORE_LINK_CLOCK_DOMAIN | RESET_CORE_PACKET_PROCESSOR |
                           RESET_CORE_VIDEO_LOGIC | RESET_CORE_AUDIO_OUTPUT));
      /*make fall throug.. don't edit the states */
    case HDRX_FSM_STATE_AFR_AFB_TRIGGERED:
      /*Send the no signal notification when AFB/AFR Triggered */
    case HDRX_FSM_STATE_TRANSITION_TO_NOSIGNAL:
      sthdmirx_CORE_HDCP_Reset_Hdmi_Mode(pInpHandle,TRUE);
      /*Reset all hardware, measurement logic,state machine */
      TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx: Tr2NoSignal\n");
      /* if previous PLL mode is x10, if  225 Mhz Link clock inputed after clock loss, so PLL will be out of operating range i.e 225x10 MHz so keep in x2.5 mode */
      sthdmirx_PHY_PLL_setup(&pInpHandle->pHYControl, 0, TRUE);
      sthdmirx_reset_signal_process_handler(pInpHandle);
      sthdmirx_MEAS_reset_measurement(&pInpHandle->sMeasCtrl);
      sthdmirx_CORE_HDCP_Authentication_Clear(pInpHandle);
#ifdef STHDMIRX_CLOCK_GEN_ENABLE
      sthdmirx_clkgen_DDS_openloop_force(pInpHandle->stDdsConfigInfo.estVidDds,
                                         VDDS_DEFAULT_INIT_FREQ, pInpHandle->MappedClkGenAddress);
      sthdmirx_clkgen_DDS_openloop_force(pInpHandle->stDdsConfigInfo.estAudDds,
                                         ADDS_DEFAULT_INIT_FREQ,pInpHandle->MappedClkGenAddress);
#endif
      if (pInpHandle->bIsNoSignalNotified == FALSE)
        {
          pInpHandle->bIsNoSignalNotify = TRUE;
          pInpHandle->bIsNoSignalNotified = TRUE;
        }
      /*Reset Australian mode detection to allow for other modes */
      if (overlapFlag &&
          (!(pInpHandle->HwInputSigType == HDRX_INPUT_SIGNAL_DVI)))
        {
          overlapFlag = FALSE;
          sthdmirx_IFM_reset_australianmode_interlace_detect((sthdmirx_IFM_context_t *) & pInpHandle->IfmControl);
          /*sthdmirx_video_enable_regen_sync(pInpHandle);*/
          TRC(TRC_ID_HDMI_RX_SYS,"\n  Australian modes disabled .......................... !!!!!!!! \n");
        }
      break;

    case HDRX_FSM_STATE_TRANSITION_TO_SIGNAL:
      TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx:Tr2Signal\n");
      /*reset all measurement logics. */
      sthdmirx_MEAS_reset_measurement(&pInpHandle->sMeasCtrl);
      /*reset core link */

      /* if previous PLL mode is x10, if  225 Mhz Link clock inputed after clock loss, so PLL will be out of operating range i.e 225x10 MHz so keep in x2.5 mode */
      pInpHandle->HdrxState = HDRX_FSM_STATE_IDLE;
      break;

    case HDRX_FSM_STATE_START:
      pInpHandle->bIsSigPresentNotified = FALSE;
      /*prepare for Equalization & Set for Abnormalities & double clock mode checks. */
      if (sthdmirx_CORE_HDCP_Authentication_Detection(pInpHandle) == TRUE)
        {
          pInpHandle->bIsHDCPAuthenticationDetected = TRUE;
        }
      if ( sthdmirx_is_hdcp_decryption_notification_required(pInpHandle)==TRUE)
        {
          pInpHandle->bIsHDCPFrameDecryptionNotify = TRUE;
        }

      if (pInpHandle->pHYControl.EqualizationType == SIGNAL_EQUALIZATION_ADAPTIVE)
        {
          pInpHandle->pHYControl.RTermValue = 0xC;
          pInpHandle->HdrxState = HDRX_FSM_STATE_EQUALIZATION_UNDER_PROGRESS;
        }
      else
        {
          pInpHandle->HdrxState = HDRX_FSM_STATE_VERIFY_LINK_HEALTH;
        }

      break;

    case HDRX_FSM_STATE_EQUALIZATION_UNDER_PROGRESS:
      if (sthdmirx_CORE_HDCP_Authentication_Detection(pInpHandle) == TRUE)
        {
          pInpHandle->bIsHDCPAuthenticationDetected = TRUE;
        }
      if ( sthdmirx_is_hdcp_decryption_notification_required(pInpHandle)==TRUE)
        {
          pInpHandle->bIsHDCPFrameDecryptionNotify = TRUE;
        }
      /* checks the Eq state machine, if completed, then Move to stable otherwise take actions as per states */
      if (pInpHandle->pHYControl.AdaptiveEqHandle.EqState == EQ_STATE_DONE)
        {
          pInpHandle->HdrxState = HDRX_FSM_STATE_VERIFY_LINK_HEALTH;
          TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx: Eq done!!\n");
        }
      else if (pInpHandle->pHYControl.AdaptiveEqHandle.EqState == EQ_STATE_FAIL)
        {
          /* Take some hardware action if needed */
          TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx: unstable due to Equalizer...\n");
          pInpHandle->HdrxState = HDRX_FSM_STATE_UNSTABLE_TIMING;
        }
      else
        {
          TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx: Adaptive Equalizer...\n");
        }
      break;

    case HDRX_FSM_STATE_VERIFY_LINK_HEALTH:	//transition to
      if (sthdmirx_CORE_HDCP_Authentication_Detection(pInpHandle) == TRUE)
        {
          pInpHandle->bIsHDCPAuthenticationDetected = TRUE;
        }
      if ( sthdmirx_is_hdcp_decryption_notification_required(pInpHandle)==TRUE)
        {
          pInpHandle->bIsHDCPFrameDecryptionNotify = TRUE;
        }
      /* check sync abnormality , if found ok, declare as stable link */
      if(!CHECK_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_STABLE))
        {
          pInpHandle->HdrxState = HDRX_FSM_STATE_VERIFY_LINK_HEALTH;
          break;
        }
      pInpHandle->HdrxState = HDRX_FSM_STATE_SIGNAL_STABLE;
      stability_cnt = 0;
      NeedVideoEvt = TRUE;
#ifdef STHDMIRX_CLOCK_GEN_ENABLE
      sthdmirx_clkgen_DDS_closeloop_force(pInpHandle->stDdsConfigInfo.estVidDds,
                                          (pInpHandle->sMeasCtrl.CurrentTimingInfo.LinkClockKHz * 1000),
                                          pInpHandle->MappedClkGenAddress);
#endif
      sthdmirx_CORE_select_pixel_clk_out_domain(pInpHandle);

      /* Prints the detected Timing Data */
      sthdmirx_MEAS_print_mode(&pInpHandle->sMeasCtrl.CurrentTimingInfo);
      sthdmirx_IFM_clear_interlace_decode_error_status(&pInpHandle->IfmControl);
      pInpHandle->ulAVIInfoFrameAvblTMO = stm_hdmirx_time_now();
      if (sthdmirx_IFM_is_mode_overlapping ((sthdmirx_IFM_context_t *) &
                                     pInpHandle->IfmControl) == TRUE &&
          (!(pInpHandle->HwInputSigType == HDRX_INPUT_SIGNAL_DVI)))
        {
          overlapFlag = TRUE;
          sthdmirx_IFM_set_australianmode_interlace_detect((sthdmirx_IFM_context_t *) & pInpHandle->IfmControl);
          TRC(TRC_ID_HDMI_RX_SYS,"\n Australian modes found .......................... !!!!!!!! \n");
        }
      break;

    case HDRX_FSM_STATE_SIGNAL_STABLE:
      if (CHECK_BIT(pInpHandle->HdrxStatus, HDRX_STATUS_STABLE))
        {
          sthdmirx_CORE_ISR_SDP_client(pInpHandle);
          sthdmirx_CORE_ISR_SDP_audio_client(pInpHandle);
          /* Clears the AFR detect status */
          if (pInpHandle->bIsSigPresentNotified == FALSE)
            {
              sthdmirx_IFM_clear_AFRsignal_detect_status(&pInpHandle->IfmControl);
              stm_hdmirx_delay_ms(40);
            }
          if (
            (pInpHandle->HdmiRxMode == STM_HDMIRX_ROUTE_OP_MODE_DVI) ||
            (sthdmirx_CORE_is_HDMI_signal(pInpHandle) == FALSE))
            {
              sthdmirx_CORE_DVI_signal_process_handler
              (pInpHandle);
            }

          if ((pInpHandle->HdmiRxMode != STM_HDMIRX_ROUTE_OP_MODE_DVI) &&
              (sthdmirx_CORE_is_HDMI_signal(pInpHandle) == TRUE))
            {
              sthdmirx_CORE_HDMI_signal_process_handler(pInpHandle);
            }
          sthdmirx_audio_manager(pInpHandle);
          if ((sthdmirx_is_signal_change_notification_required(pInpHandle) == TRUE) &&
              (sthdmirx_IFM_get_AFRsignal_detect_status(&pInpHandle->IfmControl) == TRUE))
            {
              TRC(TRC_ID_HDMI_RX_SYS,"HdmiRx: Trigger AFB!!\n");
              pInpHandle->HdrxState = HDRX_FSM_STATE_AFR_AFB_TRIGGERED;
              if(!InFsTransition)
                {
                  InFsTransition = TRUE;
                  sthdmirx_audio_soft_mute(pInpHandle, TRUE);
                  pInpHandle->bIsAudioOutPutStarted = FALSE;
                  pInpHandle->bIsAudioPropertyChanged = TRUE;
                }
              break;
            }

          /* first time signal notification, when Audio/video is completely ready */
          if ((pInpHandle->bIsSigPresentNotified == FALSE) &&
              (sthdmirx_is_signal_change_notification_required(pInpHandle) == TRUE))
            {
              pInpHandle->IfmControl.IFMTimingData.LineClk_Hz = pInpHandle->sMeasCtrl.CurrentTimingInfo.LinkClockKHz * 1000;

              /* Allow signal present once IBD registers contents are stable */
              if (sthdmirx_is_legal_resolution(pInpHandle) == TRUE)
              {
                stability_cnt ++;
              }
              else
              {
                stability_cnt = 0;
              }

              if (stability_cnt > 1)
              {
                pInpHandle->bIsNoSignalNotified = FALSE;
                pInpHandle->bIsSigPresentNotified = TRUE;
                pInpHandle->bIsSignalPresentNotify = TRUE;
              }
            }

          if (sthdmirx_CORE_HDCP_Authentication_Detection(pInpHandle) == TRUE)
            {
              pInpHandle->bIsHDCPAuthenticationDetected = TRUE;
            }
          if ( sthdmirx_is_hdcp_decryption_notification_required(pInpHandle)==TRUE)
            {
              pInpHandle->bIsHDCPFrameDecryptionNotify = TRUE;
            }

          if ((pInpHandle->bIsSignalPresentNotify == FALSE) &&
              (pInpHandle->bIsSigPresentNotified == TRUE))
            {
              pInpHandle->HDCPStatus = sthdmirx_CORE_HDCP_status_get(pInpHandle);
            }
          TRC(TRC_ID_HDMI_RX_SYS,"HDRX_FSM_STATE_SIGNAL_STABLE\n");
        }
      break;

    }
  return;

}
