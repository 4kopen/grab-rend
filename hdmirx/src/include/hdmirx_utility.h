/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __HDMIRX_UTILITY_H__
#define __HDMIRX_UTILITY_H__

/*Includes------------------------------------------------------------------------------*/

/* Support dual Interface C & C++*/
#ifdef __cplusplus
extern "C" {
#endif

  /* Private Types ---------------------------------------------------------- --------------*/


  /* Private Constants -------------------------------------------------------------------- */

  /* Private variables (static) ----------------------------------------------------------- */

  /* Global Variables --------------------------------------------------------------------- */

  /* Private Macros ----------------------------------------------------------------------- */

#define     MAKE_WORD(LSB, MSB)             ((LSB) | ((U16)(MSB) << 8))
#define     MAKE_DWORD(LSB, B1, B2, MSB)    ((U32)(MAKE_WORD(LSB, B1)) | \
                                        ((U32)(MAKE_WORD(B2, MSB)) << 16))

#define     SET_BIT(a, bit)         (a) |= (bit)
#define     CLEAR_BIT(a, bit)       (a) &= ~(bit)
#define     CHECK_BIT(a, bit)       (((a) & (bit)) == (bit))

#define     HDMI_MIN(a, b)          (((a) < (b)) ? (a) : (b))
#define     HDMI_MAX(a, b)          (((a) > (b)) ? (a) : (b))
  /* Exported Macros--------------------------------------------------------- --------------*/

  /* Exported Functions ----------------------------------------------------- ---------------*/

  /* ------------------------------- End of file ---------------------------------------------------------------------------- */
#ifdef __cplusplus
}
#endif

#endif /*end of __HDMIRX_ANALOGPHY_H__*/
