/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifdef STHDMIRX_INTERNAL_EDID_SUPPORT

/* Standard Includes ----------------------------------------------*/
#include <linux/kernel.h>
/* Include of Other module interface headers ---------------------------*/
/* Local Includes -------------------------------------------------*/
#include <hdmirx_csm.h>
#include <hdmirx_RegOffsets.h>
#include <hdmirx_i2cslave.h>
#include <InternalTypes.h>
#include <linux/power/st_lpm.h>
#include <hdmirx_drv.h>

/* Private Typedef -----------------------------------------------*/

/* Private Defines ------------------------------------------------*/

#define EDID_BLOCK_SIZE    128
#define FIFO_SIZE    16

/* Private macro's ------------------------------------------------*/
#define  GET_ADDRS(BaseAddress, RegOffSet, PortId)    (BaseAddress + I2C_SLAVE_MODULE_BASE_ADDRS + (PortId * I2C_SLV_REG_SIZE) + RegOffSet)

/* Private Variables -----------------------------------------------*/
static volatile bool init_Done=false;

/* Private functions prototypes ------------------------------------*/

/* Interface procedures & functions ----------------------------------*/

/******************************************************************************
 FUNCTION        : sthdmirx_I2C_slave_init()
 USAGE           : Initializes the slaves connected to the Internal EDID(s)
 INPUT           : I2C Slave Handle, I2C Slave initialization parameters
 RETURN          :
 USED_REGS       :
******************************************************************************/
void sthdmirx_I2C_slave_init(sthdmirx_I2C_slave_control_t *I2CSlaveCtrl,
                             sthdmirx_I2C_slave_init_params_t *slInit)
{
  U32 uCtrlWord = 0x00000000;
  U32 PortNum = slInit->PortNo;
  BOOL RegStatus;

  TRC(TRC_ID_HDMI_RX,"I2C slave init");

  I2CSlaveCtrl->ulCellBaseAddr = slInit->RegBaseAddrs;

  if (slInit->bIsClkStretchNeeded)
    {
      uCtrlWord |= I2C_SLV1_CLOCK_STRETCH_EN;
    }
  if (!(slInit->bIsFIFOModeNeeded)) /* FIFO enabled for both read and write operations */
    {
      uCtrlWord |= (I2C_SLV1_NO_READ_FIFO_MODE | I2C_SLV1_NO_WRITE_FIFO_MODE);
    }

    /*  Disable all interrupts for now */
  HDMI_WRITE_REG_DWORD(
    GET_ADDRS(slInit->RegBaseAddrs, I2C_SLV1_IRQ_CTRL, PortNum), 0);

  HDMI_WRITE_REG_DWORD(
    GET_ADDRS(slInit->RegBaseAddrs, I2C_SLV1_CTRL, PortNum), (uCtrlWord | I2C_SLV1_EN));


  RegStatus = sthdmirx_I2C_register_dev_id(I2CSlaveCtrl,slInit->DevAddress, PortNum);
  if (RegStatus == TRUE)
    {
      I2CSlaveCtrl->uDevAddress[0] = slInit->DevAddress[0];
      I2CSlaveCtrl->uDevAddress[1] = slInit->DevAddress[1];
    }

  I2CSlaveCtrl->PortNo = slInit->PortNo;
  I2CSlaveCtrl->Received = I2C_SLAVE_RCV_NONE;
  I2CSlaveCtrl->puBuffer = NULL;
  I2CSlaveCtrl->seg_ptr = 0;
  I2CSlaveCtrl->dbg_cnt = 0;

  /*  Here SBC code would make SCL/SDA PIO configuration, already done in host */

  HDMI_SET_REG_BITS_DWORD(GET_ADDRS(slInit->RegBaseAddrs, I2C_SLV1_STS_CLR, PortNum),
    I2C_SLV1_READ_FIFO_RESET | I2C_SLV1_WRITE_FIFO_RESET);

  HDMI_WRITE_REG_DWORD(
    GET_ADDRS(slInit->RegBaseAddrs, I2C_SLV1_IRQ_STATUS, PortNum), 0xFFF);


  /*  Enable interrupts from slave */
  HDMI_WRITE_REG_DWORD(
    GET_ADDRS(slInit->RegBaseAddrs, I2C_SLV1_IRQ_CTRL, PortNum),
           I2C_SLV1_START_IRQ_EN |
           I2C_SLV1_STOP_IRQ_EN |
           I2C_SLV1_DEV_IRQ_EN |
           I2C_SLV1_RFIFO_nE_IRQ_EN |
           I2C_SLV1_IRQ_EN);

  /* Here SBC code would toggle HPD: done in hdmirx initialization */

  TRC(TRC_ID_HDMI_RX,"I2C Slave device initialized for Port no. %d",
                         I2CSlaveCtrl->PortNo);

  init_Done=true;

  return;
}

/******************************************************************************
 FUNCTION        : sthdmirx_I2C_slave_close()
 USAGE           : frees resources of slave
 INPUT           : I2C Slave ctrl pointer
 RETURN          :
 USED_REGS       :
******************************************************************************/
void sthdmirx_I2C_slave_close(sthdmirx_I2C_slave_control_t *I2CSlaveCtrl)
{
  HDMI_WRITE_REG_DWORD(
    GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr, I2C_SLV1_CTRL, I2CSlaveCtrl->PortNo), 0);

  HDMI_WRITE_REG_DWORD(
    GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr, I2C_SLV1_IRQ_CTRL, I2CSlaveCtrl->PortNo), 0);

  sthdmirx_I2C_unregister_dev_id(I2CSlaveCtrl);

  if(I2CSlaveCtrl->puBuffer)
  {
    stm_hdmirx_free(I2CSlaveCtrl->puBuffer);
    I2CSlaveCtrl->puBuffer = 0;
  }

  I2CSlaveCtrl->uDevAddress[0] = 0;
  I2CSlaveCtrl->uDevAddress[1] = 0;

  I2CSlaveCtrl->Received = I2C_SLAVE_RCV_NONE;

}

/******************************************************************************
 FUNCTION        : sthdmirx_I2C_slave_update()
 USAGE           : synchronize EDID block contents
 INPUT           : I2C Slave ctrl pointer
 RETURN          :
 USED_REGS       :
******************************************************************************/
void sthdmirx_I2C_slave_update(sthdmirx_I2C_slave_control_t *I2CSlaveCtrl,
                               stm_hdmirx_port_edid_block_t *edid_block,
                               uint8_t block_number)
{
  /* If buffer not present, allocate it */
  if (I2CSlaveCtrl->puBuffer == NULL)
    {
      I2CSlaveCtrl->puBuffer = (u8 *) stm_hdmirx_malloc(EDID_BLOCK_SIZE << 2);
      memset(I2CSlaveCtrl->puBuffer, 0, EDID_BLOCK_SIZE << 2);
    }

  memcpy(& (I2CSlaveCtrl->puBuffer[EDID_BLOCK_SIZE * block_number]),
            edid_block,
            EDID_BLOCK_SIZE);

  /* Note down that block is present in buffer */
  I2CSlaveCtrl->BlocksInBuf |= 1 << block_number;

  TRC(TRC_ID_HDMI_RX,"I2C slave Update block %d", block_number);

}

/******************************************************************************
 FUNCTION        : sthdmirx_I2C_slave_interrupt_routine
 USAGE           : handles CSM I2C interrupt (also used by CEC, HDCP)
 INPUT           : irq number, device handle
 RETURN          : IRQ_NONE ou IRQ_HANDLED
 USED_REGS       :
******************************************************************************/
irqreturn_t sthdmirx_I2C_slave_interrupt_routine(int irq, void *dev)
{
  U32 PortNum, I2CSlaveIRQStatus = 0, CsmIRQStatus;
  U16 ulLength;
  U8 DeviceAddrs;
  BOOL OperStatus;
  irqreturn_t  irq_result = IRQ_NONE;
  sthdmirx_I2C_slave_control_t *I2CSlaveCtrl;
  hdmirx_dev_handle_t *dHandle_p = (hdmirx_dev_handle_t *) dev;


  if (init_Done == false)
  {
    return IRQ_NONE;
  }

  TRC(TRC_ID_HDMI_RX,"I2C slave ISR ");

  /* Ckeck all ports status */
  for (PortNum = 0; PortNum < dHandle_p->DeviceProp.Max_Ports; PortNum ++)
   {
     I2CSlaveCtrl= &(dHandle_p->PortHandle[PortNum].stI2CSlaveCtrl);

     /* If interrupt created by this port, it must be handled here */
     CsmIRQStatus = HDMI_READ_REG_DWORD(dHandle_p->CsmCellBaseAddress + I2C_CSM_IRQ_STATUS);
     if (CsmIRQStatus & (1 << (PortNum + 1))) irq_result = IRQ_HANDLED;

     /* If i2c slave IT inhibited, skip processing (IT must be from cec...) */
     if ((HDMI_READ_REG_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum)) & I2C_SLV1_IRQ_EN) == 0) continue;

     I2CSlaveIRQStatus = HDMI_READ_REG_DWORD(
                              GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_STATUS, PortNum));

     if (I2CSlaveIRQStatus & I2C_SLV1_STOP)
       {
         TRC(TRC_ID_HDMI_RX,"I2C slave STOP");

         HDMI_SET_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_STS_CLR, PortNum),
                         I2C_SLV1_WRITE_FIFO_RESET);

         HDMI_SET_REG_BITS_DWORD(
                GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_STATUS, PortNum),
                 I2C_SLV1_STOP);

         /* In case master does not give offset for next transfer */
         I2CSlaveCtrl->offset += I2CSlaveCtrl->TotalBytes;

         /* Allow offset change*/
         I2CSlaveCtrl->Received &= ~I2C_SLAVE_RCV_WRITE;

         sthdmirx_I2C_slave_reset(PortNum, I2CSlaveCtrl);

         I2CSlaveCtrl->dbg_cnt = 0;

       }


     /* Just clear start interrupt if present, processing starts on device id
      * received */
     if (I2CSlaveIRQStatus & I2C_SLV1_START)
       {
         TRC(TRC_ID_HDMI_RX,"I2C slave START");

         HDMI_SET_REG_BITS_DWORD(
                    GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_STATUS, PortNum),
                    I2C_SLV1_START);

         /* OHO for test */
         if (I2CSlaveCtrl->Received & I2C_SLAVE_RCV_SENDING)
           {
             HDMI_SET_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_STS_CLR, PortNum),
                         I2C_SLV1_WRITE_FIFO_RESET);
             sthdmirx_I2C_slave_reset(PortNum, I2CSlaveCtrl);
             TRC(TRC_ID_HDMI_RX,"START received while sending");
           }


         I2CSlaveCtrl->Received |= I2C_SLAVE_RCV_START;
       }

     if (I2CSlaveIRQStatus & I2C_SLV1_DEV_ID_RECEIVED)        {
         TRC(TRC_ID_HDMI_RX,"I2C slave DEV ID received");

         HDMI_SET_REG_BITS_DWORD(
                    GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_STATUS, PortNum),
                    I2C_SLV1_DEV_ID_RECEIVED);

         I2CSlaveCtrl->Received |= I2C_SLAVE_RCV_DEVID;


         DeviceAddrs = (U8) (HDMI_READ_REG_DWORD(
                        GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_RX_DEV_ID, PortNum)) & I2C_SLV1_DATA_MASK);

         /*  For 4 blocks edid we need to manage two Dev Ids */
         if (I2CSlaveCtrl->uDevAddress[1] == (DeviceAddrs & I2C_SLAVE_DEVICEID_MASK))
           {
             I2CSlaveCtrl->Received |= I2C_SLAVE_RCV_SEGPTR;
           }
         else
           {
             if (I2CSlaveCtrl->uDevAddress[0] != (DeviceAddrs & I2C_SLAVE_DEVICEID_MASK))
               {
                 I2CSlaveCtrl->eI2CSlaveState = I2C_SLAVE_STATE_DATA_ERROR;
                 TRC(TRC_ID_HDMI_RX,"Invalid Device Address  - %x",
                     DeviceAddrs);
                 return irq_result;
               }
           }

         /* Determine now if the operation is a READ from or a WRITE to slave */
         if (DeviceAddrs & I2C_SLAVE_ADDR_READ_MASK)
           {
             TRC(TRC_ID_HDMI_RX,"I2C slave READ FROM");

             I2CSlaveCtrl->Received |= I2C_SLAVE_RCV_READ;


             HDMI_SET_REG_BITS_DWORD(
                    GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum),
                    I2C_SLV1_WRITE_FIFO_AE|I2C_SLV1_WRITE_FIFO_E);


             I2CSlaveCtrl->TotalBytes = 0;
           }/* read operation */
         else
           {
             TRC(TRC_ID_HDMI_RX,"I2C slave WRITE TO ");

             /* Operation is a Write to Slave */
              /* Wait for read_fifo_nE  */
           } /*  write operation */
       }/* device_id received */


     /* Get what master has sent */
     if ((I2CSlaveIRQStatus & I2C_SLV1_READ_FIFO_nE))
       {
         TRC(TRC_ID_HDMI_RX,"I2C slave READ F nE ");

         if ((I2CSlaveCtrl->Received & I2C_SLAVE_RCV_WRITE) == 0)
           {
             HDMI_SET_REG_BITS_DWORD(
                GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_STATUS, PortNum),
                I2C_SLV1_READ_FIFO_nE);

             if (I2CSlaveCtrl->Received & I2C_SLAVE_RCV_SEGPTR)
               {
                 OperStatus = sthdmirx_I2C_write_to_slaveFIFO(I2CSlaveCtrl,
                           &(I2CSlaveCtrl->seg_ptr), & ulLength, PortNum);

                 TRC(TRC_ID_HDMI_RX,"I2C slave PortNum %d, seg_ptr %d ",PortNum,  I2CSlaveCtrl->seg_ptr);

                 I2CSlaveCtrl->Received &= ~I2C_SLAVE_RCV_SEGPTR;
               }
              else
               {
                 U16 prevoffset;

                 /* Do not allow offset change until current has been used */
                 HDMI_CLEAR_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum), I2C_SLV1_RFIFO_nE_IRQ_EN);

                 /* Get EDID  block id sent by master */
                 prevoffset = I2CSlaveCtrl->offset;
                 I2CSlaveCtrl->offset = 0;
                 OperStatus = sthdmirx_I2C_write_to_slaveFIFO(I2CSlaveCtrl,
                           &(I2CSlaveCtrl->offset), & ulLength, PortNum);

                 if (!(I2CSlaveCtrl->offset > 0))
                 {
                   if (I2CSlaveCtrl->seg_ptr == 0)
                     {
                       /* Beginning of EDID readout, clear leftover bytes from previous operations */
                       HDMI_SET_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_STS_CLR, PortNum),
                                                 I2C_SLV1_WRITE_FIFO_RESET);
                     }
                   }

                 /* QD gives an offset, not a block id */
                 TRC(TRC_ID_HDMI_RX,"I2C slave PortNum %d, src offset %d",PortNum, I2CSlaveCtrl->offset);

                 I2CSlaveCtrl->Received |= I2C_SLAVE_RCV_WRITE;

                 /* Next step of transaction is read from slave */
                 I2CSlaveCtrl->uTransactionDir = I2C_SLAVE_DIR_READ;
               }
           }
         /* End of write to slave operation */
       }

     /* Start or continue emission of data towards master */
     if ((I2CSlaveIRQStatus & (I2C_SLV1_WRITE_FIFO_AE | I2C_SLV1_WRITE_FIFO_E)) &&
         ((I2CSlaveCtrl->Received == I2C_SLAVE_RCV_READY_TO_SEND) || (I2CSlaveCtrl->Received & I2C_SLAVE_RCV_SENDING)))
       {
         TRC(TRC_ID_HDMI_RX,"I2C slave WRITE F AE");


         if (I2CSlaveCtrl->TotalBytes == 0)
           {
             TRC(TRC_ID_HDMI_RX,"TotalBytes = 0");

             /* Free lower bits except write for next transaction */
             I2CSlaveCtrl->Received = I2C_SLAVE_RCV_SENDING | I2C_SLAVE_RCV_WRITE;

             HDMI_CLEAR_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_STS_CLR, PortNum),
                                   I2C_SLV1_WRITE_FIFO_RESET);


             HDMI_SET_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum),
             I2C_SLV1_WFIFO_AE_IRQ_EN | I2C_SLV1_WFIFO_E_IRQ_EN);

             /*  Let bottom half read Edid through lpm interface and send first Fifo, outside of
              *  interrupt context. TBC if dedicated thread needed like in CEC, using default
              *  "events" */
           }
           sthdmirx_I2C_read_from_slaveFIFO(PortNum, I2CSlaveCtrl);
       } /*  FIFO AE */
   }/*  for .. PortNum */

   return irq_result;
}

/******************************************************************************
 FUNCTION      : sthdmirx_I2C_read_from_slaveFIFO()
 USAGE           : Slave Read as per Master from the Write FIFO
 INPUT           : Buffer to Read data into, Length of the data
 RETURN          : Status of the Slave Read Operation
 USED_REGS       :
******************************************************************************/
U8 sthdmirx_I2C_read_from_slaveFIFO(
                U32 PortNum,
                sthdmirx_I2C_slave_control_t *I2CSlaveCtrl)
{
  S32  sWriteFIFOLength = 0, i;
  U8  *pulocbuf;
  U8  block_id, sBytesFilled = 0;
  U32 edid_size, curpos;
  S32 sEmptySpace;

  /* if offset belongs to a block that has not been filled yet, exit */
  block_id = (I2CSlaveCtrl->offset / EDID_BLOCK_SIZE) + (2 * I2CSlaveCtrl->seg_ptr);

/* if (!(I2CSlaveCtrl->BlocksInBuf & (1 << block_id)))  */
   if( block_id > 3)
    {
      /* disable further AE/E interrupts */
      HDMI_CLEAR_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum),
             I2C_SLV1_WFIFO_AE_IRQ_EN | I2C_SLV1_WFIFO_E_IRQ_EN);

      TRC(TRC_ID_HDMI_RX,"Block %d not available from edid buffer", block_id);
      return sBytesFilled;
    }

  /* oHO for tests */
  edid_size = EDID_BLOCK_SIZE << 2;

  curpos = I2CSlaveCtrl->offset + I2CSlaveCtrl->TotalBytes + (I2CSlaveCtrl->seg_ptr * EDID_BLOCK_SIZE * 2);

  pulocbuf = I2CSlaveCtrl->puBuffer;
  pulocbuf += curpos;

  sWriteFIFOLength =
    HDMI_READ_REG_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_OUT_FIFO_CNTR, PortNum));
  sEmptySpace = FIFO_SIZE - sWriteFIFOLength;

  if (curpos > edid_size)
  {
    sEmptySpace = 0;
  }
  else
  {
    /* Just or not enough bytes left to fill the FIFO */
    if (sEmptySpace >= (edid_size - curpos))
    {
      sEmptySpace = edid_size - curpos;

      /* disable further AE/E interrupts */
      HDMI_CLEAR_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum),
           I2C_SLV1_WFIFO_AE_IRQ_EN | I2C_SLV1_WFIFO_E_IRQ_EN);

      I2CSlaveCtrl->Received &= ~I2C_SLAVE_RCV_SENDING;

      TRC(TRC_ID_HDMI_RX,"Sending last bytes to master");
    }
  }
  TRC(TRC_ID_HDMI_RX,"curpos %d", curpos);

  if (sEmptySpace != 0)
  {
    for (i = 0; i < sEmptySpace; i++)
    {
      if (I2CSlaveCtrl->puBuffer != NULL)
      {
         HDMI_WRITE_REG_DWORD(
             GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_DATA, PortNum), *pulocbuf++);
      }
      else
      {
         HDMI_WRITE_REG_DWORD(
             GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_DATA, PortNum), '\0');
      }
      sBytesFilled ++;
    }
  }

  I2CSlaveCtrl->TotalBytes += sBytesFilled;

  return sBytesFilled;
}

/******************************************************************************
 FUNCTION      : sthdmirx_I2C_write_to_slaveFIFO()
 USAGE         : Slave Write as per Master to the Read FIFO
 INPUT         : Buffer to Write data into, Length of the data
 RETURN        : Status of the Slave Write Operation
 USED_REGS     :
******************************************************************************/
BOOL sthdmirx_I2C_write_to_slaveFIFO(sthdmirx_I2C_slave_control_t *I2CSlaveCtrl, U8 *puBuffer, U16 *ulLength, U32 PortNum)
{
  U8 ubBytes;

  /* Display the data the master writes into Slave */
  ubBytes = HDMI_READ_REG_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IN_FIFO_CNTR, PortNum));
  *ulLength =  ubBytes;

  /*Just expecting an EDID block id */
  if (ubBytes > 1) return FALSE;

  *puBuffer = HDMI_READ_REG_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_DATA, PortNum));

  return TRUE;
}


/******************************************************************************
 FUNCTION        : sthdmirx_I2C_slave_isr_bottom_half()
 USAGE           : Read EDID from SBC and start sending it to master
 INPUT           : work pointer
 RETURN          : None
 USED_REGS       :
******************************************************************************/
void sthdmirx_I2C_slave_isr_bottom_half(struct work_struct *work)
{
  U32 PortNum;
  sthdmirx_I2C_slave_control_t *I2CSlaveCtrl;
  hdmirx_dev_handle_t *dHandle_p;
  int error;
  uint8_t lpm_blk_num;

  TRC(TRC_ID_HDMI_RX,"I2C slave bottom half ");

  dHandle_p = container_of(work, hdmirx_dev_handle_t , hdmirx_csmi2c_isr_work);

  for (PortNum = 0; PortNum < dHandle_p->DeviceProp.Max_Ports; PortNum ++)
  {
    I2CSlaveCtrl= &(dHandle_p->PortHandle[PortNum].stI2CSlaveCtrl);

    /*  if no data transferred on port yet, start emission */
    if ((I2CSlaveCtrl->Received &  I2C_SLAVE_RCV_SENDING) && (I2CSlaveCtrl->TotalBytes == 0))
    {
      /* Read Edid contents into a buffer */

      if (I2CSlaveCtrl->puBuffer == NULL)
      {
        I2CSlaveCtrl->puBuffer = (u8 *) stm_hdmirx_malloc(EDID_BLOCK_SIZE + 16);
      }

      lpm_blk_num = (uint8_t) ((I2CSlaveCtrl->seg_ptr << 1) + I2CSlaveCtrl->offset);
      TRC(TRC_ID_HDMI_RX, "Port %d, SBC EDID block %d", PortNum, lpm_blk_num);

      error = st_lpm_read_edid(
                      (unsigned char * ) I2CSlaveCtrl->puBuffer,
                      lpm_blk_num);
      if (error != 0)
      {
        TRC(TRC_ID_HDMI_RX, "FAILURE in Read EDID block-%d using LPM FW, port %d ", lpm_blk_num, PortNum);
      }

      /*  Start transmission from slave */
      sthdmirx_I2C_read_from_slaveFIFO(PortNum, I2CSlaveCtrl);

      /* Allow offset change*/
      I2CSlaveCtrl->Received &= ~I2C_SLAVE_RCV_WRITE;
      HDMI_CLEAR_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum), I2C_SLV1_RFIFO_nE_IRQ_EN);

      /* Re-enable slave interrupts */
      HDMI_SET_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum), I2C_SLV1_IRQ_EN);

    } /* Port in read state */
  }/*  for...PortNum */
}

/******************************************************************************
 FUNCTION      : sthdmirx_I2C_slave_reset()
 USAGE           : I2C Slave Reset
 INPUT           : Device Address, Port Id
 RETURN          :
 USED_REGS       :
******************************************************************************/
void sthdmirx_I2C_slave_reset(
                U8 PortNum,
                sthdmirx_I2C_slave_control_t *I2CSlaveCtrl)
{
  //U32 ulCtrlWord = 0x0;

  TRC(TRC_ID_HDMI_RX,"I2C slave Reset ");

  /* Preserve possible next transaction flags in lower bits */
  I2CSlaveCtrl->Received &= ~I2C_SLAVE_RCV_SENDING;

  HDMI_READ_REG_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IN_FIFO_CNTR, PortNum));

  /* Ignore further write FIFO empty indications */
  HDMI_CLEAR_REG_BITS_DWORD(
        GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum),
        I2C_SLV1_WRITE_FIFO_AE | I2C_SLV1_WRITE_FIFO_E);

  /*  Allow next transaction */
  HDMI_SET_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,I2C_SLV1_IRQ_CTRL, PortNum),
        I2C_SLV1_DEV_IRQ_EN | I2C_SLV1_START_IRQ_EN|I2C_SLV1_RFIFO_nE_IRQ_EN);

  /* Segment pointer is reset to zero after STOP or NACK */
  I2CSlaveCtrl->seg_ptr = 0;


  TRC(TRC_ID_HDMI_RX,"I2C Slave for Port %d Reset", PortNum);
  return;
}

/******************************************************************************
 FUNCTION      : sthdmirx_I2C_register_dev_id()
 USAGE           : Register Device id to the Slave specified
 INPUT           : Device Address, Port Id
 RETURN          : Status of the Operation
 USED_REGS       :
******************************************************************************/
BOOL sthdmirx_I2C_register_dev_id(sthdmirx_I2C_slave_control_t *I2CSlaveCtrl, U8 *DeviceAddrs, U8 PortNum)
{
  U32 ulPinNum;
  U32 ulBitstoSet;
  unsigned int Addr;

  for (Addr = 0; Addr < 2; Addr ++)
  {
     ulPinNum = DeviceAddrs[Addr] >> 1;
     ulBitstoSet = (ulPinNum % I2C_SLAVE_REG_TABLE_BITS);

     HDMI_SET_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,
                          (I2C_SLV1_ADDR_TBL0 + ((ulPinNum / I2C_SLAVE_REG_TABLE_BITS) * 4)), PortNum),
                          (1 << ulBitstoSet));

     TRC(TRC_ID_HDMI_RX,"sthdmirx_I2C_register_dev_id - Device Id 0x%x Registered ",
         DeviceAddrs[Addr]);
  }

  return TRUE;
}

/******************************************************************************
 FUNCTION   : sthdmirx_I2C_unregister_dev_id()
 USAGE      : UnRegister Device id to the Slave specified
 INPUT      : I2CSlaveCtrl
 RETURN     : Status of the Operation
 USED_REGS  :
******************************************************************************/
BOOL sthdmirx_I2C_unregister_dev_id(sthdmirx_I2C_slave_control_t *I2CSlaveCtrl)
{
  U32 ulPinNum;
  U32 ulBitstoSet;

  unsigned int Addr;

  for (Addr = 0; Addr < 2; Addr ++)
  {
    ulPinNum = I2CSlaveCtrl->uDevAddress[Addr] >> 1;
    ulBitstoSet = (ulPinNum % I2C_SLAVE_REG_TABLE_BITS);

    HDMI_CLEAR_REG_BITS_DWORD(GET_ADDRS(I2CSlaveCtrl->ulCellBaseAddr,
                          (I2C_SLV1_ADDR_TBL0 + ((ulPinNum / I2C_SLAVE_REG_TABLE_BITS) *4)), I2CSlaveCtrl->PortNo),
                           ulBitstoSet);

    TRC(TRC_ID_HDMI_RX, "sthdmirx_I2C_unregister_dev_id - Device Id 0x%x UnRegistered ",
       I2CSlaveCtrl->uDevAddress[Addr]);
  }
  return TRUE;
}

#endif /*STHDMIRX_INTERNAL_EDID_SUPPORT,  End of file */
