/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __HDMIRX_HPD_H__
#define __HDMIRX_HPD_H__

/*Includes------------------------------------------------------------------------------*/
#include "stddefs_hdmirx.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* Private Types ---------------------------------------------------------- --------------*/
#define     STHDMIRX_CABLE_CONNECT_DEBOUNCE_TIME            10000	/*100 msec */

  /* Private Constants --------------------------------------------------------------------- */
  typedef enum {
    ACTIVE_HPD_HW_IN_CSM,
    ACTIVE_HPD_HW_IN_CORE
  }
  sthdmirx_HPD_hardware_select_t;

  /* ------------------------------- End of file --------------------------------------------- */

#ifdef __cplusplus
}
#endif
#endif				/* __HDMIRX_HPD_H__ */
