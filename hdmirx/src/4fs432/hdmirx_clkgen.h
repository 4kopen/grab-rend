/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __HDMIRX_CLOCKGEN_H__
#define __HDMIRX_CLOCKGEN_H__

/*Includes------------------------------------------------------------------------------*/
#include "stddefs_hdmirx.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* Private Types ---------------------------------------------------------- --------------*/

  /* Private Constants --------------------------------------------------------------------- */
  typedef enum {
    HDMIRX_PIX_AVDDS1 = 0,
    HDMIRX_AUD_AVDDS2,
    HDMIRX_PIX_AVDDS3,
    HDMIRX_AUD_AVDDS4,
  }
  sthdmirx_AVDDStypes_t;
  typedef enum
  {
    SEL_INPUT_CLOCK_FROM_DDS,
    SEL_INPUT_CLOCK_FROM_TMDS_LINK
  } sthdmirx_input_clkSource_t;

  /* Private variables (static) --------------------------------------------------------------- */

  /* Global Variables ----------------------------------------------------------------------- */

  /* Private Macros ------------------------------------------------------------------------ */

  /* Exported Macros--------------------------------------------------------- --------------*/

  /* Exported Functions ----------------------------------------------------- ---------------*/
  void sthdmirx_clkgen_powerdownDDS(sthdmirx_AVDDStypes_t estAVdds,
                                    U32 ulBaseAddrs);
  void sthdmirx_clkgen_powerupDDS(sthdmirx_AVDDStypes_t estAVdds,
                                  U32 ulBaseAddrs);
  void sthdmirx_clkgen_init(U32 ulBaseAddrs, const void *Handle);
  void sthdmirx_clkgen_term(void);
  U32 sthdmirx_clkgen_DDS_current_freq_get(sthdmirx_AVDDStypes_t estAVdds,
      U32 ulBaseAddrs);
  U16 sthdmirx_clkgen_DDS_tracking_error_get(sthdmirx_AVDDStypes_t estAVdds,
      U32 ulBaseAddrs);
  void sthdmirx_clkgen_DDS_openloop_force(
    sthdmirx_AVDDStypes_t estAVdds,U32 ulDdsInitFreq,U32 ulBaseAddrs);
  BOOL sthdmirx_clkgen_DDS_closeloop_force(sthdmirx_AVDDStypes_t estAVdds,
      U32 ulDdsInitFreq,U32 ulBaseAddrs);
  /* ------------------------------- End of file --------------------------------------------- */
#ifdef __cplusplus
}
#endif
#endif				/*end of __HDMIRX_CLOCKGEN_H__ */
