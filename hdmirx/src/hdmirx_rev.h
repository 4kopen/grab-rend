/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef _STM_HDMIRX_VERSION_H_
#define _STM_HDMIRX_VERSION_H_

#ifndef STM_HDMIRX_VERSION
#define STM_HDMIRX_VERSION "HDMIRX_INT_1-1"
#endif

#endif /* #ifndef _STM_HDMIRX_VERSION_H_ */

/* End of hdmirx_rev.h */
