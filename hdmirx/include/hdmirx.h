/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __HDMIRX_H__
#define __HDMIRX_H__
typedef enum stm_hdmirx_operational_mode_e
{
  STM_HDMIRX_ROUTE_OP_MODE_DVI = 0x01,
  STM_HDMIRX_ROUTE_OP_MODE_HDMI = 0x04,
  STM_HDMIRX_ROUTE_OP_MODE_AUTO = 0x08,

} stm_hdmirx_operational_mode_t;

typedef enum stm_hdmirx_equalization_mode_e
{
  STM_HDMIRX_ROUTE_EQ_MODE_DISABLE,
  STM_HDMIRX_ROUTE_EQ_MODE_AUTO,
  STM_HDMIRX_ROUTE_EQ_MODE_LOW_GAIN,
  STM_HDMIRX_ROUTE_EQ_MODE_MEDIUM_GAIN,
  STM_HDMIRX_ROUTE_EQ_MODE_HIGH_GAIN,
  STM_HDMIRX_ROUTE_EQ_MODE_CUSTOM
} stm_hdmirx_equalization_mode_t;
typedef enum stm_hdmirx_output_pixel_width_e
{
  STM_HDMIRX_ROUTE_OUTPUT_PIXEL_WIDTH_8_BITS = 0x01,
  STM_HDMIRX_ROUTE_OUTPUT_PIXEL_WIDTH_10_BITS = 0x02,
  STM_HDMIRX_ROUTE_OUTPUT_PIXEL_WIDTH_12_BITS = 0x04,
  STM_HDMIRX_ROUTE_OUTPUT_PIXEL_WIDTH_16_BITS = 0x08,

} stm_hdmirx_output_pixel_width_t;
typedef enum
{
  HDMIRX_INPUT_PORT_1,
  HDMIRX_INPUT_PORT_2,
  HDMIRX_INPUT_PORT_3,
  HDMIRX_INPUT_PORT_4
} hdmirx_port_t;

typedef struct stm_hdmirx_equalization_config_s
{
  uint32_t low_freq_gain;
  uint32_t high_freq_gain;
  uint32_t rterm_val;

} stm_hdmirx_equalization_config_t;

#endif
